/*
from git https://github.com/soywod/c-map
*/

#ifndef MAP_H
#define MAP_H

typedef struct map* map_t;

map_t mapNew();
char* mapAdd(const char *key, void *val, map_t map);
char* mapDynAdd(const char *key, void *val, map_t map);
void *mapGet(const char *key, map_t map);
void mapMapFunction(map_t map, void (*fun)(const char *, void *, void *), void *);
void mapClose(map_t map);

#endif
