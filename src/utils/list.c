#include "mcc/list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// typedef list_t* list_ptr_t;

void element_free(list_t *list, list_element_t *element)
{
	if (list->options & LIST_OPTIONS_OWNER)
		free(element->data);
	free(element);
}

list_ptr_t list_create(enum list_options options)
{
	list_ptr_t list = malloc(sizeof(list_t));
	list->first = NULL;
	list->last = NULL;
	list->size = 0;
	list->options = options;
	return list;
}

void list_append(list_ptr_t list, void *data)
{
	list_element_t *new_element = malloc(sizeof(list_element_t));
	new_element->next = NULL;
	new_element->data = data;
	if (list->last == NULL) {
		list->first = list->last = new_element;
	} else {
		list->last = list->last->next = new_element;
	}
	list->size++;
}

list_element_t *list_get_elem(list_ptr_t list, unsigned int i)
{
	if (i >= list->size)
		return NULL;
	list_element_t *elem = list->first;
	while (i > 0) {
		elem = elem->next;
		i--;
	}
	return elem;
}

void *list_get(list_ptr_t list, unsigned int i)
{
	list_element_t *elem = list_get_elem(list, i);
	if (elem == NULL)
		return NULL;
	return elem->data;
}

void list_push(list_ptr_t list, void *data)
{
	list_element_t *new_element = malloc(sizeof(list_element_t));
	new_element->next = list->first;
	new_element->data = data;
	if (list->last == NULL) {
		list->first = list->last = new_element;
	} else {
		list->first = new_element;
	}
	list->size++;
}

void *list_top(list_ptr_t list)
{
	if (list->size == 0)
		return NULL;
	return list->first->data;
}

void list_pop(list_ptr_t list)
{
	if (list->first == NULL)
		return;
	list_element_t *elem = list->first;
	list->first = list->first->next;
	element_free(list, elem);
	list->size--;
}

void list_remove_idx(list_ptr_t list, unsigned int i)
{
	list_element_t *elem = list_get_elem(list, i);
	if (elem == NULL)
		return;
	if (elem == list->first) {
		list->first = elem->next;
	} else {
		list_element_t *predElem = list_get_elem(list, i - 1);
		if (elem == list->last) {
			list->last = predElem;
		}
		predElem->next = elem->next;
	}
	element_free(list, elem);
	list->size--;
}

int list_find(list_ptr_t list, void *data)
{
	list_element_t *elem = list->first;
	int i = 0;
	while (elem != NULL) {
		if (elem->data == data)
			return i;
		elem = elem->next;
		i++;
	}
	return -1;
}

void list_remove(list_ptr_t list, void *data)
{
	if (list->first->data == data) {
		list->first = list->first->next;
	}
}

void list_iterate(list_ptr_t list, void (*func)(void *, void *), void *arg)
{
	list_element_t *iterator = list->first;
	while (iterator != NULL) {
		func(iterator->data, arg);
		iterator = iterator->next;
	}
}

void list_iterate_idx(list_ptr_t list, unsigned int from, unsigned int to, void (*func)(void *, void *), void *arg)
{
	list_element_t *iterator = list->first;
	unsigned int idx = 0;
	while (iterator != NULL) {
		if (idx >= from && idx < to) {
			func(iterator->data, arg);
		}
		iterator = iterator->next;
		idx++;
	}
}

void list_free(list_ptr_t list)
{
	list_element_t *iterator = list->first;
	while (iterator != NULL) {
		list_element_t *temp = iterator;
		iterator = temp->next;
		element_free(list, temp);
	}
	free(list);
}