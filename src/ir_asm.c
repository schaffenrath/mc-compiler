#include "mcc/ir_asm.h"
#include <stdarg.h>
#include <stdlib.h>

static char *getBuffer(unsigned int size)
{
	return malloc(sizeof(char) * size);
}

static void add_line(list_ptr_t code, const char *format, ...)
{
	char *buffer = getBuffer(256);
	va_list args;
	va_start(args, format);
	vsnprintf(buffer, 256, format, args);

	// do something with the error

	list_append(code, buffer);
	va_end(args);
}

static void get_variable_access(char *access, const char *var, struct mcc_symbolTable *tab)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, var, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	snprintf(access, 20, "%d(%%ebp)", offset + attr->negativeStackOffset);
}

static void translate_copy_variable(
    struct mcc_symbolAttributes *attr, int offset, char *res, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{

	int offset2 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr2 = mcc_symbTlookupRec2(tab, res, MCC_SYMBOL_VARIABLE_ONLY, &offset2);

	if (attr->type == MCC_AST_TYPE_FLOAT) {
		add_line(assembler->code, "flds %d(%%ebp)", offset + attr->negativeStackOffset);
		add_line(assembler->code, "fstps %d(%%ebp)", offset2 + attr2->negativeStackOffset);
	} else {
		add_line(assembler->code, "movl %d(%%ebp), %%eax", offset + attr->negativeStackOffset);
		add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset2 + attr2->negativeStackOffset);
	}
}

static void translate_copy_literal(char *arg1, char *res, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, res, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	switch (attr->type) {
	case MCC_AST_TYPE_FLOAT: {
		if (strcmp("returnregister", arg1) == 0) {
			add_line(assembler->code, "fstps %d(%%ebp)", offset + attr->negativeStackOffset);
		} else {
			float literal = atof(arg1);
			int *parts = (int *)&literal;
			add_line(assembler->data, "__LCD%d:", assembler->labelCounter);
			add_line(assembler->data, "  .long %d", parts[0]);
			add_line(assembler->code, "flds __LCD%d", assembler->labelCounter++);
			add_line(assembler->code, "fstps %d(%%ebp)", offset + attr->negativeStackOffset);
		}
		break;
	}
	case MCC_AST_TYPE_INT:
		if (strcmp("returnregister", arg1) == 0) {
			add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset + attr->negativeStackOffset);
		} else {
			add_line(assembler->code, "movl $%s, %d(%%ebp)", arg1, offset + attr->negativeStackOffset);
		}
		break;
	case MCC_AST_TYPE_BOOL:
		if (strcmp("returnregister", arg1) == 0) {
			add_line(assembler->code, "movl %s, %d(%%ebp)", "%eax", offset + attr->negativeStackOffset);
		} else {
			if (strcmp("FALSE", arg1) == 0) {
				add_line(assembler->code, "movl $0, %d(%%ebp)", offset + attr->negativeStackOffset);
			} else {
				add_line(assembler->code, "movl $1, %d(%%ebp)", offset + attr->negativeStackOffset);
			}
		}
		break;
	case MCC_AST_TYPE_STRING:
		// string is stored as \"\" <- so remove \" for assembler program
		arg1[strlen(arg1) - 2] = '\0'; // overwrite last \"
		add_line(assembler->data, "__LCD%d:", assembler->labelCounter);
		add_line(assembler->data, "  .string \"%s\"", arg1 + 2); // skip first \"
		add_line(assembler->code, "movl $__LCD%d, %d(%%ebp)", assembler->labelCounter++,
		         offset + attr->negativeStackOffset);
		break;
	default:
		break;
	}
}

static void translate_copy(char *arg1, char *res, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	// Intel syntax
	// mov     eax, DWORD PTR [rbp-4]
	// mov     DWORD PTR [rbp-12], eax
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	if (attr != NULL) { // copy variable to variable
		translate_copy_variable(attr, offset, res, tab, assembler);
	} else { // copy literal to variable
		translate_copy_literal(arg1, res, tab, assembler);
	}
}

static void translate_label(char *arg1, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{

	add_line(assembler->code, "%s:", arg1);
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec(tab, arg1, MCC_SYMBOL_FUNC_ONLY);
	if (attr != NULL) { // symbol is a function
		add_line(assembler->code, "push %%ebp");
		add_line(assembler->code, "mov  %%esp, %%ebp");
		add_line(assembler->code, "sub $%u, %%esp",
		         mcc_symbTgetMemorySpace(mcc_symbTfindScope(tab, arg1))); // reserve stack for local variables
		assembler->nestingCounter = 0;                                    // start of function = no nesting
	}
}

static void translate_return(char *arg1, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	if (arg1 != NULL) {
		int offset = -mcc_symbTgetMemorySpace(tab) - 4;
		struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
		switch (attr->type) {
		case MCC_AST_TYPE_INT:
		case MCC_AST_TYPE_BOOL:
			add_line(assembler->code, "movl %d(%%ebp), %%eax", offset + attr->negativeStackOffset);
			break;
		case MCC_AST_TYPE_FLOAT:
			add_line(assembler->code, "flds %d(%%ebp)", offset + attr->negativeStackOffset);
			break;
		default:
			break;
		}
	}
	for (unsigned int i = 0; i < assembler->nestingCounter; i++) {
		add_line(assembler->code, "leave"); // leave all blocks before able to leave function
	}
	// variables
	add_line(assembler->code, "leave"); // -> is reversed push & mov of ebp and esp
	add_line(assembler->code, "ret");
}

static void translate_call(char *arg1, int arg2, mcc_asm_ptr_t assembler)
{
	add_line(assembler->code, "call %s", arg1);
	add_line(assembler->code, "add $%d, %%esp", arg2 * 4); // clear parameters from stack TODO: consider double
}

static void translate_prepare(char *arg1, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	add_line(assembler->code, "push %d(%%ebp)", offset + attr->negativeStackOffset);
}

static void translate_open_block(char *arg1, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	add_line(assembler->code, "push %%ebp");
	add_line(assembler->code, "mov  %%esp, %%ebp");
	add_line(assembler->code, "sub $%u, %%esp",
	         mcc_symbTgetMemorySpace(mcc_symbTfindScope(tab, arg1))); // reserve stack for local variables
	assembler->nestingCounter++;                                      // increase nesting on block
}

static void translate_close_block(mcc_asm_ptr_t assembler)
{
	add_line(assembler->code, "leave");
	assembler->nestingCounter--; // decrease nesting on block leave
}

static void translate_build_array(char *arg1, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	int pointerSkip = mcc_symbTtypeToByte(attr->type);
	add_line(assembler->code, "leal %d(%%ebp), %%eax", offset + attr->negativeStackOffset + pointerSkip);
	add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset + attr->negativeStackOffset);
}

static void translate_binary_calc(
    char *arg1, char *arg2, const char *op, char *res, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	char var1[20];
	char var2[20];
	char result[20];
	get_variable_access(var1, arg1, tab);
	get_variable_access(var2, arg2, tab);
	get_variable_access(result, res, tab);

	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec(tab, res, MCC_SYMBOL_VARIABLE_ONLY);
	if (attr->type == MCC_AST_TYPE_FLOAT) {
		add_line(assembler->code, "flds %s", var1);
		add_line(assembler->code, "f%ss %s", op, var2);
		add_line(assembler->code, "fstps %s", result);
	} else if (attr->type == MCC_AST_TYPE_INT) {
		add_line(assembler->code, "movl %s, %%eax", var1);
		add_line(assembler->code, "%sl %s, %%eax", op, var2);
		add_line(assembler->code, "movl %%eax, %s", result);
	}
}

static void
translate_binary_mul(char *arg1, char *arg2, char *res, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	char var1[20];
	char var2[20];
	char result[20];
	get_variable_access(var1, arg1, tab);
	get_variable_access(var2, arg2, tab);
	get_variable_access(result, res, tab);

	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec(tab, res, MCC_SYMBOL_VARIABLE_ONLY);
	if (attr->type == MCC_AST_TYPE_FLOAT) {
		add_line(assembler->code, "flds %s", var1);
		add_line(assembler->code, "fmuls %s", var2);
		add_line(assembler->code, "fstps %s", result);
	} else if (attr->type == MCC_AST_TYPE_INT) {
		add_line(assembler->code, "movl %s, %%eax", var1);
		add_line(assembler->code, "imull %s, %%eax", var2);
		add_line(assembler->code, "movl %%eax, %s", result);
	}
}

static void
translate_binary_div(char *arg1, char *arg2, char *res, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	char var1[20];
	char var2[20];
	char result[20];
	get_variable_access(var1, arg1, tab);
	get_variable_access(var2, arg2, tab);
	get_variable_access(result, res, tab);

	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec(tab, res, MCC_SYMBOL_VARIABLE_ONLY);
	if (attr->type == MCC_AST_TYPE_FLOAT) {
		add_line(assembler->code, "flds %s", var1);
		add_line(assembler->code, "fdivs %s", var2);
		add_line(assembler->code, "fstps %s", result);
	} else if (attr->type == MCC_AST_TYPE_INT) {
		add_line(assembler->code, "movl %s, %%eax", var1);
		add_line(assembler->code, "cltd");
		add_line(assembler->code, "idivl %s", var2);
		add_line(assembler->code, "movl %%eax, %s", result);
	}
}

static void translate_binary_neg(char *arg1, char *res, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	char var1[20];
	char result[20];
	get_variable_access(var1, arg1, tab);
	get_variable_access(result, res, tab);

	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec(tab, res, MCC_SYMBOL_VARIABLE_ONLY);
	if (attr->type == MCC_AST_TYPE_FLOAT) {
		add_line(assembler->code, "flds %s", var1);
		add_line(assembler->code, "fchs");
		add_line(assembler->code, "fstps %s", result);
	} else if (attr->type == MCC_AST_TYPE_INT) {
		add_line(assembler->code, "movl %s, %%eax", var1);
		add_line(assembler->code, "negl %%eax");
		add_line(assembler->code, "movl %%eax, %s", result);
	}
}

static void
translate_build_load(char *arg1, char *arg2, char *res, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	int offsetRes = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attrRes = mcc_symbTlookupRec2(tab, res, MCC_SYMBOL_VARIABLE_ONLY, &offsetRes);

	char index[20];
	get_variable_access(index, arg2, tab);

	add_line(assembler->code, "movl %s, %%eax", index);
	add_line(assembler->code, "movl %d(%%ebp), %%edx", offset + attr->negativeStackOffset);
	switch (attrRes->type) {
	case MCC_AST_TYPE_FLOAT:
		add_line(assembler->code, "flds (%%edx,%%eax,%d)", mcc_symbTbaseTypeToByte(attr->type));
		add_line(assembler->code, "fstps %d(%%ebp)", offsetRes + attrRes->negativeStackOffset);
		break;
	case MCC_AST_TYPE_INT:
		add_line(assembler->code, "movl (%%edx,%%eax,%d), %%eax", mcc_symbTbaseTypeToByte(attr->type));
		add_line(assembler->code, "movl %%eax, %d(%%ebp)", offsetRes + attrRes->negativeStackOffset);
		break;
	case MCC_AST_TYPE_BOOL:
		add_line(assembler->code, "movl (%%edx,%%eax,%d), %%eax", mcc_symbTbaseTypeToByte(attr->type));
		add_line(assembler->code, "movl %%eax, %d(%%ebp)", offsetRes + attrRes->negativeStackOffset);
		break;
	default:
		break;
	}
}

static void
translate_build_store(char *arg1, char *arg2, char *res, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	int offsetRes = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attrRes = mcc_symbTlookupRec2(tab, res, MCC_SYMBOL_VARIABLE_ONLY, &offsetRes);

	char index[20];
	get_variable_access(index, arg2, tab);

	add_line(assembler->code, "movl %s, %%eax", index);
	add_line(assembler->code, "movl %d(%%ebp), %%edx", offsetRes + attrRes->negativeStackOffset);
	switch (attr->type) {
	case MCC_AST_TYPE_FLOAT:
		add_line(assembler->code, "flds %d(%%ebp)", offset + attr->negativeStackOffset);
		add_line(assembler->code, "fstps (%%edx,%%eax,%d)", mcc_symbTbaseTypeToByte(attrRes->type));
		break;
	case MCC_AST_TYPE_INT:
		add_line(assembler->code, "movl %d(%%ebp), %%ebx", offset + attr->negativeStackOffset);
		add_line(assembler->code, "movl %%ebx, (%%edx,%%eax,%d)", mcc_symbTbaseTypeToByte(attrRes->type));
		break;
	case MCC_AST_TYPE_BOOL:
		add_line(assembler->code, "movl %d(%%ebp), %%ebx", offset + attr->negativeStackOffset);
		add_line(assembler->code, "movl %%ebx, (%%edx,%%eax,%d)", mcc_symbTbaseTypeToByte(attrRes->type));
		break;
	default:
		break;
	}
}

static void translate_jump(char *label, mcc_asm_ptr_t assembler)
{
	add_line(assembler->code, "jmp %s", label);
}

static void translate_jump_false(char *label, char *false_cond, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, false_cond, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	add_line(assembler->code, "cmpl $0, %d(%%ebp)", offset + attr->negativeStackOffset);
	add_line(assembler->code, "je %s", label);
}

static void translate_equal(char *arg1, char *arg2, char *result, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	int offset2 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr2 = mcc_symbTlookupRec2(tab, arg2, MCC_SYMBOL_VARIABLE_ONLY, &offset2);
	int offset3 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *res = mcc_symbTlookupRec2(tab, result, MCC_SYMBOL_VARIABLE_ONLY, &offset3);

	switch (attr->type) {
	case MCC_AST_TYPE_INT: {
		add_line(assembler->code, "movl %d(%%ebp), %%eax", offset + attr->negativeStackOffset);
		add_line(assembler->code, "cmpl %d(%%ebp), %%eax", offset2 + attr2->negativeStackOffset);
		add_line(assembler->code, "sete %%al");
		add_line(assembler->code, "movzbl %%al, %%eax");
		add_line(assembler->code, "andl $1, %%eax");
		add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset3 + res->negativeStackOffset);
		break;
	}
	case MCC_AST_TYPE_FLOAT: {
		add_line(assembler->code, "flds %d(%%ebp)", offset + attr->negativeStackOffset);
		add_line(assembler->code, "flds %d(%%ebp)", offset2 + attr2->negativeStackOffset);
		add_line(assembler->code, "fcomip %%st(1), %%st");
		add_line(assembler->code, "fstp %%st(0)");
		add_line(assembler->code, "sete %%al");
		add_line(assembler->code, "movzbl %%al, %%eax");
		add_line(assembler->code, "andl $1, %%eax");
		add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset3 + res->negativeStackOffset);
		break;
	}
	default:
		break;
	}
}

static void translate_less(char *arg1, char *arg2, char *result, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	int offset2 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr2 = mcc_symbTlookupRec2(tab, arg2, MCC_SYMBOL_VARIABLE_ONLY, &offset2);
	int offset3 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *res = mcc_symbTlookupRec2(tab, result, MCC_SYMBOL_VARIABLE_ONLY, &offset3);
	switch (attr->type) {
	case MCC_AST_TYPE_INT: {
		add_line(assembler->code, "movl %d(%%ebp), %%eax", offset + attr->negativeStackOffset);
		add_line(assembler->code, "cmpl %d(%%ebp), %%eax", offset2 + attr2->negativeStackOffset);
		add_line(assembler->code, "setl %%al");
		add_line(assembler->code, "movzbl %%al, %%eax");
		add_line(assembler->code, "andl $1, %%eax");
		add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset3 + res->negativeStackOffset);
		break;
	}
	case MCC_AST_TYPE_FLOAT: {
		add_line(assembler->code, "flds %d(%%ebp)", offset + attr->negativeStackOffset);
		add_line(assembler->code, "flds %d(%%ebp)", offset2 + attr2->negativeStackOffset);
		add_line(assembler->code, "fcomip %%st(1), %%st");
		add_line(assembler->code, "fstp %%st(0)");
		add_line(assembler->code, "seta %%al");
		add_line(assembler->code, "movzbl %%al, %%eax");
		add_line(assembler->code, "andl $1, %%eax");
		add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset3 + res->negativeStackOffset);
		break;
	}
	default:
		break;
	}
}

static void translate_negation(char *from, char *to, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, from, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	int offset2 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *res = mcc_symbTlookupRec2(tab, to, MCC_SYMBOL_VARIABLE_ONLY, &offset2);
	add_line(assembler->code, "movl %d(%%ebp), %%eax", offset + attr->negativeStackOffset);
	add_line(assembler->code, "xorl $1, %%eax");
	add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset2 + res->negativeStackOffset);
}

static void
translate_logical_or(char *arg1, char *arg2, char *result, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	int offset2 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr2 = mcc_symbTlookupRec2(tab, arg2, MCC_SYMBOL_VARIABLE_ONLY, &offset2);
	int offset3 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *res = mcc_symbTlookupRec2(tab, result, MCC_SYMBOL_VARIABLE_ONLY, &offset3);
	add_line(assembler->code, "movl %d(%%ebp), %%eax", offset + attr->negativeStackOffset);
	add_line(assembler->code, "orl %d(%%ebp), %%eax", offset2 + attr2->negativeStackOffset);
	add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset3 + res->negativeStackOffset);
}

static void
translate_logical_and(char *arg1, char *arg2, char *result, struct mcc_symbolTable *tab, mcc_asm_ptr_t assembler)
{
	int offset = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec2(tab, arg1, MCC_SYMBOL_VARIABLE_ONLY, &offset);
	int offset2 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *attr2 = mcc_symbTlookupRec2(tab, arg2, MCC_SYMBOL_VARIABLE_ONLY, &offset2);
	int offset3 = -mcc_symbTgetMemorySpace(tab) - 4;
	struct mcc_symbolAttributes *res = mcc_symbTlookupRec2(tab, result, MCC_SYMBOL_VARIABLE_ONLY, &offset3);
	add_line(assembler->code, "movl %d(%%ebp), %%eax", offset + attr->negativeStackOffset);
	add_line(assembler->code, "andl %d(%%ebp), %%eax", offset2 + attr2->negativeStackOffset);
	add_line(assembler->code, "movl %%eax, %d(%%ebp)", offset3 + res->negativeStackOffset);
}

static void translate_ir_to_asm(enum mcc_ir_op_type type,
                                char *arg1,
                                char *arg2,
                                unsigned int arg2n,
                                char *res,
                                struct mcc_symbolTable *tab,
                                void *arg)
{
	mcc_asm_ptr_t assembler = arg;
	switch (type) {
	case MCC_IR_OP_TYPE_COPY:
		translate_copy(arg1, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_LABEL:
		translate_label(arg1, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_CALL:
		translate_call(arg1, arg2n, assembler);
		break;
	case MCC_IR_OP_TYPE_RETURN:
		translate_return(arg1, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_PREP_PARAM:
		translate_prepare(arg1, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_OPEN_BLOCK:
		translate_open_block(arg1, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_CLOSE_BLOCK:
		translate_close_block(assembler);
		break;
	case MCC_IR_OP_TYPE_BI_ADDITION:
		translate_binary_calc(arg1, arg2, "add", res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_BI_SUBRACTION:
		translate_binary_calc(arg1, arg2, "sub", res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_BI_MULTIPLICATION:
		translate_binary_mul(arg1, arg2, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_BI_DIVISION:
		translate_binary_div(arg1, arg2, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_UN_NEGATIVE:
		translate_binary_neg(arg1, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_BUILD_ARRAY:
		translate_build_array(arg1, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_LOAD:
		translate_build_load(arg1, arg2, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_STORE:
		translate_build_store(arg1, arg2, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_JUMP:
		translate_jump(arg1, assembler);
		break;
	case MCC_IR_OP_TYPE_IF_NOT_JUMP:
		translate_jump_false(arg1, arg2, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_BI_EQ:
		translate_equal(arg1, arg2, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_BI_LO:
		translate_less(arg1, arg2, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_UN_NEGATION:
		translate_negation(arg1, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_BI_LOG_OR:
		translate_logical_or(arg1, arg2, res, tab, assembler);
		break;
	case MCC_IR_OP_TYPE_BI_LOG_AND:
		translate_logical_and(arg1, arg2, res, tab, assembler);
		break;
	default:
		break;
	}
}

mcc_asm_ptr_t mcc_ir_asm_create_assembler()
{
	mcc_asm_ptr_t assembler = malloc(sizeof(mcc_asm_t));
	assembler->code = list_create(LIST_OPTIONS_OWNER);
	assembler->data = list_create(LIST_OPTIONS_OWNER);
	assembler->labelCounter = 0;
	assembler->nestingCounter = 0;
	return assembler;
}

void mcc_ir_asm_translate(mcc_ir_ptr_t ir, mcc_asm_ptr_t assembler)
{
	mcc_ir_iterate(ir, translate_ir_to_asm, assembler);
}

static void print_asm_line(void *data, void *arg)
{
	FILE *out = arg;
	char *line = data;
	fprintf(out, "%s\n", line);
}

void mcc_ir_asm_to_output(FILE *out, mcc_asm_ptr_t assembler)
{
	fprintf(out, "data:\n");
	list_iterate(assembler->data, print_asm_line, out);

	fprintf(out, ".text\n");
	fprintf(out, ".globl	main\n");
	fprintf(out, ".type	main, @function\n");

	list_iterate(assembler->code, print_asm_line, out);
	fprintf(out, "\n");
}

void mcc_ir_asm_to_output_f_flag(FILE *out, mcc_asm_ptr_t assembler)
{
	list_iterate(assembler->code, print_asm_line, out);
	fprintf(out, "\n");
}

void mcc_ir_asm_delete(mcc_asm_ptr_t assembler)
{
	list_free(assembler->code);
	list_free(assembler->data);
	free(assembler);
}
