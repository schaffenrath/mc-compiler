#include "mcc/ast_synth.h"

#include <assert.h>
#include <stdlib.h>

#include "mcc/ast_print.h"
#include "mcc/ast_visit.h"
#include "mcc/log.h"
#include "mcc/parser.h"
#include "mcc/symbol_table.h"

// ---------------------------------------------------------------- DOT Printer

static char *globalfilename = "";

struct myUserData {
	FILE *out;
	enum mcc_semanticOptions options;
	enum mcc_sythesizeStatus result;
};

#define MESSAGE_SIZE 100

int mcc_check_return_statement(struct mcc_ast_statement *statement)
{
	if (statement->hasReturn)
		return 1;
	while (statement->right_neighbor != NULL) {
		statement = statement->right_neighbor;
		if (statement->hasReturn)
			return 1;
	}
	return 0;
}

struct mcc_symbolAttributes *mcc_check_existing_symbol(struct mcc_symbolTable *actualSymbolTable,
                                                       struct mcc_ast_expression *expression)
{
	char *symbol;
	enum mcc_symbolSearch searchCall = MCC_SYMBOL_ALL;
	struct mcc_symbolAttributes *attr = NULL;
	switch (expression->type) {
	case MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS:
		symbol = expression->identifier;
		searchCall = MCC_SYMBOL_VARIABLE_ONLY;
		char msg1[MESSAGE_SIZE];
		snprintf(msg1, MESSAGE_SIZE, "checking variable access \'%s\'\n", symbol);
		mcc_parser_error_ext(&expression->node.sloc, msg1, globalfilename, LOG_INFO);
		break;
	case MCC_AST_EXPREESION_TYPE_FUNCTION_CALL:
		symbol = expression->callname;
		searchCall = MCC_SYMBOL_FUNC_ONLY;
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "checking function call \'%s\'\n", symbol);
		mcc_parser_error_ext(&expression->node.sloc, msg, globalfilename, LOG_INFO);
		break;
	default:
		return attr;
	}
	if ((attr = mcc_symbTlookupRec(actualSymbolTable, symbol, searchCall)) == NULL &&
	    expression->type == MCC_AST_EXPREESION_TYPE_FUNCTION_CALL) { // disabled for variables
		// const char* msgT = "Variable %s already declared\n";
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "Function `%s` not yet declared\n", symbol);
		mcc_parser_error_ext(&expression->node.sloc, msg, globalfilename, LOG_ERROR);
		// free(msg);
	}
	return attr;
}

static void synth_expression_literal(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);
	mcc_parser_error_ext(&expression->node.sloc, "synthesizing literals\n", globalfilename, LOG_INFO);
	switch (expression->literal->type) {
	case MCC_AST_LITERAL_TYPE_INT: {
		expression->synthesizedType = MCC_AST_TYPE_INT;
		break;
	}
	case MCC_AST_LITERAL_TYPE_FLOAT: {
		expression->synthesizedType = MCC_AST_TYPE_FLOAT;
		break;
	}
	case MCC_AST_LITERAL_TYPE_STRING: {
		expression->synthesizedType = MCC_AST_TYPE_STRING;
		break;
	}
	case MCC_AST_LITERAL_TYPE_BOOL: {
		expression->synthesizedType = MCC_AST_TYPE_BOOL;
		break;
	}
	default:
		break;
	}
}

static void synth_expression_binary_op(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);
	struct myUserData *myData = data;
	mcc_parser_error_ext(&expression->node.sloc, "checking binary operator types\n", globalfilename, LOG_INFO);
	// check if binary operator has same type on both sides. void is not permitted on any binary operator
	if (expression->lhs->synthesizedType == expression->rhs->synthesizedType &&
	    expression->rhs->synthesizedType != MCC_AST_TYPE_VOID &&
	    expression->lhs->synthesizedType != MCC_AST_TYPE_VOID) {
		switch (expression->op) {
		case MCC_AST_BINARY_OP_ADD:
		case MCC_AST_BINARY_OP_DIV:
		case MCC_AST_BINARY_OP_MUL:
		case MCC_AST_BINARY_OP_SUB: {

			expression->synthesizedType = expression->lhs->synthesizedType;
			break;
		}
		default:
			expression->synthesizedType = MCC_AST_TYPE_BOOL;
			break;
		}
	} else {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "non compatible expression literals %s %s %s \n",
		         mcc_ast_print_type(expression->lhs->synthesizedType), mcc_ast_print_binary_op(expression->op),
		         mcc_ast_print_type(expression->rhs->synthesizedType));
		mcc_parser_error_ext(&expression->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	}
	// error on binary operator use on array types
	if (expression->lhs->synthesizedType == MCC_AST_TYPE_INT_ARRAY ||
	    expression->lhs->synthesizedType == MCC_AST_TYPE_FLOAT_ARRAY ||
	    expression->lhs->synthesizedType == MCC_AST_TYPE_BOOL_ARRAY ||
	    expression->rhs->synthesizedType == MCC_AST_TYPE_INT_ARRAY ||
	    expression->rhs->synthesizedType == MCC_AST_TYPE_FLOAT_ARRAY ||
	    expression->rhs->synthesizedType == MCC_AST_TYPE_BOOL_ARRAY) {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "operation \'%s\' on array address not supported\n",
		         mcc_ast_print_binary_op(expression->op));
		mcc_parser_error_ext(&expression->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	}
}

static void synth_expression_unary_op(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);
	mcc_parser_error_ext(&expression->node.sloc, "synthesizing type out of unary operator \n", globalfilename,
	                     LOG_INFO);
	expression->synthesizedType = expression->unary_next->synthesizedType;
}

static void synth_expression_parenth(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);
	mcc_parser_error_ext(&expression->node.sloc, "synthesizing type out of parenthesis \n", globalfilename,
	                     LOG_INFO);
	expression->synthesizedType = expression->expression->synthesizedType;
}

static void synth_expression_variable_access(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);
	mcc_parser_error_ext(&expression->node.sloc, "synthesizing variable access\n", globalfilename, LOG_INFO);
	// go from array access to element access if index is used
	struct mcc_symbolAttributes *sa = mcc_check_existing_symbol(expression->node.symbolTable, expression);
	if (sa != NULL) {
		if (sa->array_size != 0 && expression->index != NULL) {
			switch (sa->type) {
			case MCC_AST_TYPE_BOOL_ARRAY: {
				expression->synthesizedType = MCC_AST_TYPE_BOOL;
				break;
			}
			case MCC_AST_TYPE_INT_ARRAY: {
				expression->synthesizedType = MCC_AST_TYPE_INT;
				break;
			}
			case MCC_AST_TYPE_FLOAT_ARRAY: {
				expression->synthesizedType = MCC_AST_TYPE_FLOAT;
				break;
			}
			default:
				break;
			}
		} else {
			expression->synthesizedType = sa->type;
		}
	}
}

static void synth_expression_function_call(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);
	unsigned int parameters = 0;
	if (expression->functionarguments != NULL) {
		parameters++;
		struct mcc_ast_expression *wexp = expression->functionarguments;
		while (wexp->right_neighbor != NULL) {
			parameters++;
			wexp = wexp->right_neighbor;
		}
	}
	struct myUserData *myData = data;
	mcc_parser_error_ext(&expression->node.sloc, "checking function call arguments\n", globalfilename, LOG_INFO);
	// check matching parameter arguments
	struct mcc_symbolAttributes *sa = mcc_check_existing_symbol(expression->node.symbolTable, expression);
	if (sa != NULL) {
		if (sa->parameter_type_list->size != parameters) {
			char msg[MESSAGE_SIZE];
			snprintf(msg, MESSAGE_SIZE, "missmatch number of arguments for \'%s\'\n", expression->callname);
			mcc_parser_error_ext(&expression->node.sloc, msg, globalfilename, LOG_ERROR);
			myData->result = MCC_SYNTH_STATUS_ERROR;
		} else {
			struct mcc_ast_expression *argument = expression->functionarguments;
			for (unsigned int i = 0; i < parameters; ++i) {
				struct mcc_symbolAttributes *paramAttributes = list_get(sa->parameter_type_list, i);
				// check if type equal for parameters
				if (argument->synthesizedType != paramAttributes->type) {
					char msg[MESSAGE_SIZE];
					snprintf(msg, MESSAGE_SIZE,
					         "missmatch Type for argument %d of \'%s\' (wanted %s; "
					         "given %s)\n",
					         i + 1, expression->callname, mcc_ast_print_type(paramAttributes->type),
					         mcc_ast_print_type(argument->synthesizedType));
					mcc_parser_error_ext(&expression->node.sloc, msg, globalfilename, LOG_ERROR);
					myData->result = MCC_SYNTH_STATUS_ERROR;
				}
				// check if arraysize is equal
				if (paramAttributes->array_size != 0 &&
				    argument->synthesizedType == paramAttributes->type) {
					struct mcc_symbolAttributes *sa2 =
					    mcc_check_existing_symbol(expression->node.symbolTable, argument);
					if (sa2 != NULL) {
						if (paramAttributes->array_size != sa2->array_size) {
							char msg[MESSAGE_SIZE];
							snprintf(msg, MESSAGE_SIZE,
							         "missmatch Array Sizes (wanted %d; "
							         "given %d)\n",
							         paramAttributes->array_size, sa2->array_size);
							mcc_parser_error_ext(&expression->node.sloc, msg,
							                     globalfilename, LOG_ERROR);
							myData->result = MCC_SYNTH_STATUS_ERROR;
						}
					}
				}
				argument = argument->right_neighbor;
			}
		}
		expression->synthesizedType = sa->type;
	}
}

static void synth_literal_int(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);
}

static void synth_literal_float(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);
}

static void synth_literal_string(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);
}

static void synth_statement(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	// printf("%p\n", statement->node.symbolTable);
}

static void synth_statement_decl(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
}

static void synth_statement_decl_array(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
}

static void synth_statement_if(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	struct myUserData *myData = data;
	mcc_parser_error_ext(&statement->node.sloc, "checking if statement type \n", globalfilename, LOG_INFO);
	if (statement->condition->synthesizedType != MCC_AST_TYPE_BOOL) {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "Condition not of Type BOOL. is %s\n",
		         mcc_ast_print_type(statement->condition->synthesizedType));
		mcc_parser_error_ext(&statement->condition->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	}
	if (statement->falsepath != NULL && mcc_check_return_statement(statement->truepath) &&
	    mcc_check_return_statement(statement->falsepath))
		statement->hasReturn = 1;
}

static void synth_statement_while(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	struct myUserData *myData = data;
	mcc_parser_error_ext(&statement->node.sloc, "checking while statement type \n", globalfilename, LOG_INFO);
	if (statement->condition->synthesizedType != MCC_AST_TYPE_BOOL) {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "Condition not of Type BOOL. is \'%s\'\n",
		         mcc_ast_print_type(statement->condition->synthesizedType));
		mcc_parser_error_ext(&statement->condition->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	}
}

static void synth_statement_return(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	struct mcc_symbolAttributes *sa = mcc_symbTgetFunction(statement->node.symbolTable);
	struct myUserData *myData = data;
	// missmatch return types
	char msg[MESSAGE_SIZE];
	snprintf(msg, MESSAGE_SIZE, "checking return type match\n");
	mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_INFO);
	if (sa != NULL && statement->returnExpression != NULL &&
	    sa->type != statement->returnExpression->synthesizedType) {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "missmatch return Type (wanted %s; given %s)\n", mcc_ast_print_type(sa->type),
		         mcc_ast_print_type(statement->returnExpression->synthesizedType));
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	}

	// void on return but not on function
	if (sa != NULL && sa->type != MCC_AST_TYPE_VOID && statement->returnExpression == NULL) {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "missmatch return Type (wanted %s; given %s)\n", mcc_ast_print_type(sa->type),
		         mcc_ast_print_type(MCC_AST_TYPE_VOID));
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	}
	statement->hasReturn = 1;
}

static void synth_statement_func_decl(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	struct myUserData *myData = data;
	if (statement->returnType != MCC_AST_TYPE_VOID && !mcc_check_return_statement(statement->functionBody)) {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "no return statement found in function \'%s\'\n", statement->functionName);
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	} else {
		statement->hasReturn = 1;
	}
}

static void synth_statement_expression(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
}

static void synth_statement_assign(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	struct myUserData *myData = data;
	mcc_parser_error_ext(&statement->node.sloc, "checking assign statement \n", globalfilename, LOG_INFO);
	// assignment same on both sides. void is not allowed
	if (statement->statementAssignRightside != NULL &&
	    statement->statementAssignLeftside->synthesizedType ==
	        statement->statementAssignRightside->synthesizedType &&
	    statement->statementAssignLeftside->synthesizedType != MCC_AST_TYPE_VOID &&
	    statement->statementAssignRightside->synthesizedType != MCC_AST_TYPE_VOID) {
	} else {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "non compatible expression literals for assignment to \'%s\', (%s and %s) \n",
		         statement->statementAssignLeftside->identifier,
		         mcc_ast_print_type(statement->statementAssignLeftside->synthesizedType),
		         mcc_ast_print_type(statement->statementAssignRightside->synthesizedType));
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	}

	// array types not allowed on any side of assign
	if (statement->statementAssignLeftside->synthesizedType == MCC_AST_TYPE_INT_ARRAY ||
	    statement->statementAssignLeftside->synthesizedType == MCC_AST_TYPE_BOOL_ARRAY ||
	    statement->statementAssignLeftside->synthesizedType == MCC_AST_TYPE_FLOAT_ARRAY) {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "assignment to array address not supported for \'%s\'\n",
		         statement->statementAssignLeftside->identifier);
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	}
	if (statement->statementAssignRightside->synthesizedType == MCC_AST_TYPE_INT_ARRAY ||
	    statement->statementAssignRightside->synthesizedType == MCC_AST_TYPE_BOOL_ARRAY ||
	    statement->statementAssignRightside->synthesizedType == MCC_AST_TYPE_FLOAT_ARRAY) {
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "assignment from array address not supported \n");
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_ERROR);
		myData->result = MCC_SYNTH_STATUS_ERROR;
	}
}

static void synth_statement_block(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	statement->hasReturn = mcc_check_return_statement(statement->firstStatement);
}

// Setup an AST Visitor for synthing.
static struct mcc_ast_visitor synth_visitor(struct myUserData *data)
{
	assert(data);

	return (struct mcc_ast_visitor){
	    .traversal = MCC_AST_VISIT_DEPTH_FIRST,
	    .order = MCC_AST_VISIT_POST_ORDER,

	    .userdata = data,

	    .expression_literal = synth_expression_literal,
	    .expression_binary_op = synth_expression_binary_op,
	    .expression_parenth = synth_expression_parenth,
	    .expression_unary_op = synth_expression_unary_op,
	    .expression_variable_access = synth_expression_variable_access,
	    .expression_function_call = synth_expression_function_call,

	    .literal_int = synth_literal_int,
	    .literal_float = synth_literal_float,
	    .literal_string = synth_literal_string,

	    .statement = synth_statement,
	    .statement_decl = synth_statement_decl,
	    .statement_if = synth_statement_if,
	    .statement_while = synth_statement_while,
	    .statement_return = synth_statement_return,
	    .statement_decl_array = synth_statement_decl_array,
	    .statement_func_decl = synth_statement_func_decl,
	    .statement_expression = synth_statement_expression,
	    .statement_assign = synth_statement_assign,
	    .statement_block = synth_statement_block,

	};
}

static struct myUserData *createUserData(FILE *out, enum mcc_semanticOptions options)
{
	struct myUserData *data = malloc(sizeof(struct myUserData));
	data->out = out;
	data->options = options;
	data->result = MCC_SYNTH_STATUS_OK;
	return data;
}

static void freeUserData(struct myUserData *data)
{
	free(data);
}

enum mcc_sythesizeStatus
mcc_ast_synth_expression(FILE *out, struct mcc_ast_expression *expression, enum mcc_semanticOptions options)
{
	assert(out);
	assert(expression);
	struct myUserData *data = createUserData(out, options);
	struct mcc_ast_visitor visitor = synth_visitor(data);
	mcc_ast_visit(expression, &visitor);
	enum mcc_sythesizeStatus result = data->result;
	freeUserData(data);
	return result;
}

enum mcc_sythesizeStatus
mcc_ast_synth_literal(FILE *out, struct mcc_ast_literal *literal, enum mcc_semanticOptions options)
{
	assert(out);
	assert(literal);
	struct myUserData *data = createUserData(out, options);
	struct mcc_ast_visitor visitor = synth_visitor(data);
	mcc_ast_visit(literal, &visitor);
	enum mcc_sythesizeStatus result = data->result;
	freeUserData(data);
	return result;
}

enum mcc_sythesizeStatus
mcc_ast_synth_statement(FILE *out, struct mcc_ast_statement *statement, enum mcc_semanticOptions options)
{
	assert(out);
	assert(statement);
	struct myUserData *data = createUserData(out, options);
	struct mcc_ast_visitor visitor = synth_visitor(data);
	mcc_ast_visit(statement, &visitor);
	enum mcc_sythesizeStatus result = data->result;
	freeUserData(data);
	return result;
}

enum mcc_sythesizeStatus
mcc_ast_synth_start(FILE *out, struct mcc_ast_statement *statement, enum mcc_semanticOptions options, char *filename)
{
	globalfilename = filename;
	mcc_parser_error_ext(&statement->node.sloc, "synthesizing/checking tree\n", globalfilename, LOG_INFO);
	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec(statement->node.symbolTable, "main", MCC_SYMBOL_FUNC_ONLY);
	if (attr != NULL && attr->type == MCC_AST_TYPE_INT) {
		mcc_parser_error_ext(&statement->node.sloc, "main found with int returntype\n", globalfilename,
		                     LOG_INFO);
	} else {
		mcc_parser_error_ext(&statement->node.sloc, "main not found or wrong return type\n", globalfilename,
		                     LOG_ERROR);
		mcc_ast_synth(out, statement, options);
		return MCC_SYNTH_STATUS_ERROR;
	}
	return mcc_ast_synth(out, statement, options);
}
