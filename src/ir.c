#include "mcc/ir.h"
#include "mcc/list.h"
#include "mcc/log.h"
#include "vendor/map/map.h"

#include <assert.h>
#include <stdlib.h>

typedef struct {
	char *name;
	unsigned int index; // index of the code line pointing
} mcc_labels_t;

typedef struct mcc_ir {
	list_ptr_t code;
	map_t labels;
	unsigned int var_temp_counter;
	unsigned int label_temp_counter;
	list_ptr_t basic_blocks;
	map_t basic_block_4_labels;
	list_ptr_t literals;
} mcc_ir_t;

typedef struct {
	enum mcc_ir_op_type type;
	char *arg1;
	union {
		char *arg2;
		unsigned int arg2n;
	};
	char *result;
	struct mcc_symbolTable *tab;
} mcc_ir_tuple_t;

typedef mcc_ir_tuple_t *mcc_ir_tuple_ptr_t;

typedef struct mcc_ir_basic_block {
	mcc_ir_tuple_ptr_t start;
	mcc_ir_tuple_ptr_t end;
	mcc_ir_tuple_ptr_t successor;
	mcc_ir_tuple_ptr_t successor2;
} mcc_ir_basic_block_t;

typedef mcc_ir_basic_block_t *mcc_ir_basic_block_ptr_t;

static const unsigned int MAX_LINE_SIZE = 132;

static void print_dot_begin(FILE *out)
{
	assert(out);

	fprintf(out, "digraph \"AST\" {\n"
	             "\tnodesep=0.6\n");
}

static void print_dot_end(FILE *out)
{
	assert(out);

	fprintf(out, "}\n");
}

static void print_dot_node_open(FILE *out, const void *node)
{
	assert(out);
	assert(node);

	fprintf(out, "\t\"%p\" [shape=box, label=\"", node);
}

static void print_dot_node_close(FILE *out)
{
	assert(out);

	fprintf(out, "\"];\n");
}

static void print_dot_edge(FILE *out, const void *src_node, const void *dst_node, char *label)
{
	assert(out);
	assert(src_node);
	assert(dst_node);
	assert(label);

	fprintf(out, "\t\"%p\" -> \"%p\" [label=\"%s\"];\n", src_node, dst_node, label);
}

mcc_ir_ptr_t mcc_ir_createIR()
{
	mcc_ir_ptr_t ir = malloc(sizeof(struct mcc_ir));
	ir->code = list_create(LIST_OPTIONS_OWNER);
	ir->labels = mapNew();
	ir->var_temp_counter = 0;
	ir->label_temp_counter = 0;
	ir->basic_blocks = list_create(LIST_OPTIONS_OWNER);
	ir->basic_block_4_labels = mapNew();
	ir->literals = list_create(LIST_OPTIONS_OWNER);
	return ir;
}

char *mcc_ir_createLabelAtIdx(mcc_ir_ptr_t ir, char *name, unsigned int idx)
{
	unsigned int *copy = malloc(sizeof(unsigned int));
	*copy = idx;
	return mapDynAdd(name, copy, ir->labels);
}

char *mcc_ir_createLabel(mcc_ir_ptr_t ir, char *name)
{
	// create label at actual position
	return mcc_ir_createLabelAtIdx(ir, name, ir->code->size);
}

void mcc_ir_addOperationN(
    mcc_ir_ptr_t ir, enum mcc_ir_op_type type, char *arg1, unsigned int arg2, char *result, struct mcc_symbolTable *tab)
{
	mcc_ir_tuple_ptr_t tuple = malloc(sizeof(mcc_ir_tuple_t));
	tuple->type = type;
	tuple->arg1 = arg1;
	tuple->arg2n = arg2;
	tuple->result = result;

	tuple->tab = tab;

	list_append(ir->code, tuple);
}

void mcc_ir_addOperation(
    mcc_ir_ptr_t ir, enum mcc_ir_op_type type, char *arg1, char *arg2, char *result, struct mcc_symbolTable *tab)
{
	mcc_ir_tuple_ptr_t tuple = malloc(sizeof(mcc_ir_tuple_t));
	tuple->type = type;
	tuple->arg1 = arg1;
	tuple->arg2 = arg2;
	tuple->result = result;

	tuple->tab = tab;

	list_append(ir->code, tuple);
}

void mcc_ir_addOperationReturn(mcc_ir_ptr_t ir, char *returnvalue, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_RETURN, returnvalue, NULL, NULL, tab);
}

void mcc_ir_addOperationUNeg(mcc_ir_ptr_t ir, char *from, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_UN_NEGATIVE, from, NULL, to, tab);
}

void mcc_ir_addOperationUNot(mcc_ir_ptr_t ir, char *from, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_UN_NEGATION, from, NULL, to, tab);
}

void mcc_ir_addOperationBAdd(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_BI_ADDITION, op1, op2, to, tab);
}

void mcc_ir_addOperationBSub(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_BI_SUBRACTION, op1, op2, to, tab);
}

void mcc_ir_addOperationBMul(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_BI_MULTIPLICATION, op1, op2, to, tab);
}

void mcc_ir_addOperationBDiv(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_BI_DIVISION, op1, op2, to, tab);
}

void mcc_ir_addOperationBAnd(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_BI_LOG_AND, op1, op2, to, tab);
}

void mcc_ir_addOperationBOr(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_BI_LOG_OR, op1, op2, to, tab);
}

void mcc_ir_addOperationBEq(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_BI_EQ, op1, op2, to, tab);
}

void mcc_ir_addOperationBL(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_BI_LO, op1, op2, to, tab);
}

void mcc_ir_addOperationCopy(mcc_ir_ptr_t ir, char *from, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_COPY, from, NULL, to, tab);
}

void mcc_ir_addOperationJump(mcc_ir_ptr_t ir, char *label, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_JUMP, label, NULL, NULL, tab);
}

void mcc_ir_addOperationIfNotJump(mcc_ir_ptr_t ir, char *label, char *false_cond, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_IF_NOT_JUMP, label, false_cond, NULL, tab);
}

void mcc_ir_addOperationLabel(mcc_ir_ptr_t ir, char *label, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_LABEL, label, NULL, NULL, tab);
}

void mcc_ir_addOperationPrepParam(mcc_ir_ptr_t ir, char *parameter, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_PREP_PARAM, parameter, NULL, NULL, tab);
}

void mcc_ir_addOperationCall(mcc_ir_ptr_t ir,
                             char *function,
                             unsigned int parameterNumbers,
                             struct mcc_symbolTable *tab) // Is this correct?
{
	mcc_ir_addOperationN(ir, MCC_IR_OP_TYPE_CALL, function, parameterNumbers, NULL, tab);
}

void mcc_ir_addOperationLoad(mcc_ir_ptr_t ir, char *from, char *index, char *to, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_LOAD, from, index, to, tab);
}

void mcc_ir_addOperationStore(mcc_ir_ptr_t ir, char *from, char *index, char *where, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_STORE, from, index, where, tab);
}

void mcc_ir_addOperationOpenBlock(mcc_ir_ptr_t ir, char *blockName, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_OPEN_BLOCK, blockName, NULL, NULL, tab);
}

void mcc_ir_addOperationCloseBlock(mcc_ir_ptr_t ir, char *blockName, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_CLOSE_BLOCK, blockName, NULL, NULL, tab);
}

void mcc_ir_addOperationBuildArray(mcc_ir_ptr_t ir, char *varName, struct mcc_symbolTable *tab)
{
	mcc_ir_addOperation(ir, MCC_IR_OP_TYPE_BUILD_ARRAY, varName, NULL, NULL, tab);
}

char *mcc_ir_createTemporaryLabel(mcc_ir_ptr_t ir, char *name, int max, const char *template)
{
	snprintf(name, max, "_%s%u", template, ir->label_temp_counter++);
	return mapAdd(name, &name, ir->labels);
}

void mcc_ir_createTemporaryVariable(mcc_ir_ptr_t ir, char *name, int max, char *template)
{
	snprintf(name, max, "_%s%u", template, ir->var_temp_counter++);
}

char *mcc_ir_createTemporary(mcc_ir_ptr_t ir, struct mcc_symbolTable *table, enum mcc_ast_type_type type)
{
	return mcc_ir_createTemporaryTemplate(ir, table, type, "temp");
}

char *mcc_ir_createTemporaryTemplate(mcc_ir_ptr_t ir,
                                     struct mcc_symbolTable *table,
                                     enum mcc_ast_type_type type,
                                     char *template)
{
	const int tempVarNameLen = 30;

	struct mcc_symbolAttributes *attr = malloc(sizeof(struct mcc_symbolAttributes));
	attr->type = type;
	attr->array_size = 0;
	attr->parameter_type_list = NULL;
	attr->isParameter = 0;
	attr->negativeStackOffset = 0;
	char name[tempVarNameLen];
	mcc_ir_createTemporaryVariable(ir, name, tempVarNameLen, template);

	return mcc_symbTaddSymbolVariable(table, name, attr);
}

static void ir_string_line(mcc_ir_tuple_ptr_t tuple, char *dest)
{
	switch (tuple->type) {
	case MCC_IR_OP_TYPE_COPY:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s", tuple->result, tuple->arg1);
		break;
	case MCC_IR_OP_TYPE_JUMP:
		snprintf(dest, MAX_LINE_SIZE, "JUMP %s", tuple->arg1);
		break;
	case MCC_IR_OP_TYPE_IF_NOT_JUMP:
		snprintf(dest, MAX_LINE_SIZE, "JUMP TO %s IF NOT %s", tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_LABEL:
		snprintf(dest, MAX_LINE_SIZE, "%s:", tuple->arg1);
		break;
	case MCC_IR_OP_TYPE_PREP_PARAM:
		snprintf(dest, MAX_LINE_SIZE, "PARAM %s:", tuple->arg1);
		break;
	case MCC_IR_OP_TYPE_CALL:
		snprintf(dest, MAX_LINE_SIZE, "CALL %s,%u", tuple->arg1, tuple->arg2n);
		break;
	case MCC_IR_OP_TYPE_LOAD:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s[%s]", tuple->result, tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_STORE:
		snprintf(dest, MAX_LINE_SIZE, "%s[%s] = %s", tuple->result, tuple->arg2, tuple->arg1);
		break;
	case MCC_IR_OP_TYPE_UN_NEGATIVE:
		snprintf(dest, MAX_LINE_SIZE, "%s = -%s", tuple->result, tuple->arg1);
		break;
	case MCC_IR_OP_TYPE_UN_NEGATION:
		snprintf(dest, MAX_LINE_SIZE, "%s = !%s", tuple->result, tuple->arg1);
		break;
	case MCC_IR_OP_TYPE_BI_ADDITION:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s+%s", tuple->result, tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_BI_SUBRACTION:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s-%s", tuple->result, tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_BI_MULTIPLICATION:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s*%s", tuple->result, tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_BI_DIVISION:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s/%s", tuple->result, tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_BI_LOG_AND:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s&&%s", tuple->result, tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_BI_LOG_OR:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s||%s", tuple->result, tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_BI_EQ:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s==%s", tuple->result, tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_BI_LO:
		snprintf(dest, MAX_LINE_SIZE, "%s = %s<%s", tuple->result, tuple->arg1, tuple->arg2);
		break;
	case MCC_IR_OP_TYPE_OPEN_BLOCK:
		snprintf(dest, MAX_LINE_SIZE, "OPEN BLOCK %s", tuple->arg1);
		break;
	case MCC_IR_OP_TYPE_CLOSE_BLOCK:
		snprintf(dest, MAX_LINE_SIZE, "CLOSE BLOCK %s", tuple->arg1);
		break;
	case MCC_IR_OP_TYPE_RETURN:
		if (tuple->arg1 != NULL)
			snprintf(dest, MAX_LINE_SIZE, "RETURN %s", tuple->arg1);
		else
			snprintf(dest, MAX_LINE_SIZE, "RETURN");
		break;
	case MCC_IR_OP_TYPE_BUILD_ARRAY:
		snprintf(dest, MAX_LINE_SIZE, "ARRAY \"%s\"", tuple->arg1);
		break;
	}
}

void mcc_ir_print_line(void *data, void *arg)
{
	FILE *out = arg;
	mcc_ir_tuple_ptr_t tuple = data;
	char string[MAX_LINE_SIZE];
	ir_string_line(tuple, string);
	fprintf(out, "%s\n", string);
}

void mcc_ir_print(mcc_ir_ptr_t ir, FILE *out)
{
	list_iterate(ir->code, mcc_ir_print_line, out);
}

static mcc_ir_basic_block_ptr_t ir_create_asdf(mcc_ir_ptr_t ir, mcc_ir_tuple_ptr_t start, char *name)
{
	mcc_ir_basic_block_ptr_t bb = malloc(sizeof(mcc_ir_basic_block_t));
	bb->end = NULL;
	bb->start = start;
	bb->successor = NULL;
	bb->successor2 = NULL;

	list_append(ir->basic_blocks, bb);
	if (name != NULL)
		mapAdd(name, bb->start, ir->basic_block_4_labels);

	return bb;
}

static void ir_find_bbs(void *data, void *arg)
{
	mcc_ir_ptr_t ir = arg;
	mcc_ir_tuple_ptr_t tuple = data;

	static mcc_ir_basic_block_ptr_t lastBlock = NULL;
	// if (ir->basic_blocks->size == 0) {
	// 	lastBlock = ir_create_asdf(ir, tuple);
	// }
	char *name = NULL;
	switch (tuple->type) {
	case MCC_IR_OP_TYPE_LABEL:
		name = tuple->arg1;
		if (lastBlock != NULL && lastBlock->start == tuple) {
			// if last opend block is already this labeled block return
			mapAdd(name, lastBlock->start, ir->basic_block_4_labels);
			return;
		}
		if (lastBlock != NULL) {
			lastBlock->end = list_get(ir->code, list_find(ir->code, tuple) - 1);
		}
		lastBlock = ir_create_asdf(ir, tuple, name);
		return;
	case MCC_IR_OP_TYPE_JUMP:
		break;
	case MCC_IR_OP_TYPE_IF_NOT_JUMP:
		break;
	// case MCC_IR_OP_TYPE_CALL:
	// 	break;
	default:
		return;
	}
	if (lastBlock != NULL) {
		lastBlock->end = tuple;
	}
	lastBlock = ir_create_asdf(ir, list_get(ir->code, list_find(ir->code, tuple) + 1), name);
}

static void ir_build_bb_successors(void *data, void *arg)
{
	mcc_ir_basic_block_ptr_t bb = data;
	mcc_ir_ptr_t ir = arg;
	switch (bb->end->type) {
	// case MCC_IR_OP_TYPE_LABEL:
	// 	name = tuple->arg1;
	// 	break;
	case MCC_IR_OP_TYPE_IF_NOT_JUMP:
		// add next statement as successor
		bb->successor2 = list_get(ir->code, list_find(ir->code, bb->end) + 1);
		__attribute__((fallthrough));
	case MCC_IR_OP_TYPE_JUMP:
		bb->successor = mapGet(bb->end->arg1, ir->basic_block_4_labels);
		break;
	case MCC_IR_OP_TYPE_RETURN:
		bb->successor = NULL;
		break;
	default:
		// add next statement as successor
		bb->successor = list_get(ir->code, list_find(ir->code, bb->end) + 1);
		break;
	}
}

void mcc_ir_build_bb(mcc_ir_ptr_t ir)
{
	list_iterate(ir->code, ir_find_bbs, ir);
	mcc_ir_basic_block_ptr_t lastBlock = list_get(ir->basic_blocks, ir->basic_blocks->size - 1);
	lastBlock->end = list_get(ir->code, ir->code->size - 1);

	list_iterate(ir->basic_blocks, ir_build_bb_successors, ir);
}

static void ir_print_dot_bb(void *data, void *arg)
{
	void **args = arg;
	mcc_ir_ptr_t ir = args[0];
	FILE *out = args[1];
	mcc_ir_basic_block_ptr_t bb = data;
	print_dot_node_open(out, bb->start);

	// print all the code between start and end
	list_iterate_idx(ir->code, list_find(ir->code, bb->start), list_find(ir->code, bb->end) + 1, mcc_ir_print_line,
	                 out);

	print_dot_node_close(out);

	if (bb->successor != NULL) {
		print_dot_edge(out, bb->start, bb->successor, "");
	}
	if (bb->successor2 != NULL) {
		print_dot_edge(out, bb->start, bb->successor2, "");
	}
}

void mcc_ir_print_cfg(mcc_ir_ptr_t ir, FILE *out)
{
	print_dot_begin(out);
	void *args[2] = {ir, out};
	list_iterate(ir->basic_blocks, ir_print_dot_bb, args);
	print_dot_end(out);
}

void mcc_ir_delete(mcc_ir_ptr_t ir)
{
	list_free(ir->code);
	mapClose(ir->labels);
	list_free(ir->basic_blocks);
	mapClose(ir->basic_block_4_labels);
	list_free(ir->literals);
	free(ir);
}

void mcc_ir_addLiteral(mcc_ir_ptr_t ir, char *literal)
{
	list_append(ir->literals, literal);
}

static mcc_ir_it_fun_t actual_fun;

static void ir_iterate(void *data, void *arg)
{
	mcc_ir_tuple_ptr_t tuple = data;
	actual_fun(tuple->type, tuple->arg1, tuple->arg2, tuple->arg2n, tuple->result, tuple->tab, arg);
}

void mcc_ir_iterate(mcc_ir_ptr_t ir, mcc_ir_it_fun_t function, void *arg)
{
	actual_fun = function;
	list_iterate(ir->code, ir_iterate, arg);
}

struct mcc_symbolTable *mcc_ir_find_base_symbolTable(mcc_ir_ptr_t ir)
{
	mcc_ir_tuple_ptr_t first = list_get(ir->code, 0);
	return first->tab;
}