#include <assert.h>
#include <stdlib.h>

#include "mcc/ast_symb.h"
#include "mcc/ast_visit.h"
#include "mcc/log.h"
#include "mcc/parser.h"
#include "mcc/symbol_table.h"
#include "utils/unused.h"

// ---------------------------------------------------------------- DOT Printer

static char *globalfilename = "";

struct myUserData {
	FILE *out;
	struct mcc_symbolTable *actualTable;
	list_t *allSymbolTableList;
	enum mcc_symbolTableOptions options;
	enum mcc_symbolTableStatus result;
	int paramCounter;
};

#define MESSAGE_SIZE 100

static struct mcc_symbolAttributes *symb_check_existing_symbol(struct mcc_symbolTable *actualSymbolTable,
                                                               struct mcc_ast_expression *expression,
                                                               enum mcc_symbolTableStatus *res)
{

	enum mcc_symbolSearch searchCall = MCC_SYMBOL_ALL;
	char *symbol;
	struct mcc_symbolAttributes *attr = NULL;
	switch (expression->type) {
	case MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS:
		symbol = expression->identifier;
		searchCall = MCC_SYMBOL_VARIABLE_ONLY;
		break;
	case MCC_AST_EXPREESION_TYPE_FUNCTION_CALL:
		symbol = expression->callname;
		searchCall = MCC_SYMBOL_FUNC_ONLY;
		break;
	default:
		return attr;
	}

	if ((attr = mcc_symbTlookupRec(actualSymbolTable, symbol, searchCall)) == NULL) {
		// const char* msgT = "Variable %s already declared\n";
		*res = MCC_SYMBT_STATUS_ERROR;
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "Symbol `%s` not yet declared\n", symbol);
		mcc_parser_error_ext(&expression->node.sloc, msg, globalfilename, LOG_ERROR);
		// free(msg);
	}
	return attr;
}

static int symb_check_dupl_symbol(struct mcc_symbolTable *actualSymbolTable,
                                  struct mcc_ast_statement *statement,
                                  enum mcc_symbolTableStatus *res)
{

	assert(statement);

	// printf("checking symbol %d\n", statement->type);
	enum mcc_symbolSearch searchCall = MCC_SYMBOL_ALL;
	char *symbol;
	switch (statement->type) {
	case MCC_AST_STATEMENT_FUNC_DECLARATION:
		symbol = statement->functionName;
		searchCall = MCC_SYMBOL_FUNC_ONLY;
		break;
	case MCC_AST_STATEMENT_TYPE_DECLARATION:
	case MCC_AST_STATEMENT_ARRAY_DECLARATION:
		symbol = statement->identifier;
		searchCall = MCC_SYMBOL_VARIABLE_ONLY;
		break;
	default:
		return 0;
	}
	if (mcc_symbTlookup(actualSymbolTable, symbol, searchCall) != NULL) {
		// const char* msgT = "Variable %s already declared\n";
		*res = MCC_SYMBT_STATUS_ERROR;
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "Symbol `%s` already declared\n", symbol);
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_ERROR);
		// free(msg);
		return 1;
	}
	return 0;
}

static void
symb_add_symbol(struct mcc_symbolTable *actualSymbolTable, struct mcc_ast_statement *statement, int *paramCounter)
{
	// printf("add symbol\n");
	char *symbol;
	struct mcc_symbolAttributes *attr = malloc(sizeof(struct mcc_symbolAttributes));
	attr->array_size = 0;
	attr->isParameter = 0;
	attr->negativeStackOffset = 0;
	switch (statement->type) {
	case MCC_AST_STATEMENT_FUNC_DECLARATION:
		symbol = statement->functionName;
		attr->type = statement->returnType;
		attr->parameter_type_list = list_create(LIST_OPTIONS_OWNER);
		struct mcc_ast_statement *param = statement->functionParameters;
		while (param != NULL) {
			struct mcc_symbolAttributes *paramAttr = malloc(sizeof(struct mcc_symbolAttributes));
			paramAttr->array_size = param->size == NULL ? 0 : param->size->i_value;
			paramAttr->type = param->declType;
			paramAttr->parameter_type_list = NULL;
			paramAttr->isParameter = 1;
			paramAttr->negativeStackOffset = 0;
			list_append(attr->parameter_type_list, paramAttr);
			param = param->right_neighbor;
		}
		*paramCounter = attr->parameter_type_list->size;
		mcc_symbTaddSymbolFunction(actualSymbolTable, symbol, attr);
		{
			char msg[MESSAGE_SIZE];
			snprintf(msg, MESSAGE_SIZE, "Function `%s` added to table %p\n", symbol, (void *)actualSymbolTable);
			mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_INFO);
		}
		break;
	case MCC_AST_STATEMENT_ARRAY_DECLARATION:
		attr->array_size = statement->size->i_value;
		__attribute__((fallthrough));
	case MCC_AST_STATEMENT_TYPE_DECLARATION:
		symbol = statement->identifier;
		attr->type = statement->declType;
		attr->parameter_type_list = NULL;
		if (*paramCounter > 0) {
			attr->isParameter = 1;
			(*paramCounter)--;
		}
		mcc_symbTaddSymbolVariable(actualSymbolTable, symbol, attr);
		{
			char msg[MESSAGE_SIZE];
			snprintf(msg, MESSAGE_SIZE, "Variable `%s` added to table %p\n", symbol, (void *)actualSymbolTable);
			mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_INFO);
		}
		break;
	default:
		free(attr);
		return;
	}
}

static void symb_statement(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	struct myUserData *myData = data;

	statement->node.symbolTable = myData->actualTable;
}

static void symb_expression(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	struct myUserData *myData = data;

	expression->node.symbolTable = myData->actualTable;
}

static void symb_literal(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);

	struct myUserData *myData = data;

	literal->node.symbolTable = myData->actualTable;
}

static void symb_statement_decl(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	struct myUserData *myData = data;
	if (!symb_check_dupl_symbol(myData->actualTable, statement, &myData->result))
		symb_add_symbol(myData->actualTable, statement, &myData->paramCounter);
}

static void symb_statement_decl_array(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	struct myUserData *myData = data;
	if (!symb_check_dupl_symbol(myData->actualTable, statement, &myData->result))
		symb_add_symbol(myData->actualTable, statement, &myData->paramCounter);
}

static void symb_statement_func_decl(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	struct myUserData *myData = data;
	if (!symb_check_dupl_symbol(myData->actualTable, statement, &myData->result))
		symb_add_symbol(myData->actualTable, statement, &myData->paramCounter);

	// increase scope
	myData->actualTable = mcc_symbTcreate(myData->actualTable, statement->functionName);
	mcc_symbTsetFunction(myData->actualTable,
	                     mcc_symbTlookupRec(myData->actualTable, statement->functionName, MCC_SYMBOL_FUNC_ONLY));
	list_append(myData->allSymbolTableList, myData->actualTable);
	{
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "Closing function scope %p for %s\n", (void *)myData->actualTable,
		         statement->functionName);
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_DEBUG);
	}
}

static void symb_statement_block(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	struct myUserData *myData = data;
	static int blockcnt = 0;

	snprintf(statement->blockName, 29, "_BLOCK%d", blockcnt++);
	// increase scope
	myData->actualTable = mcc_symbTcreate(myData->actualTable, statement->blockName);
	list_append(myData->allSymbolTableList, myData->actualTable);

	{
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "Opening block scope %p\n", (void *)myData->actualTable);
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_DEBUG);
	}
}

static void symb_statement_block_leave(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	struct myUserData *myData = data;

	{
		char msg[MESSAGE_SIZE];
		snprintf(msg, MESSAGE_SIZE, "Closing scope of table %p\n", (void *)myData->actualTable);
		mcc_parser_error_ext(&statement->node.sloc, msg, globalfilename, LOG_DEBUG);
	}

	// decrease scope
	if (myData->options & MCC_SYMBT_OPTION_PRINT_SYMBOL_TABLE)
		mcc_symbTprint(myData->actualTable, myData->out);
	myData->actualTable = mcc_symbTgetParent(myData->actualTable);
}

static void symb_var_access(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	struct myUserData *myData = data;

	symb_check_existing_symbol(myData->actualTable, expression, &myData->result);
}

// Setup an AST Visitor for synthing.
static struct mcc_ast_visitor symb_visitor(struct myUserData *data)
{
	assert(data);

	return (struct mcc_ast_visitor){.traversal = MCC_AST_VISIT_DEPTH_FIRST,
	                                .order = MCC_AST_VISIT_PRE_ORDER,

	                                .userdata = data,

	                                .literal = symb_literal,

	                                .statement = symb_statement,
	                                .statement_decl = symb_statement_decl,
	                                .statement_decl_array = symb_statement_decl_array,
	                                .statement_func_decl = symb_statement_func_decl,
	                                .statement_block = symb_statement_block,
	                                .statement_block_leave = symb_statement_block_leave,

	                                .expression = symb_expression,
	                                .expression_variable_access = symb_var_access

	};
}

static void mcc_ast_symb_addDefaultFunctions(struct mcc_symbolTable *table)
{
	struct mcc_symbolAttributes *funAttr = malloc(sizeof(struct mcc_symbolAttributes));
	funAttr->array_size = 0;
	funAttr->type = MCC_AST_TYPE_VOID;
	funAttr->parameter_type_list = list_create(LIST_OPTIONS_OWNER);
	funAttr->isParameter = 0;
	funAttr->negativeStackOffset = 0;
	struct mcc_symbolAttributes *paramAttr = malloc(sizeof(struct mcc_symbolAttributes));
	paramAttr->array_size = 0;
	paramAttr->type = MCC_AST_TYPE_STRING;
	paramAttr->parameter_type_list = NULL;
	paramAttr->negativeStackOffset = 0;
	paramAttr->isParameter = 1;
	list_append(funAttr->parameter_type_list, paramAttr);
	mcc_symbTaddSymbolFunction(table, "print", funAttr);

	funAttr = malloc(sizeof(struct mcc_symbolAttributes));
	funAttr->type = MCC_AST_TYPE_VOID;
	funAttr->parameter_type_list = list_create(LIST_OPTIONS_OWNER);
	funAttr->isParameter = 0;
	funAttr->negativeStackOffset = 0;
	mcc_symbTaddSymbolFunction(table, "print_nl", funAttr);

	funAttr = malloc(sizeof(struct mcc_symbolAttributes));
	funAttr->type = MCC_AST_TYPE_VOID;
	funAttr->parameter_type_list = list_create(LIST_OPTIONS_OWNER);
	funAttr->isParameter = 0;
	funAttr->negativeStackOffset = 0;
	paramAttr = malloc(sizeof(struct mcc_symbolAttributes));
	paramAttr->type = MCC_AST_TYPE_INT;
	paramAttr->array_size = 0;
	paramAttr->parameter_type_list = NULL;
	paramAttr->negativeStackOffset = 0;
	paramAttr->isParameter = 1;
	list_append(funAttr->parameter_type_list, paramAttr);
	mcc_symbTaddSymbolFunction(table, "print_int", funAttr);

	funAttr = malloc(sizeof(struct mcc_symbolAttributes));
	funAttr->type = MCC_AST_TYPE_VOID;
	funAttr->parameter_type_list = list_create(LIST_OPTIONS_OWNER);
	funAttr->isParameter = 0;
	funAttr->negativeStackOffset = 0;
	paramAttr = malloc(sizeof(struct mcc_symbolAttributes));
	paramAttr->type = MCC_AST_TYPE_FLOAT;
	paramAttr->array_size = 0;
	paramAttr->parameter_type_list = NULL;
	paramAttr->negativeStackOffset = 0;
	paramAttr->isParameter = 1;
	list_append(funAttr->parameter_type_list, paramAttr);
	mcc_symbTaddSymbolFunction(table, "print_float", funAttr);

	funAttr = malloc(sizeof(struct mcc_symbolAttributes));
	funAttr->type = MCC_AST_TYPE_INT;
	funAttr->parameter_type_list = list_create(LIST_OPTIONS_OWNER);
	funAttr->isParameter = 0;
	funAttr->negativeStackOffset = 0;
	mcc_symbTaddSymbolFunction(table, "read_int", funAttr);

	funAttr = malloc(sizeof(struct mcc_symbolAttributes));
	funAttr->type = MCC_AST_TYPE_FLOAT;
	funAttr->parameter_type_list = list_create(LIST_OPTIONS_OWNER);
	funAttr->isParameter = 0;
	funAttr->negativeStackOffset = 0;
	mcc_symbTaddSymbolFunction(table, "read_float", funAttr);
}

static struct myUserData *mcc_ast_symb_createUserData(FILE *out, enum mcc_symbolTableOptions options)
{
	struct myUserData *data = malloc(sizeof(struct myUserData));
	data->out = out;
	data->actualTable = mcc_symbTcreateRoot("ROOT");
	data->allSymbolTableList = list_create(0);
	list_append(data->allSymbolTableList, data->actualTable);
	mcc_ast_symb_addDefaultFunctions(data->actualTable);
	data->options = options;
	data->result = MCC_SYMBT_STATUS_OK;
	data->paramCounter = 0;
	return data;
}

static enum mcc_symbolTableStatus mcc_ast_symb_freeUserData(struct myUserData *data)
{
	enum mcc_symbolTableStatus result = data->result;
	if (data->options & MCC_SYMBT_OPTION_PRINT_SYMBOL_TABLE) {
		mcc_symbTprint(data->actualTable, data->out);
	}
	/*mcc_symbTdelete(data->actualTable); */
	free(data);
	return result;
}

enum mcc_symbolTableStatus mcc_ast_symb_expression(FILE *out,
                                                   struct mcc_ast_expression *expression,
                                                   enum mcc_symbolTableOptions options,
                                                   list_t **allSymbolTableList)
{
	assert(out);
	assert(expression);

	struct myUserData *data = mcc_ast_symb_createUserData(out, options);
	struct mcc_ast_visitor visitor = symb_visitor(data);
	mcc_ast_visit(expression, &visitor);
	*allSymbolTableList = data->allSymbolTableList;
	return mcc_ast_symb_freeUserData(data);
}

enum mcc_symbolTableStatus mcc_ast_symb_literal(FILE *out,
                                                struct mcc_ast_literal *literal,
                                                enum mcc_symbolTableOptions options,
                                                list_t **allSymbolTableList)
{
	assert(out);
	assert(literal);

	struct myUserData *data = mcc_ast_symb_createUserData(out, options);
	struct mcc_ast_visitor visitor = symb_visitor(data);
	mcc_ast_visit(literal, &visitor);
	*allSymbolTableList = data->allSymbolTableList;
	return mcc_ast_symb_freeUserData(data);
}

enum mcc_symbolTableStatus mcc_ast_symb_statement(FILE *out,
                                                  struct mcc_ast_statement *statement,
                                                  enum mcc_symbolTableOptions options,
                                                  list_t **allSymbolTableList)
{
	assert(out);
	assert(statement);

	struct myUserData *data = mcc_ast_symb_createUserData(out, options);
	struct mcc_ast_visitor visitor = symb_visitor(data);
	mcc_ast_visit(statement, &visitor);
	*allSymbolTableList = data->allSymbolTableList;
	return mcc_ast_symb_freeUserData(data);
}

enum mcc_symbolTableStatus mcc_ast_symb_start(FILE *out,
                                              struct mcc_ast_statement *statement,
                                              enum mcc_symbolTableOptions options,
                                              char *filename,
                                              list_t **allSymbolTableList)
{
	globalfilename = filename;
	mcc_parser_error_ext(&statement->node.sloc, "running symbol table on tree\n", globalfilename, LOG_INFO);
	return mcc_ast_symb(out, statement, options, allSymbolTableList);
}

static void mcc_ast_symb_free_symbol_list(void *data, void *arg)
{
	UNUSED(arg);
	struct mcc_symbolTable *tab = data;
	mcc_symbTdelete(tab);
}

void mcc_ast_symb_clear_symTab(list_t *allSymbolTableList)
{
	list_iterate(allSymbolTableList, mcc_ast_symb_free_symbol_list, NULL);
	list_free(allSymbolTableList);
}
