#include "mcc/ast_print.h"

#include <assert.h>

#include "mcc/ast_visit.h"

const char *mcc_ast_print_binary_op(enum mcc_ast_binary_op op)
{
	switch (op) {
	case MCC_AST_BINARY_OP_ADD:
		return "+";
	case MCC_AST_BINARY_OP_SUB:
		return "-";
	case MCC_AST_BINARY_OP_MUL:
		return "*";
	case MCC_AST_BINARY_OP_DIV:
		return "/";
	case MCC_AST_BINARY_OP_EQU:
		return "==";
	case MCC_AST_BINARY_OP_NEQ:
		return "!=";
	case MCC_AST_BINARY_OP_LES:
		return "<";
	case MCC_AST_BINARY_OP_GRT:
		return ">";
	case MCC_AST_BINARY_OP_LEQ:
		return "<=";
	case MCC_AST_BINARY_OP_GEQ:
		return ">=";
	case MCC_AST_BINARY_OP_AND:
		return "&&";
	case MCC_AST_BINARY_OP_OR:
		return "||";
	case MCC_AST_BINARY_SEPERATE:
		return ",";
	}

	return "unknown op";
}

const char *mcc_ast_print_type(enum mcc_ast_type_type type)
{
	switch (type) {
	case MCC_AST_TYPE_UNKNOWN:
		return "UNKNOWN TYPE";
	case MCC_AST_TYPE_BOOL:
		return "BOOL";
	case MCC_AST_TYPE_INT:
		return "INT";
	case MCC_AST_TYPE_FLOAT:
		return "FLOAT";
	case MCC_AST_TYPE_VOID:
		return "VOID";
	case MCC_AST_TYPE_STRING:
		return "STRING";
	case MCC_AST_TYPE_BOOL_ARRAY:
		return "BOOL[]";
	case MCC_AST_TYPE_INT_ARRAY:
		return "INT[]";
	case MCC_AST_TYPE_FLOAT_ARRAY:
		return "FLOAT[]";
	}

	return "unknown type";
}

const char *mcc_ast_print_unary_op(enum mcc_ast_unary_op op)
{
	switch (op) {
	case MCC_AST_UNARY_OP_NEG:
		return "-";
	case MCC_AST_UNARY_OP_NOT:
		return "!";
	}
	return "unknown op";
}

// ---------------------------------------------------------------- DOT Printer

#define LABEL_SIZE 64

static void print_dot_begin(FILE *out)
{
	assert(out);

	fprintf(out, "digraph \"AST\" {\n"
	             "\tnodesep=0.6\n");
}

static void print_dot_end(FILE *out)
{
	assert(out);

	fprintf(out, "}\n");
}

static void print_dot_node(FILE *out, const void *node, const char *label)
{
	assert(out);
	assert(node);
	assert(label);

	fprintf(out, "\t\"%p\" [shape=box, label=\"%s\"];\n", node, label);
}

static void print_dot_edge(FILE *out, const void *src_node, const void *dst_node, const char *label)
{
	assert(out);
	assert(src_node);
	assert(dst_node);
	assert(label);

	fprintf(out, "\t\"%p\" -> \"%p\" [label=\"%s\"];\n", src_node, dst_node, label);
}

static void print_dot_neighbor_edge(FILE *out, const void *src_node, const void *dst_node)
{
	assert(out);
	assert(src_node);
	assert(dst_node);

	fprintf(out, "\t\"%p\" -> \"%p\" [style=dotted];\n", src_node, dst_node);
	fprintf(out, "{ rank=same; \"%p\" \"%p\" }\n", src_node, dst_node);
}

static void print_dot_expression(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	FILE *out = data;
	if (expression->right_neighbor != NULL)
		print_dot_neighbor_edge(out, expression, expression->right_neighbor);
}

static void print_dot_expression_literal(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	FILE *out = data;
	print_dot_node(out, expression, "expr: lit");
	print_dot_edge(out, expression, expression->literal, "literal");
}

static void print_dot_expression_binary_op(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	char label[LABEL_SIZE] = {0};
	snprintf(label, sizeof(label), "expr: %s", mcc_ast_print_binary_op(expression->op));

	FILE *out = data;
	print_dot_node(out, expression, label);
	print_dot_edge(out, expression, expression->lhs, "lhs");
	print_dot_edge(out, expression, expression->rhs, "rhs");
}

static void print_dot_expression_unary_op(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	char label[LABEL_SIZE] = {0};
	snprintf(label, sizeof(label), "expr: %s", mcc_ast_print_unary_op(expression->unary_op));

	FILE *out = data;
	print_dot_node(out, expression, label);
	print_dot_edge(out, expression, expression->unary_next, "next");
}

static void print_dot_expression_parenth(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	FILE *out = data;
	print_dot_node(out, expression, "( )");
	print_dot_edge(out, expression, expression->expression, "expression");
}

static void print_dot_expression_variable_access(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	FILE *out = data;
	print_dot_node(out, expression, "access");
	print_dot_edge(out, expression, expression->identifier, "ID");
	print_dot_node(out, expression->identifier, expression->identifier);

	if (expression->index != NULL)
		print_dot_edge(out, expression, expression->index, "index");
}

static void print_dot_expression_function_call(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	FILE *out = data;
	print_dot_node(out, expression, "FUNC_CALL");
	print_dot_edge(out, expression, expression->callname, "callname");
	if (expression->functionarguments != NULL)
		print_dot_edge(out, expression, expression->functionarguments, "Arguments");
	print_dot_node(out, expression->callname, expression->callname);
}

static void print_dot_literal_int(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);

	char label[LABEL_SIZE] = {0};
	snprintf(label, sizeof(label), "%ld", literal->i_value);

	FILE *out = data;
	print_dot_node(out, literal, label);
}

static void print_dot_literal_float(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);

	char label[LABEL_SIZE] = {0};
	snprintf(label, sizeof(label), "%f", literal->f_value);

	FILE *out = data;
	print_dot_node(out, literal, label);
}

static void print_dot_literal_string(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);

	char label[LABEL_SIZE] = {0};
	snprintf(label, sizeof(label), "%s", literal->s_value + 1);

	FILE *out = data;
	print_dot_node(out, literal, label);
}

static void print_dot_literal_bool(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);

	char label[LABEL_SIZE] = {0};
	snprintf(label, sizeof(label), "%d", literal->b_value);

	FILE *out = data;
	print_dot_node(out, literal, label);
}

static void print_dot_statement(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;
	if (statement->right_neighbor != NULL)
		print_dot_neighbor_edge(out, statement, statement->right_neighbor);
}

static void print_dot_statement_decl(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;

	char label[LABEL_SIZE] = {0};
	snprintf(label, sizeof(label), "%s DECL", mcc_ast_print_type(statement->declType));
	print_dot_node(out, statement, label);
	print_dot_edge(out, statement, statement->identifier, "name");
	print_dot_node(out, statement->identifier, statement->identifier);
	if (statement->initialValue != NULL)
		print_dot_edge(out, statement, statement->initialValue, "initValue");
}

static void print_dot_statement_decl_array(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;
	char label[LABEL_SIZE] = {0};
	snprintf(label, sizeof(label), "%s ARRAY", mcc_ast_print_type(statement->declType));
	print_dot_node(out, statement, label);
	print_dot_edge(out, statement, statement->identifier, "name");
	print_dot_edge(out, statement, statement->size, "size");
	print_dot_node(out, statement->identifier, statement->identifier);
}

static void print_dot_statement_if(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;
	print_dot_node(out, statement, "IF");
	print_dot_edge(out, statement, statement->condition, "If Condition");
	print_dot_edge(out, statement, statement->truepath, "truepath");
	if (statement->falsepath != NULL)
		print_dot_edge(out, statement, statement->falsepath, "falsepath");
}

static void print_dot_statement_while(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;
	print_dot_node(out, statement, "WHILE");
	print_dot_edge(out, statement, statement->whilecondition, "While Condition");
	print_dot_edge(out, statement, statement->whilebody, "whilebody");
}

static void print_dot_statement_return(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;
	print_dot_node(out, statement, "RETURN");
	if (statement->returnExpression != NULL)
		print_dot_edge(out, statement, statement->returnExpression, "Return expression");
}

static void print_dot_statement_func_decl(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;
	char label[LABEL_SIZE] = {0};
	snprintf(label, sizeof(label), "%s FUNC_DECL", mcc_ast_print_type(statement->returnType));
	print_dot_node(out, statement, label);
	print_dot_edge(out, statement, statement->identifier, "functionName");
	if (statement->functionParameters != NULL)
		print_dot_edge(out, statement, statement->functionParameters, "Parameters");
	if (statement->functionBody != NULL)
		print_dot_edge(out, statement, statement->functionBody, "functionBody");
	print_dot_node(out, statement->identifier, statement->identifier);
}

static void print_dot_statement_expression(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;
	print_dot_node(out, statement, "EXPRESSION");
	if (statement->statementExpression != NULL)
		print_dot_edge(out, statement, statement->statementExpression, "Expression");
}

static void print_dot_statement_assign(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;
	print_dot_node(out, statement, "ASSIGNMENT");
	print_dot_edge(out, statement, statement->statementAssignLeftside, "assign left");
	print_dot_edge(out, statement, statement->statementAssignRightside, "assign right");
}

static void print_dot_statement_block(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	FILE *out = data;
	print_dot_node(out, statement, "{}");
	print_dot_edge(out, statement, statement->firstStatement, "");
}

static void print_dot_identifier(struct mcc_ast_identifier *identifier, void *data)
{
	assert(identifier);
	assert(data);

	FILE *out = data;
	print_dot_node(out, identifier, "ID");
	print_dot_edge(out, identifier, identifier->name, "");
	print_dot_node(out, identifier->name, identifier->name);
}

// Setup an AST Visitor for printing.
static struct mcc_ast_visitor print_dot_visitor(FILE *out)
{
	assert(out);

	return (struct mcc_ast_visitor){.traversal = MCC_AST_VISIT_DEPTH_FIRST,
	                                .order = MCC_AST_VISIT_PRE_ORDER,

	                                .userdata = out,

	                                .expression = print_dot_expression,
	                                .expression_literal = print_dot_expression_literal,
	                                .expression_binary_op = print_dot_expression_binary_op,
	                                .expression_parenth = print_dot_expression_parenth,
	                                .expression_unary_op = print_dot_expression_unary_op,
	                                .expression_variable_access = print_dot_expression_variable_access,
	                                .expression_function_call = print_dot_expression_function_call,

	                                .literal_int = print_dot_literal_int,
	                                .literal_float = print_dot_literal_float,
	                                .literal_string = print_dot_literal_string,
	                                .literal_bool = print_dot_literal_bool,

	                                .statement = print_dot_statement,
	                                .statement_decl = print_dot_statement_decl,
	                                .statement_if = print_dot_statement_if,
	                                .statement_while = print_dot_statement_while,
	                                .statement_return = print_dot_statement_return,
	                                .statement_decl_array = print_dot_statement_decl_array,
	                                .statement_func_decl = print_dot_statement_func_decl,
	                                .statement_expression = print_dot_statement_expression,
	                                .statement_assign = print_dot_statement_assign,
	                                .statement_block = print_dot_statement_block,

	                                .identifier = print_dot_identifier};
}

void mcc_ast_print_dot_expression(FILE *out, struct mcc_ast_expression *expression)
{
	assert(out);
	assert(expression);

	print_dot_begin(out);

	struct mcc_ast_visitor visitor = print_dot_visitor(out);
	mcc_ast_visit(expression, &visitor);

	print_dot_end(out);
}

void mcc_ast_print_dot_literal(FILE *out, struct mcc_ast_literal *literal)
{
	assert(out);
	assert(literal);

	print_dot_begin(out);

	struct mcc_ast_visitor visitor = print_dot_visitor(out);
	mcc_ast_visit(literal, &visitor);

	print_dot_end(out);
}

void mcc_ast_print_dot_statement(FILE *out, struct mcc_ast_statement *statement)
{
	assert(out);
	assert(statement);

	print_dot_begin(out);

	struct mcc_ast_visitor visitor = print_dot_visitor(out);
	mcc_ast_visit(statement, &visitor);
	print_dot_end(out);
}
