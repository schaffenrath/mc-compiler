#include "mcc/ast_visit.h"

#include <assert.h>
#include <stdio.h>

#define visit(node, callback, visitor) \
	do { \
		if (callback) { \
			(callback)(node, (visitor)->userdata); \
		} \
	} while (0)

#define visit_if(cond, node, callback, visitor) \
	do { \
		if (cond) { \
			visit(node, callback, visitor); \
		} \
	} while (0)

#define visit_if_pre_order(node, callback, visitor) \
	visit_if((visitor)->order == MCC_AST_VISIT_PRE_ORDER, node, callback, visitor)

#define visit_if_post_order(node, callback, visitor) \
	visit_if((visitor)->order == MCC_AST_VISIT_POST_ORDER, node, callback, visitor)

void mcc_ast_visit_expression(struct mcc_ast_expression *expression, struct mcc_ast_visitor *visitor)
{
	assert(expression);
	assert(visitor);

	visit_if_pre_order(expression, visitor->expression, visitor);

	switch (expression->type) {
	case MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS:
		visit_if_pre_order(expression, visitor->expression_variable_access, visitor);
		if (expression->index != NULL)
			mcc_ast_visit(expression->index, visitor);
		visit_if_post_order(expression, visitor->expression_variable_access, visitor);
		break;

	case MCC_AST_EXPREESION_TYPE_FUNCTION_CALL:
		visit_if_pre_order(expression, visitor->expression_function_call, visitor);
		if (expression->functionarguments != NULL)
			mcc_ast_visit(expression->functionarguments, visitor);
		visit_if_post_order(expression, visitor->expression_function_call, visitor);
		break;

	case MCC_AST_EXPRESSION_TYPE_LITERAL:
		visit_if_pre_order(expression, visitor->expression_literal, visitor);
		mcc_ast_visit(expression->literal, visitor);
		visit_if_post_order(expression, visitor->expression_literal, visitor);
		break;

	case MCC_AST_EXPRESSION_TYPE_BINARY_OP:
		visit_if_pre_order(expression, visitor->expression_binary_op, visitor);
		mcc_ast_visit(expression->lhs, visitor);
		mcc_ast_visit(expression->rhs, visitor);
		visit_if_post_order(expression, visitor->expression_binary_op, visitor);
		break;

	case MCC_AST_EXPRESSION_TYPE_PARENTH:
		visit_if_pre_order(expression, visitor->expression_parenth, visitor);
		mcc_ast_visit(expression->expression, visitor);
		visit_if_post_order(expression, visitor->expression_parenth, visitor);
		break;

	case MCC_AST_EXPRESSION_TYPE_UNARY_OP:
		visit_if_pre_order(expression, visitor->expression_unary_op, visitor);
		mcc_ast_visit(expression->unary_next, visitor);
		visit_if_post_order(expression, visitor->expression_unary_op, visitor);
		break;
	}

	visit_if_post_order(expression, visitor->expression, visitor);

	if (expression->right_neighbor != NULL) {
		mcc_ast_visit_expression(expression->right_neighbor, visitor);
	}
}

void mcc_ast_visit_literal(struct mcc_ast_literal *literal, struct mcc_ast_visitor *visitor)
{
	assert(literal);
	assert(visitor);

	visit_if_pre_order(literal, visitor->literal, visitor);

	switch (literal->type) {
	case MCC_AST_LITERAL_TYPE_INT:
		visit(literal, visitor->literal_int, visitor);
		break;

	case MCC_AST_LITERAL_TYPE_FLOAT:
		visit(literal, visitor->literal_float, visitor);
		break;

	case MCC_AST_LITERAL_TYPE_STRING:
		visit(literal, visitor->literal_string, visitor);
		break;
	case MCC_AST_LITERAL_TYPE_BOOL:
		visit(literal, visitor->literal_bool, visitor);
		break;
	}

	visit_if_post_order(literal, visitor->literal, visitor);
}

void mcc_ast_visit_identifier(struct mcc_ast_identifier *identifier, struct mcc_ast_visitor *visitor)
{
	assert(identifier);
	assert(visitor);

	visit(identifier, visitor->identifier, visitor);
}

void mcc_ast_visit_statement(struct mcc_ast_statement *statement, struct mcc_ast_visitor *visitor)
{
	assert(statement);
	assert(visitor);

	visit_if_pre_order(statement, visitor->statement, visitor);

	switch (statement->type) {
	case MCC_AST_STATEMENT_TYPE_DECLARATION:
		visit_if_pre_order(statement, visitor->statement_decl, visitor);
		/* if (statement->initialValue != NULL)
		        mcc_ast_visit(statement->initialValue, visitor); */
		visit_if_post_order(statement, visitor->statement_decl, visitor);
		break;

	case MCC_AST_STATEMENT_ARRAY_DECLARATION:
		visit_if_pre_order(statement, visitor->statement_decl_array, visitor);
		mcc_ast_visit(statement->size, visitor);
		visit_if_post_order(statement, visitor->statement_decl_array, visitor);
		break;

	case MCC_AST_STATEMENT_IF:
		visit_if_pre_order(statement, visitor->statement_if, visitor);
		mcc_ast_visit(statement->condition, visitor);
		mcc_ast_visit(statement->truepath, visitor);
		if (statement->falsepath != NULL)
			mcc_ast_visit(statement->falsepath, visitor);
		visit_if_post_order(statement, visitor->statement_if, visitor);
		break;
	case MCC_AST_STATEMENT_BLOCK:
		visit_if_pre_order(statement, visitor->statement_block, visitor);
		mcc_ast_visit(statement->firstStatement, visitor);
		visit_if_post_order(statement, visitor->statement_block, visitor);
		visit(statement, visitor->statement_block_leave, visitor);
		break;
	case MCC_AST_STATEMENT_WHILE:
		visit_if_pre_order(statement, visitor->statement_while, visitor);
		mcc_ast_visit(statement->whilecondition, visitor);
		mcc_ast_visit(statement->whilebody, visitor);
		visit_if_post_order(statement, visitor->statement_while, visitor);
		break;
	case MCC_AST_STATEMENT_RETURN:
		visit_if_pre_order(statement, visitor->statement_return, visitor);
		if (statement->returnExpression != NULL)
			mcc_ast_visit(statement->returnExpression, visitor);
		visit_if_post_order(statement, visitor->statement_return, visitor);
		break;
	case MCC_AST_STATEMENT_FUNC_DECLARATION:
		visit_if_pre_order(statement, visitor->statement_func_decl, visitor);
		if (statement->functionParameters != NULL)
			mcc_ast_visit(statement->functionParameters, visitor);
		if (statement->functionBody != NULL)
			mcc_ast_visit(statement->functionBody, visitor);
		visit_if_post_order(statement, visitor->statement_func_decl, visitor);
		visit(statement, visitor->statement_block_leave, visitor);
		break;
	case MCC_AST_STATEMENT_EXPRESSION:
		visit_if_pre_order(statement, visitor->statement_expression, visitor);
		if (statement->statementExpression != NULL)
			mcc_ast_visit(statement->statementExpression, visitor);
		visit_if_post_order(statement, visitor->statement_expression, visitor);
		break;
	case MCC_AST_STATEMENT_ASSIGNMENT:
		visit_if_pre_order(statement, visitor->statement_assign, visitor);
		mcc_ast_visit(statement->statementAssignLeftside, visitor);
		mcc_ast_visit(statement->statementAssignRightside, visitor);
		visit_if_post_order(statement, visitor->statement_assign, visitor);
		break;
	}
	visit_if_post_order(statement, visitor->statement, visitor);

	if (statement->right_neighbor != NULL) {
		mcc_ast_visit_statement(statement->right_neighbor, visitor);
	}
}
