#include "mcc/ast_ir.h"
#include "mcc/ast_visit.h"
#include "mcc/ir.h"
#include "mcc/list.h"
#include <assert.h>
#include <stdlib.h>

// static char *globalfilename = "";

struct myUserData {
	FILE *out;
	mcc_ir_ptr_t ir;
	list_t *variable_stack;
	enum mcc_irStatus result;
};

static void ir_expression_binary_op(struct mcc_ast_expression *expression, void *data)
{
	struct myUserData *myData = data;

	char *value = mcc_ir_createTemporary(myData->ir, expression->node.symbolTable, expression->synthesizedType);
	mcc_ir_visit_expression(expression->lhs, data);
	mcc_ir_visit_expression(expression->rhs, data);
	char *op2 = list_top(myData->variable_stack);
	list_pop(myData->variable_stack);
	char *op1 = list_top(myData->variable_stack);
	list_pop(myData->variable_stack);
	char *temp;
	switch (expression->op) {
	case MCC_AST_BINARY_OP_ADD:
		mcc_ir_addOperationBAdd(myData->ir, op1, op2, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_SUB:
		mcc_ir_addOperationBSub(myData->ir, op1, op2, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_MUL:
		mcc_ir_addOperationBMul(myData->ir, op1, op2, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_DIV:
		mcc_ir_addOperationBDiv(myData->ir, op1, op2, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_AND:
		mcc_ir_addOperationBAnd(myData->ir, op1, op2, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_OR:
		mcc_ir_addOperationBOr(myData->ir, op1, op2, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_EQU:
		mcc_ir_addOperationBEq(myData->ir, op1, op2, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_NEQ:
		mcc_ir_addOperationBEq(myData->ir, op1, op2, value, expression->node.symbolTable);
		mcc_ir_addOperationUNot(myData->ir, value, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_LES:
		mcc_ir_addOperationBL(myData->ir, op1, op2, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_GRT:
		// simply switch operands in lower
		mcc_ir_addOperationBL(myData->ir, op2, op1, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_LEQ:
		// simply switch operands in lower
		mcc_ir_addOperationBL(myData->ir, op1, op2, value, expression->node.symbolTable);
		temp = mcc_ir_createTemporary(myData->ir, expression->node.symbolTable, expression->synthesizedType);
		mcc_ir_addOperationBEq(myData->ir, op1, op2, temp, expression->node.symbolTable);
		mcc_ir_addOperationBOr(myData->ir, value, temp, value, expression->node.symbolTable);
		break;
	case MCC_AST_BINARY_OP_GEQ:
		// simply switch operands in lower
		mcc_ir_addOperationBL(myData->ir, op2, op1, value, expression->node.symbolTable);
		temp = mcc_ir_createTemporary(myData->ir, expression->node.symbolTable, expression->synthesizedType);
		mcc_ir_addOperationBEq(myData->ir, op2, op1, temp, expression->node.symbolTable);
		mcc_ir_addOperationBOr(myData->ir, value, temp, value, expression->node.symbolTable);
		break;
	default:
		return;
	}
	list_push(myData->variable_stack, value);
}

static void ir_expression_unary_op(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	struct myUserData *myData = data;
	mcc_ir_visit_expression(expression->unary_next, data);
	char *sub = list_top(myData->variable_stack);
	list_pop(myData->variable_stack);
	char *result = mcc_ir_createTemporary(myData->ir, expression->node.symbolTable, expression->synthesizedType);
	switch (expression->unary_op) {
	case MCC_AST_UNARY_OP_NOT:
		mcc_ir_addOperationUNot(myData->ir, sub, result, expression->node.symbolTable);
		break;
	case MCC_AST_UNARY_OP_NEG:
		mcc_ir_addOperationUNeg(myData->ir, sub, result, expression->node.symbolTable);
		break;
	}
	list_push(myData->variable_stack, result);
}

static void ir_expression_parenth(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	mcc_ir_visit_expression(expression->expression, data);
}

static void ir_expression_variable_access(struct mcc_ast_expression *expression, void *data)
{
	struct myUserData *myData = data;

	if (expression->index == NULL) {
		list_push(myData->variable_stack, expression->identifier);
	} else {
		// visit index expr here?
		char *value =
		    mcc_ir_createTemporary(myData->ir, expression->node.symbolTable, expression->synthesizedType);
		mcc_ir_visit_expression(expression->index, data);
		mcc_ir_addOperationLoad(myData->ir, expression->identifier, (char *)list_top(myData->variable_stack),
		                        value, expression->node.symbolTable);
		list_pop(myData->variable_stack);
		list_push(myData->variable_stack, value);
	}
}

static void ir_expression_function_call(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	struct myUserData *myData = data;

	struct mcc_ast_expression *exp_iterator = expression->functionarguments;

	// use stack to reverse parameterlist
	list_ptr_t paramStack = list_create(0);

	// find all parameters first
	unsigned int argument_count = 0;
	while (exp_iterator != NULL) {
		// mcc_ir_visit_expression(exp_iterator->functionarguments, myData);
		// char *function_arg = list_top(myData->variable_stack);

		// mcc_ir_addOperationPrepParam(myData->ir, function_arg, expression->node.symbolTable);
		list_push(paramStack, exp_iterator);
		exp_iterator = exp_iterator->right_neighbor;
		argument_count++;
	}

	// visit them in reversed order
	while ((exp_iterator = list_top(paramStack)) != NULL) {
		mcc_ir_visit_expression(exp_iterator, myData);
		char *function_arg = list_top(myData->variable_stack);
		list_pop(myData->variable_stack);

		mcc_ir_addOperationPrepParam(myData->ir, function_arg, expression->node.symbolTable);
		list_pop(paramStack);
	}

	list_free(paramStack);
	mcc_ir_addOperationCall(myData->ir, expression->callname, argument_count, expression->node.symbolTable);
	if (expression->synthesizedType != MCC_AST_TYPE_VOID) {
		char *temp;
		temp = mcc_ir_createTemporary(myData->ir, expression->node.symbolTable, expression->synthesizedType);
		mcc_ir_addOperationCopy(myData->ir, "returnregister", temp, expression->node.symbolTable);
		list_push(myData->variable_stack, temp);
	}
}

static void ir_literal_int(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);

	struct myUserData *myData = data;
	char *var = list_top(myData->variable_stack);
	// char lit[30];
	char *lit = malloc(30 * sizeof(char));
	snprintf(lit, 30, "%ld", literal->i_value);
	mcc_ir_addLiteral(myData->ir, lit);
	mcc_ir_addOperationCopy(myData->ir, lit, var, literal->node.symbolTable);
}

static void ir_literal_float(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);

	struct myUserData *myData = data;
	char *var = list_top(myData->variable_stack);
	char *lit = malloc(30 * sizeof(char));
	snprintf(lit, 30, "%f", literal->f_value);
	mcc_ir_addLiteral(myData->ir, lit);
	mcc_ir_addOperationCopy(myData->ir, lit, var, literal->node.symbolTable);
}

static void ir_literal_string(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);

	struct myUserData *myData = data;
	char *var = list_top(myData->variable_stack);
	size_t len = strlen(literal->s_value + 1) + 5;
	char *lit = malloc(sizeof(char) * len);
	snprintf(lit, len, "\\\"%s\\\"", literal->s_value + 1);
	mcc_ir_addLiteral(myData->ir, lit);
	mcc_ir_addOperationCopy(myData->ir, lit, var, literal->node.symbolTable);
}

static void ir_literal_bool(struct mcc_ast_literal *literal, void *data)
{
	assert(literal);
	assert(data);

	struct myUserData *myData = data;
	char *var = list_top(myData->variable_stack);
	char *lit = malloc(6 * sizeof(char));
	snprintf(lit, 6, "%s", literal->b_value ? "TRUE" : "FALSE");
	mcc_ir_addLiteral(myData->ir, lit);
	mcc_ir_addOperationCopy(myData->ir, lit, var, literal->node.symbolTable);
}

static void ir_expression_literal(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);

	struct myUserData *myData = data;

	char *temp = mcc_ir_createTemporary(myData->ir, expression->node.symbolTable, expression->synthesizedType);
	list_push(myData->variable_stack, (void *)temp);
	switch (expression->literal->type) {
	case MCC_AST_LITERAL_TYPE_INT:
		ir_literal_int(expression->literal, data);
		break;
	case MCC_AST_LITERAL_TYPE_FLOAT:
		ir_literal_float(expression->literal, data);
		break;
	case MCC_AST_LITERAL_TYPE_BOOL:
		ir_literal_bool(expression->literal, data);
		break;
	case MCC_AST_LITERAL_TYPE_STRING:
		ir_literal_string(expression->literal, data);
		break;
	}
}

static void ir_statement_decl(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	// nothing to do. add debug output?
}

static void ir_statement_decl_array(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	// nothing to do. add debug output?
	struct myUserData *myData = data;

	mcc_ir_addOperationBuildArray(myData->ir, statement->identifier, statement->node.symbolTable);
}

static void ir_statement_if(struct mcc_ast_statement *statement, void *data)
{
	struct myUserData *myData = data;

	mcc_ir_visit_expression(statement->condition, data);
	char *t1 = list_top(myData->variable_stack);
	list_pop(myData->variable_stack);
	char L1[30];
	char *L1Stored = mcc_ir_createTemporaryLabel(myData->ir, L1, 30, "ELSE");
	char L2[30];
	char *L2Stored = mcc_ir_createTemporaryLabel(myData->ir, L2, 30, "FI");
	mcc_ir_addOperationIfNotJump(myData->ir, L1Stored, t1, statement->node.symbolTable);
	if (statement->truepath != NULL)
		mcc_ir_visit_statement(statement->truepath, data);
	if (statement->falsepath != NULL)
		mcc_ir_addOperationJump(myData->ir, L2Stored, statement->node.symbolTable);
	mcc_ir_addOperationLabel(myData->ir, L1Stored, statement->node.symbolTable);
	if (statement->falsepath != NULL) {
		if (statement->falsepath != NULL)
			mcc_ir_visit_statement(statement->falsepath, data);
		mcc_ir_addOperationLabel(myData->ir, L2Stored, statement->node.symbolTable);
	}
}

static void ir_statement_while(struct mcc_ast_statement *statement, void *data)
{
	struct myUserData *myData = data;

	char L1[30];
	char *L1Stored = mcc_ir_createTemporaryLabel(myData->ir, L1, 30, "WHILEBEG");
	char L2[30];
	char *L2Stored = mcc_ir_createTemporaryLabel(myData->ir, L2, 30, "WHILEEND");
	mcc_ir_addOperationLabel(myData->ir, L1Stored, statement->node.symbolTable);
	mcc_ir_visit_expression(statement->whilecondition, data);
	char *t1 = list_top(myData->variable_stack);
	list_pop(myData->variable_stack);
	mcc_ir_addOperationIfNotJump(myData->ir, L2Stored, t1, statement->node.symbolTable);
	mcc_ir_visit_statement(statement->whilebody, data);
	mcc_ir_addOperationJump(myData->ir, L1Stored, statement->node.symbolTable);
	mcc_ir_addOperationLabel(myData->ir, L2Stored, statement->node.symbolTable);
}

static void ir_statement_return(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	struct myUserData *myData = data;
	if (statement->returnExpression != NULL) {
		mcc_ir_visit_expression(statement->returnExpression, data);
		mcc_ir_addOperationReturn(myData->ir, list_top(myData->variable_stack), statement->node.symbolTable);
		list_pop(myData->variable_stack);
	} else {
		mcc_ir_addOperationReturn(myData->ir, NULL, statement->node.symbolTable);
	}
	// char* retVar = list_top(myData->variable_stack);
	// list_pop(myData->variable_stack);
	// do something with the variable?!
	// mcc_ir_addOperationJump(myData->ir, "How to Jump back?!?");
}

static void ir_statement_func_decl(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	struct myUserData *myData = data;
	mcc_ir_addOperationLabel(myData->ir, statement->functionName, statement->node.symbolTable);

	mcc_ir_visit_statement(statement->functionBody, data);
	if (statement->returnType == MCC_AST_TYPE_VOID) {
		mcc_ir_addOperationReturn(myData->ir, NULL, statement->node.symbolTable);
	}
}

static void ir_statement_expression(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	if (statement->statementExpression != NULL)
		mcc_ir_visit_expression(statement->statementExpression, data);
}

static void ir_statement_assign(struct mcc_ast_statement *statement, void *data)
{
	struct myUserData *myData = data;
	mcc_ir_visit_expression(statement->statementAssignRightside, data);
	char *value = list_top(myData->variable_stack);
	list_pop(myData->variable_stack);
	if (statement->statementAssignLeftside->index == NULL) {
		mcc_ir_addOperationCopy(myData->ir, value, statement->statementAssignLeftside->identifier,
		                        statement->node.symbolTable);
	} else {
		mcc_ir_visit_expression(statement->statementAssignLeftside->index, data);
		char *offset = list_top(myData->variable_stack);
		list_pop(myData->variable_stack);
		mcc_ir_addOperationStore(myData->ir, value, offset, statement->statementAssignLeftside->identifier,
		                         statement->node.symbolTable);
	}
}

static void ir_statement_block(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);

	struct myUserData *myData = data;
	mcc_ir_addOperationOpenBlock(myData->ir, statement->blockName, statement->node.symbolTable);
	if (statement->firstStatement != NULL)
		mcc_ir_visit_statement(statement->firstStatement, data);
	mcc_ir_addOperationCloseBlock(myData->ir, statement->blockName, statement->node.symbolTable);
}

void mcc_ir_visit_statement(struct mcc_ast_statement *statement, void *data)
{
	assert(statement);
	assert(data);
	do {
		// struct myUserData *myData = data;
		switch (statement->type) {
		case MCC_AST_STATEMENT_ASSIGNMENT: {
			ir_statement_assign(statement, data);
			break;
		}
		case MCC_AST_STATEMENT_FUNC_DECLARATION: {
			ir_statement_func_decl(statement, data);
			break;
		}
		case MCC_AST_STATEMENT_TYPE_DECLARATION: {
			ir_statement_decl(statement, data);
			break;
		}
		case MCC_AST_STATEMENT_ARRAY_DECLARATION: {
			ir_statement_decl_array(statement, data);
			break;
		}
		case MCC_AST_STATEMENT_IF: {
			ir_statement_if(statement, data);
			break;
		}
		case MCC_AST_STATEMENT_WHILE: {
			ir_statement_while(statement, data);
			break;
		}
		case MCC_AST_STATEMENT_EXPRESSION: {
			ir_statement_expression(statement, data);
			break;
		}
		case MCC_AST_STATEMENT_BLOCK: {
			ir_statement_block(statement, data);
			break;
		}
		case MCC_AST_STATEMENT_RETURN: {
			ir_statement_return(statement, data);
			break;
		}
		default:
			break;
		}
		statement = statement->right_neighbor;
	} while (statement != NULL);
}

void mcc_ir_visit_expression(struct mcc_ast_expression *expression, void *data)
{
	assert(expression);
	assert(data);
	// struct myUserData *myData = data;
	switch (expression->type) {
	case MCC_AST_EXPRESSION_TYPE_BINARY_OP:
		ir_expression_binary_op(expression, data);
		break;

	case MCC_AST_EXPRESSION_TYPE_LITERAL:
		ir_expression_literal(expression, data);
		break;

	case MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS:
		ir_expression_variable_access(expression, data);
		break;

	case MCC_AST_EXPRESSION_TYPE_UNARY_OP:
		ir_expression_unary_op(expression, data);
		break;

	case MCC_AST_EXPREESION_TYPE_FUNCTION_CALL:
		ir_expression_function_call(expression, data);
		break;
	case MCC_AST_EXPRESSION_TYPE_PARENTH:
		ir_expression_parenth(expression, data);
		break;
	}
}

struct myUserData *mcc_ir_createUserData(FILE *out)
{
	struct myUserData *data = malloc(sizeof(struct myUserData));
	data->out = out;
	data->ir = mcc_ir_createIR();
	data->variable_stack = list_create(0);
	data->result = MCC_IR_STATUS_OK;
	return data;
}

void mcc_ir_freeUserData(struct myUserData *data)
{
	list_free(data->variable_stack);
	free(data);
}

enum mcc_irStatus mcc_ast_ir_expression(FILE *out, struct mcc_ast_expression *expression, mcc_ir_ptr_t *ir)
{
	assert(out);
	assert(expression);

	struct myUserData *data = mcc_ir_createUserData(out);
	mcc_ir_visit_expression(expression, data);
	*ir = data->ir;
	enum mcc_irStatus res = data->result;
	mcc_ir_freeUserData(data);
	return res;
}

enum mcc_irStatus mcc_ast_ir_statement(FILE *out, struct mcc_ast_statement *statement, mcc_ir_ptr_t *ir)
{
	assert(out);
	assert(statement);

	struct myUserData *data = mcc_ir_createUserData(out);
	mcc_ir_visit_statement(statement, data);
	*ir = data->ir;
	enum mcc_irStatus res = data->result;
	mcc_ir_freeUserData(data);
	return res;
}
