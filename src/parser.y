%define api.prefix {mcc_parser_}

%define api.pure full
%lex-param   {void *scanner}
%parse-param {void *scanner} {struct mcc_ast_statement** result} {struct mcc_ast_expression** result_expr}

%define parse.trace
%define parse.error verbose

%code requires {
#include "mcc/parser.h"
}

%{
#include <string.h>
static char* globalfilename = "notset";
int mcc_parser_lex();
void mcc_parser_error();
//struct mcc_symbolTable *actualSymbolTable;

#define loc(ast_node, ast_sloc) \
	(ast_node)->node.sloc.start_col = (ast_sloc).first_column; \
	(ast_node)->node.sloc.start_line = (ast_sloc).first_line; \
	(ast_node)->node.sloc.end_col = (ast_sloc).last_column; \
	(ast_node)->node.sloc.end_line = (ast_sloc).last_line;

%}

%define api.value.type union
%define api.token.prefix {TK_}

%locations

%token START_PROG
%token START_EXPR
%token START_STMT

%token END 0 "EOF"

%token <long>   INT_LITERAL   "integer literal"
%token <double> FLOAT_LITERAL "float literal"
%token <int>    BOOL_LITERAL   "bool literal"
%token <char *> STRING_LITERAL "string literal"

%token <char*> IDENTIFIER "ID"

%token LPARENTH "("
%token RPARENTH ")"

%token LPARRECT  "["
%token RPARRECT  "]"
%token LBRACKET  "{"
%token RBRACKET  "}"

%token PLUS  "+"
%token MINUS "-"
%token ASTER "*"
%token SLASH "/"

%token SEPERATOR ","
%token TERMINATOR ";"

%token ASSIGN "="

%token LOR  "OR"
%token LAND "AND"
%token NOT "!"

%token EQUAL      "=="
%token NEQUAL     "!="
%token LESS       "<"
%token GREATER    ">"
%token LESSEQ     "<="
%token GREATEREQ  ">="
%token TYPE_VOID 	"void"
%token TYPE_BOOL  "bool"
%token TYPE_INT   "int"
%token TYPE_FLOAT "float"
%token TYPE_STRING "string"
%token WHILE      "while"		
%token IF 				"if"			
%token ELSE       "else"
%token RETURN     "return"
%token THEN

%left LAND LOR NOT UMINUS
%left EQUAL NEQUAL LESS GREATER LESSEQ GREATEREQ
%left PLUS MINUS
%left ASTER SLASH


%right THEN ELSE
                        
%type <struct mcc_ast_expression *> expression
%type <struct mcc_ast_expression *> call_expr
%type <struct mcc_ast_expression *> arguments
%type <struct mcc_ast_expression *> ident_expr
%type <struct mcc_ast_literal *> literal
%type <struct mcc_ast_statement *> statement
%type <struct mcc_ast_statement *> declaration
%type <struct mcc_ast_statement *> declarationStmt
%type <struct mcc_ast_statement *> statementLst
%type <struct mcc_ast_statement *> ifStmt
%type <struct mcc_ast_statement *> whileStmt
%type <struct mcc_ast_statement *> returnStmt
%type <struct mcc_ast_statement *> function_def
%type <struct mcc_ast_statement *> function_body
%type <struct mcc_ast_statement *> parameterLst
%type <struct mcc_ast_statement *> assignment
%type <struct mcc_ast_statement *> funcLst
%type <enum mcc_ast_type_type> type

%start start

%destructor {free($$);} <char*>
%destructor {mcc_ast_delete_expression($$);} expression
%destructor {mcc_ast_delete_expression($$);} arguments
%destructor {mcc_ast_delete_expression($$);} call_expr
%destructor {mcc_ast_delete_expression($$);} ident_expr
%destructor {mcc_ast_delete_literal($$);} literal
%destructor {mcc_ast_delete_statement($$);} statement 
%destructor {mcc_ast_delete_statement($$);} declaration 
%destructor {mcc_ast_delete_statement($$);} declarationStmt 
%destructor {mcc_ast_delete_statement($$);} statementLst
%destructor {mcc_ast_delete_statement($$);} ifStmt
%destructor {mcc_ast_delete_statement($$);} whileStmt
%destructor {mcc_ast_delete_statement($$);} returnStmt
%destructor {mcc_ast_delete_statement($$);} function_def
%destructor {mcc_ast_delete_statement($$);} function_body
%destructor {mcc_ast_delete_statement($$);} parameterLst
%destructor {mcc_ast_delete_statement($$);} assignment
%destructor {mcc_ast_delete_statement($$);} funcLst


%%

start : START_PROG toplevel
			| START_EXPR expression { *result_expr = $2; }
			| START_STMT statement { *result = $2; }
			;

toplevel : funcLst { *result = $1; }
         ;

funcLst : %empty 								{ $$ = NULL; }
				| funcLst function_def 	{ if($1 == NULL) $$ = $2; else (mcc_ast_get_last_statement($1))->right_neighbor = $2; loc($$, @1); }

statementLst : statement							{$$ = $1; loc($$, @1); }
						 | statementLst statement {$$ = $1; (mcc_ast_get_last_statement($1))->right_neighbor = $2; loc($$, @1); }
						 ;

statement : declarationStmt													         {$$ = $1; loc($$, @1); }
					| ifStmt																					 {$$ = $1; loc($$, @1); }
          | whileStmt                                        {$$ = $1; loc($$, @1); }
          | returnStmt                                       {$$ = $1; loc($$, @1); }
          | LBRACKET statementLst RBRACKET									 {$$ = mcc_ast_new_block_statement($2); loc($$, @1); }
          | LBRACKET RBRACKET								                 {$$ = mcc_ast_new_expression_statement(NULL); loc($$, @1); }
					| function_def 																		 {$$ = $1; loc($$, @1); }
					| expression TERMINATOR 													 {$$ = mcc_ast_new_expression_statement($1); loc($$, @1); }
          | assignment                                       {$$ = $1; loc($$, @1); }
					;

assignment : ident_expr ASSIGN expression TERMINATOR         {$$ = mcc_ast_new_assign_statement($1,$3); loc($$, @1); }
           ;

ifStmt : IF LPARENTH expression RPARENTH statement ELSE statement {$$ = mcc_ast_new_if_statement($3,$5,$7); loc($$, @1); }
			 | IF LPARENTH expression RPARENTH statement %prec THEN     {$$ = mcc_ast_new_if_statement($3,$5,NULL); loc($$, @1); }
			 ;

whileStmt : WHILE LPARENTH expression RPARENTH statement		 { $$ = mcc_ast_new_while_statement($3,$5); loc($$, @1); }
			 ;

returnStmt : RETURN TERMINATOR  														 { $$ = mcc_ast_new_return_statement(NULL); loc($$, @1); }
           | RETURN expression TERMINATOR										 { $$ = mcc_ast_new_return_statement($2); loc($$, @1); }
           ;

declarationStmt : declaration TERMINATOR 										 { $$ = $1; loc($$, @1); }
								;

declaration : type IDENTIFIER 															 { $$ = mcc_ast_new_decl_statement($1,$2,NULL); loc($$, @1); }
						| type LPARRECT INT_LITERAL RPARRECT IDENTIFIER	 { $$ = mcc_ast_new_array_statement($1,$5,mcc_ast_new_literal_int($3)); loc($$, @1); }
						;

function_def : type IDENTIFIER LPARENTH parameterLst RPARENTH function_body { $$ = mcc_ast_new_func_statement($1, $2, $4, $6); loc($$, @1); }
             | type IDENTIFIER LPARENTH RPARENTH function_body { $$ = mcc_ast_new_func_statement($1, $2, NULL, $5); loc($$, @1); }
             | TYPE_VOID IDENTIFIER LPARENTH parameterLst RPARENTH function_body { $$ = mcc_ast_new_func_statement(MCC_AST_TYPE_VOID, $2, $4, $6); loc($$, @1); }
             | TYPE_VOID IDENTIFIER LPARENTH RPARENTH function_body { $$ = mcc_ast_new_func_statement(MCC_AST_TYPE_VOID, $2, NULL, $5); loc($$, @1); }
					   ;

function_body : LBRACKET statementLst RBRACKET   { $$ = $2; loc($$, @1); }
              | LBRACKET RBRACKET   { $$ = NULL; }      
              ;
						 
parameterLst : declaration 																	 { $$ = $1; loc($$, @1); }
					 	 | parameterLst SEPERATOR declaration 					 { $$ = $1; (mcc_ast_get_last_statement($1))->right_neighbor = $3; loc($$, @1); }
					 	 ;

expression : literal                         					 { $$ = mcc_ast_new_expression_literal($1);                              loc($$, @1); }
           | expression PLUS  expression     					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_ADD, $1, $3); loc($$, @1); }
           | expression MINUS expression     					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_SUB, $1, $3); loc($$, @1); }
           | expression ASTER expression     					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_MUL, $1, $3); loc($$, @1); }
           | expression SLASH expression     					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_DIV, $1, $3); loc($$, @1); }
           | expression EQUAL expression     					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_EQU, $1, $3); loc($$, @1); }
           | expression NEQUAL expression    					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_NEQ, $1, $3); loc($$, @1); }
           | expression LESS expression      					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_LES, $1, $3); loc($$, @1); }
           | expression GREATER expression   					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_GRT, $1, $3); loc($$, @1); }
           | expression LESSEQ expression    					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_LEQ, $1, $3); loc($$, @1); }
           | expression GREATEREQ expression 					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_GEQ, $1, $3); loc($$, @1); }
           | expression LAND expression      					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_AND, $1, $3); loc($$, @1); }
           | expression LOR expression       					 { $$ = mcc_ast_new_expression_binary_op(MCC_AST_BINARY_OP_OR,  $1, $3); loc($$, @1); }
           | LPARENTH expression RPARENTH    					 { $$ = mcc_ast_new_expression_parenth($2);                              loc($$, @1); }
           | MINUS expression %prec UMINUS   					 { $$ = mcc_ast_new_expression_unary_op(MCC_AST_UNARY_OP_NEG, $2); loc($$, @1); }
           | NOT expression                  					 { $$ = mcc_ast_new_expression_unary_op(MCC_AST_UNARY_OP_NOT, $2); loc($$, @1); }
					 | ident_expr																 { $$ = $1; loc($$, @1); }
					 | call_expr																 { $$ = $1; loc($$, @1); }
					 ;

ident_expr : IDENTIFIER																 { $$ = mcc_ast_new_expression_identifier($1, NULL); loc($$, @1); }
					 | IDENTIFIER	LPARRECT expression RPARRECT   { $$ = mcc_ast_new_expression_identifier($1, $3); loc($$, @1); }
           ;

call_expr : IDENTIFIER LPARENTH RPARENTH 							 { $$ = mcc_ast_func_call_expression($1, NULL); loc($$, @1); }
					| IDENTIFIER LPARENTH arguments RPARENTH		 { $$ = mcc_ast_func_call_expression($1, $3); loc($$, @1); }
					; 

arguments : expression {$$ = $1; loc($$, @1); }
          | arguments SEPERATOR expression {$$ = $1; (mcc_ast_get_last_argument($1))->right_neighbor = $3; loc($$, @1); }
;


literal : INT_LITERAL   { $$ = mcc_ast_new_literal_int($1);   loc($$, @1); }
        | FLOAT_LITERAL { $$ = mcc_ast_new_literal_float($1); loc($$, @1); }
        | BOOL_LITERAL  { $$ = mcc_ast_new_literal_bool($1);   loc($$, @1); }
				| STRING_LITERAL { $$ = mcc_ast_new_literal_string($1); loc($$, @1); }
        ;

type : TYPE_INT     { $$ = MCC_AST_TYPE_INT; }
		 | TYPE_FLOAT   { $$ = MCC_AST_TYPE_FLOAT; }
		 | TYPE_BOOL    { $$ = MCC_AST_TYPE_BOOL; }
		 | TYPE_STRING  { $$ = MCC_AST_TYPE_STRING; }
		 ;

%%

#include <assert.h>

#include "scanner.h"
#include "utils/unused.h"

__thread int mcc_start_rule = TK_START_PROG;

void mcc_parser_error(struct MCC_PARSER_LTYPE *yylloc, yyscan_t *scanner, struct mcc_ast_statement **statement, struct mcc_ast_expression **expression, const char *msg, const char* filename, int level)
{
	// TODO
	//UNUSED(yylloc);
	// if(yylloc != NULL){
	// 	fprintf(stderr,"error line starts at %d\n", yylloc->first_line);
	// 	fprintf(stderr,"error line ends at %d\n", yylloc->last_line);
	// 	fprintf(stderr,"error column starts at %d\n", yylloc->first_column);
	// 	fprintf(stderr,"error column ends at %d\n", yylloc->last_column);
	// }
	UNUSED(scanner);
	UNUSED(statement);
	UNUSED(expression);

	//UNUSED(msg);
  if (scanner != NULL){  //error on parsing/scanning stage
    level = 4;
    filename = globalfilename;
  }
  log_log(level, filename, yylloc->first_line, yylloc->first_column, msg);
}

void mcc_parser_error_ext(struct mcc_ast_source_location *srcloc, const char *msg, const char *filename, int level) {
    filename = globalfilename;
  	log_log(level, filename, srcloc->start_line, srcloc->start_col, msg);
	
}

struct mcc_parser_result mcc_parse_string_with_rule(const char *input, int startrule)
{
  assert(input);
  mcc_start_rule = startrule;
	FILE *in = fmemopen((void *)input, strlen(input), "r");
	if (!in) {
		return (struct mcc_parser_result){
		    .status = MCC_PARSER_STATUS_UNABLE_TO_OPEN_STREAM,
		};
	}

	struct mcc_parser_result result = mcc_parse_file(in);

	fclose(in);

	return result;
}

struct mcc_parser_result mcc_parse_string(const char *input)
{
	return mcc_parse_string_with_rule(input, TK_START_PROG);
}

struct mcc_parser_result mcc_parse_string_expression(const char *input)
{
  return mcc_parse_string_with_rule(input, TK_START_EXPR);
}

struct mcc_parser_result mcc_parse_string_statement(const char *input)
{
	return mcc_parse_string_with_rule(input, TK_START_STMT);
}

struct mcc_parser_result mcc_parse_file(FILE *input)
{
	assert(input);

	yyscan_t scanner;
	mcc_parser_lex_init(&scanner);
	mcc_parser_set_in(input, scanner);

	struct mcc_parser_result result = {
	    .status = MCC_PARSER_STATUS_OK,
	};

	if (yyparse(scanner, &(result.statement), &(result.expression)) != 0) {
		result.status = MCC_PARSER_STATUS_UNKNOWN_ERROR;
	}

	mcc_parser_lex_destroy(scanner);

	return result;
}

struct mcc_parser_result mcc_parse_file_with_name(FILE *input, char *filename)
{
  assert(filename);
  globalfilename = filename;

	return mcc_parse_file(input);
}
