#include <stdlib.h>

#include "mcc/ast_print.h"
#include "vendor/map/map.h"
#include "mcc/symbol_table.h"

#include "utils/unused.h"

struct mcc_symbolTable {
	unsigned int level;
	// map_t myMap;
	map_t variableMap;
	map_t functionMap;
	struct mcc_symbolAttributes *innermostFunction;
	struct mcc_symbolTable *parent;
	map_t scopeMap;
	unsigned int memorySpace;
};

typedef struct mcc_symbolTable symbolTable;

struct mcc_symbolTable *mcc_symbTcreate(struct mcc_symbolTable *parent, const char *name)
{
	symbolTable *tab = malloc(sizeof(symbolTable));

	// tab->myMap = hashmap_new();
	tab->variableMap = mapNew();
	tab->functionMap = mapNew();
	tab->parent = parent;
	if (tab->parent == NULL) {
		tab->level = 0;
		tab->innermostFunction = NULL;
		tab->scopeMap = mapNew();
	} else {
		tab->level = tab->parent->level + 1;
		tab->innermostFunction = tab->parent->innermostFunction;
		tab->scopeMap = tab->parent->scopeMap;
	}

	mapAdd(name, tab, tab->scopeMap);

	return tab;
}

struct mcc_symbolTable *mcc_symbTcreateRoot()
{
	return mcc_symbTcreate(NULL, "ROOT");
}

char *mcc_symbTaddSymbol(const char *symbol, struct mcc_symbolAttributes *attr, map_t someMap)
{
	// hashmap_put(tab->myMap, symbol, attr);
	return mapDynAdd(symbol, attr, someMap);
}

char *mcc_symbTaddSymbolFunction(struct mcc_symbolTable *tab, const char *symbol, struct mcc_symbolAttributes *attr)
{
	return mcc_symbTaddSymbol(symbol, attr, tab->functionMap);
}
char *mcc_symbTaddSymbolVariable(struct mcc_symbolTable *tab, const char *symbol, struct mcc_symbolAttributes *attr)
{
	return mcc_symbTaddSymbol(symbol, attr, tab->variableMap);
}

struct mcc_symbolAttributes *
mcc_symbTlookup(struct mcc_symbolTable *tab, const char *symbol, enum mcc_symbolSearch syms)
{
	struct mcc_symbolAttributes *attr = NULL;
	switch (syms) {
	case MCC_SYMBOL_ALL: {
		attr = mapGet(symbol, tab->variableMap);
		if (attr == NULL)
			attr = mapGet(symbol, tab->functionMap);
		break;
	}
	case MCC_SYMBOL_FUNC_ONLY: {
		attr = mapGet(symbol, tab->functionMap);
		break;
	}
	case MCC_SYMBOL_VARIABLE_ONLY: {
		attr = mapGet(symbol, tab->variableMap);
		break;
	}
	default:
		break;
	}

	// struct mcc_symbolAttributes *attr;
	// hashmap_get(tab->myMap, symbol, &attr);
	return attr;
}

struct mcc_symbolAttributes *
mcc_symbTlookupRec(struct mcc_symbolTable *tab, const char *symbol, enum mcc_symbolSearch syms)
{
	struct mcc_symbolAttributes *attr = NULL;
	attr = mcc_symbTlookup(tab, symbol, syms);
	if (attr == NULL && tab->parent != NULL) {
		return mcc_symbTlookupRec(tab->parent, symbol, syms);
	}
	return attr;
}

struct mcc_symbolAttributes *
mcc_symbTlookupRec2(struct mcc_symbolTable *tab, const char *symbol, enum mcc_symbolSearch syms, int *offset)
{
	(*offset) += (tab->memorySpace + 4); //+4 since ebp is stored extra
	struct mcc_symbolAttributes *attr = NULL;
	attr = mcc_symbTlookup(tab, symbol, syms);
	if (attr == NULL && tab->parent != NULL) {
		return mcc_symbTlookupRec2(tab->parent, symbol, syms, offset);
	}
	return attr;
}

struct mcc_symbolTable *mcc_symbTgetParent(struct mcc_symbolTable *tab)
{
	return tab->parent;
}

void mcc_symbTsetFunction(struct mcc_symbolTable *tab, struct mcc_symbolAttributes *attr)
{
	tab->innermostFunction = attr;
}

struct mcc_symbolAttributes *mcc_symbTgetFunction(struct mcc_symbolTable *tab)
{
	return tab->innermostFunction;
}

// int deleteParamtertypelist(any_t dummy, any_t attr){
static void deleteParamtertypelist(const char *key, void *attr, void *dummy)
{
	UNUSED(dummy);
	UNUSED(key);
	struct mcc_symbolAttributes *attrib = attr;
	if (attrib->parameter_type_list != NULL) {
		list_free(attrib->parameter_type_list);
	}
	// return MAP_OK;
}

void mcc_symbTdelete(struct mcc_symbolTable *table)
{
	mapMapFunction(table->variableMap, deleteParamtertypelist, NULL);
	mapClose(table->variableMap);
	mapMapFunction(table->functionMap, deleteParamtertypelist, NULL);
	mapClose(table->functionMap);
	// hashmap_iterate(table->myMap, deleteParamtertypelist, NULL);
	// hashmap_free(table->myMap);
	if (table->parent == NULL) {
		mapClose(table->scopeMap);
	}
	free(table);
}

// int printLine(any_t data, char* key, any_t value){
static void printLine(const char *key, void *value, void *data)
{
	FILE *out = (FILE *)data;
	struct mcc_symbolAttributes *attr = (struct mcc_symbolAttributes *)value;
	fprintf(out, "| %-29s| %-29s| %-29d| %-6d| %-9d|\n", key, mcc_ast_print_type(attr->type),
	        attr->parameter_type_list == NULL ? attr->array_size : attr->parameter_type_list->size,
	        attr->isParameter, attr->negativeStackOffset);
	// return MAP_OK;
}

int mcc_symbTtypeToByte(enum mcc_ast_type_type type)
{
	switch (type) {
	case MCC_AST_TYPE_INT: // should be a long with 8 bytes??
		return 4;
	case MCC_AST_TYPE_FLOAT:
		return 4;
	case MCC_AST_TYPE_STRING: // pointer?
		return 4;
	case MCC_AST_TYPE_BOOL:
		return 4;
	case MCC_AST_TYPE_FLOAT_ARRAY: // pointer?
	case MCC_AST_TYPE_INT_ARRAY:
	case MCC_AST_TYPE_BOOL_ARRAY:
		return 4;
	default:
		return 0;
	}
}

int mcc_symbTbaseTypeToByte(enum mcc_ast_type_type type)
{
	switch (type) {
	case MCC_AST_TYPE_FLOAT_ARRAY:
		return mcc_symbTtypeToByte(MCC_AST_TYPE_FLOAT);
	case MCC_AST_TYPE_INT_ARRAY:
		return mcc_symbTtypeToByte(MCC_AST_TYPE_INT);
	case MCC_AST_TYPE_BOOL_ARRAY:
		return mcc_symbTtypeToByte(MCC_AST_TYPE_BOOL);
	default:
		return 0;
	}
}

static void buildMemoryForVariable(const char *key, void *value, void *arg)
{
	UNUSED(key);
	struct mcc_symbolAttributes *attr = value;
	int *start = arg; // actual offset

	if (attr->isParameter) {
		attr->negativeStackOffset = start[1] = start[1] + mcc_symbTtypeToByte(attr->type);
	} else {
		int offset = 0;
		switch (attr->type) {
		case MCC_AST_TYPE_FLOAT_ARRAY:
		case MCC_AST_TYPE_INT_ARRAY:
		case MCC_AST_TYPE_BOOL_ARRAY:
			offset =
			    attr->array_size * mcc_symbTbaseTypeToByte(attr->type) + mcc_symbTtypeToByte(attr->type);
			break;
		default:
			offset = mcc_symbTtypeToByte(attr->type);
			break;
		}
		attr->negativeStackOffset = start[0] = start[0] - offset;
	}
}

void mcc_symbTbuildMemory(void *tabVoid, void *arg)
{
	// creates offsets for variables within symbol table, to adress them
	// reversed order should be fine as well
	UNUSED(arg);
	struct mcc_symbolTable *tab = tabVoid;
	int start[] = {0, 4}; // first for local, second for parameter & skip 4 bytes for return address
	mapMapFunction(tab->variableMap, buildMemoryForVariable, start);
	tab->memorySpace = start[0] * -1;
}

void mcc_symbTprintList(void *table, void *arg)
{
	struct mcc_symbolTable *tab = table;
	FILE *out = arg;
	mcc_symbTprint(tab, out);
}

void mcc_symbTprint(struct mcc_symbolTable *table, FILE *out)
{
	fprintf(out, "Level: %d (%p)\n", table->level, (void *)table);
	fprintf(out, "-------------------------------------------------------------------------------------------------"
	             "----------------\n");
	fprintf(out, "|         NAME                 |        (RET)TYPE             |       SIZE / NUM PARAM       | "
	             "PARAM |  OFFSET  |\n");
	fprintf(out, "-------------------------------------------------------------------------------------------------"
	             "----------------\n");
	mapMapFunction(table->variableMap, printLine, out);
	fprintf(out, "|                              |                              |                              |   "
	             "    |          |\n");
	mapMapFunction(table->functionMap, printLine, out);
	// hashmap_iterate_key(table->myMap, printLine, out);
	fprintf(out, "-------------------------------------------------------------------------------------------------"
	             "----------------\n\n ");
}

struct mcc_symbolTable *mcc_symbTfindScope(struct mcc_symbolTable *table, const char *name)
{
	return (struct mcc_symbolTable *)mapGet(name, table->scopeMap);
}

unsigned int mcc_symbTgetMemorySpace(struct mcc_symbolTable *tab)
{
	if (tab == NULL)
		return 0;
	return tab->memorySpace;
}
