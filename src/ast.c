#include "mcc/ast.h"
#include "mcc/ast_print.h"
#include "mcc/symbol_table.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

// ---------------------------------------------------------------- Expressions

struct mcc_ast_expression *mcc_ast_new_expression_literal(struct mcc_ast_literal *literal)
{
	assert(literal);

	struct mcc_ast_expression *expr = malloc(sizeof(*expr));
	if (!expr) {
		return NULL;
	}
	expr->synthesizedType = 0;
	expr->type = MCC_AST_EXPRESSION_TYPE_LITERAL;
	expr->right_neighbor = NULL;
	expr->literal = literal;
	expr->node.symbolTable = NULL;
	return expr;
}

struct mcc_ast_expression *mcc_ast_new_expression_binary_op(enum mcc_ast_binary_op op,
                                                            struct mcc_ast_expression *lhs,
                                                            struct mcc_ast_expression *rhs)
{
	assert(lhs);
	assert(rhs);

	struct mcc_ast_expression *expr = malloc(sizeof(*expr));
	if (!expr) {
		return NULL;
	}

	expr->synthesizedType = 0;
	expr->type = MCC_AST_EXPRESSION_TYPE_BINARY_OP;
	expr->right_neighbor = NULL;
	expr->op = op;
	expr->lhs = lhs;
	expr->rhs = rhs;
	expr->node.symbolTable = NULL;
	return expr;
}

struct mcc_ast_expression *mcc_ast_new_expression_unary_op(enum mcc_ast_unary_op unary_op,
                                                           struct mcc_ast_expression *unary_next)
{
	assert(unary_next);

	struct mcc_ast_expression *expr = malloc(sizeof(*expr));
	if (!expr) {
		return NULL;
	}

	expr->synthesizedType = 0;
	expr->type = MCC_AST_EXPRESSION_TYPE_UNARY_OP;
	expr->right_neighbor = NULL;
	expr->unary_op = unary_op;
	expr->unary_next = unary_next;
	expr->node.symbolTable = NULL;
	return expr;
}

struct mcc_ast_expression *mcc_ast_new_expression_parenth(struct mcc_ast_expression *expression)
{
	assert(expression);

	struct mcc_ast_expression *expr = malloc(sizeof(*expr));
	if (!expr) {
		return NULL;
	}

	expr->synthesizedType = 0;
	expr->type = MCC_AST_EXPRESSION_TYPE_PARENTH;
	expr->right_neighbor = NULL;
	expr->expression = expression;
	expr->node.symbolTable = NULL;
	return expr;
}

struct mcc_ast_expression *mcc_ast_new_expression_identifier(char *identifier, struct mcc_ast_expression *expression)
{
	struct mcc_ast_expression *expr = malloc(sizeof(*expr));
	if (!expr) {
		return NULL; 
	}

	expr->synthesizedType = 0;
	expr->type = MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS;
	expr->right_neighbor = NULL;
	expr->identifier = identifier;
	expr->index = expression;
	expr->node.symbolTable = NULL;
	return expr;
}

struct mcc_ast_expression *mcc_ast_func_call_expression(char *callname, struct mcc_ast_expression *func_arguments)
{
	struct mcc_ast_expression *expr = malloc(sizeof(*expr));
	if (!expr) {
		return NULL;
	}

	expr->synthesizedType = 0;
	expr->type = MCC_AST_EXPREESION_TYPE_FUNCTION_CALL;
	expr->right_neighbor = NULL;
	expr->callname = callname;
	expr->functionarguments = func_arguments;
	expr->node.symbolTable = NULL;
	return expr;
}

void mcc_ast_delete_expression(struct mcc_ast_expression *expression)
{
	assert(expression);

	switch (expression->type) {
	case MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS:
		free(expression->identifier);
		if (expression->index != NULL)
			mcc_ast_delete_expression(expression->index);
		break;
	case MCC_AST_EXPRESSION_TYPE_LITERAL:
		mcc_ast_delete_literal(expression->literal);
		break;

	case MCC_AST_EXPRESSION_TYPE_BINARY_OP:
		mcc_ast_delete_expression(expression->lhs);
		mcc_ast_delete_expression(expression->rhs);
		break;

	case MCC_AST_EXPRESSION_TYPE_PARENTH:
		mcc_ast_delete_expression(expression->expression);
		break;

	case MCC_AST_EXPRESSION_TYPE_UNARY_OP:
		mcc_ast_delete_expression(expression->unary_next);
		break;

	case MCC_AST_EXPREESION_TYPE_FUNCTION_CALL:
		free(expression->callname);
		if (expression->functionarguments != NULL)
			mcc_ast_delete_expression(expression->functionarguments);
		break;
	}
	if (expression->right_neighbor != NULL) {
		mcc_ast_delete_expression(expression->right_neighbor);
	}

	free(expression);
}

// ------------------------------------------------------------------- Literals

struct mcc_ast_literal *mcc_ast_new_literal_int(long value)
{
	struct mcc_ast_literal *lit = malloc(sizeof(*lit));
	if (!lit) {
		return NULL;
	}

	lit->type = MCC_AST_LITERAL_TYPE_INT;
	lit->i_value = value;
	lit->node.symbolTable = NULL;
	return lit;
}

struct mcc_ast_literal *mcc_ast_new_literal_float(double value)
{
	struct mcc_ast_literal *lit = malloc(sizeof(*lit));
	if (!lit) {
		return NULL;
	}

	lit->type = MCC_AST_LITERAL_TYPE_FLOAT;
	lit->f_value = value;
	lit->node.symbolTable = NULL;
	return lit;
}

struct mcc_ast_literal *mcc_ast_new_literal_string(char *value)
{
	struct mcc_ast_literal *lit = malloc(sizeof(*lit));
	if (!lit) {
		return NULL;
	}

	lit->type = MCC_AST_LITERAL_TYPE_STRING;
	lit->s_value = value;
	int i = 0;
	while (lit->s_value[i] != '\0') {
		i++;
	}
	lit->s_value[i - 1] = '\0';
	lit->node.symbolTable = NULL;
	return lit;
}

struct mcc_ast_literal *mcc_ast_new_literal_bool(int value)
{
	struct mcc_ast_literal *lit = malloc(sizeof(*lit));
	if (!lit) {
		return NULL;
	}

	lit->type = MCC_AST_LITERAL_TYPE_BOOL;
	lit->b_value = value;
	lit->node.symbolTable = NULL;
	return lit;
}

void mcc_ast_delete_literal(struct mcc_ast_literal *literal)
{
	assert(literal);
	if (literal->type == MCC_AST_LITERAL_TYPE_STRING && literal->s_value != NULL)
		free(literal->s_value);
	free(literal);
}

// ------------------------------------------------------------------- Identifier

struct mcc_ast_identifier *mcc_ast_new_identifier(char *name)
{
	struct mcc_ast_identifier *id = malloc(sizeof(*id));
	if (!id) {
		return NULL;
	}

	id->name = name;
	id->node.symbolTable = NULL;
	return id;
}

void mcc_ast_delete_identifier(struct mcc_ast_identifier *identifier)
{
	if (identifier == NULL)
		return;

	assert(identifier);
	free(identifier->name);

	free(identifier);
}

// ------------------------------------------------------------------- Statements

struct mcc_ast_statement *mcc_ast_new_if_statement(struct mcc_ast_expression *cond,
                                                   struct mcc_ast_statement *truepath,
                                                   struct mcc_ast_statement *falsepath)
{
	struct mcc_ast_statement *stat = malloc(sizeof(*stat));
	if (!stat) {
		return NULL;
	}

	stat->type = MCC_AST_STATEMENT_IF;
	stat->right_neighbor = NULL;
	stat->hasReturn = 0;

	stat->condition = cond;
	stat->truepath = truepath;
	stat->falsepath = falsepath;
	stat->node.symbolTable = NULL;

	return stat;
}

struct mcc_ast_statement *mcc_ast_new_while_statement(struct mcc_ast_expression *cond,
                                                      struct mcc_ast_statement *whilebody)
{
	struct mcc_ast_statement *stat = malloc(sizeof(*stat));
	if (!stat) {
		return NULL;
	}

	stat->type = MCC_AST_STATEMENT_WHILE;
	stat->right_neighbor = NULL;
	stat->hasReturn = 0;

	stat->whilecondition = cond;
	stat->whilebody = whilebody;
	stat->node.symbolTable = NULL;
	return stat;
}

struct mcc_ast_statement *mcc_ast_new_return_statement(struct mcc_ast_expression *expr)
{
	struct mcc_ast_statement *stat = malloc(sizeof(*stat));
	if (!stat) {
		return NULL;
	}

	stat->type = MCC_AST_STATEMENT_RETURN;
	stat->right_neighbor = NULL;
	stat->hasReturn = 0;

	stat->returnExpression = expr;
	stat->node.symbolTable = NULL;
	return stat;
}

struct mcc_ast_statement *mcc_ast_new_func_statement(enum mcc_ast_type_type type,
                                                     char *identifier,
                                                     struct mcc_ast_statement *func_parameters,
                                                     struct mcc_ast_statement *func_body)
{
	struct mcc_ast_statement *stat = malloc(sizeof(*stat));
	if (!stat) {
		return NULL;
	}
	stat->type = MCC_AST_STATEMENT_FUNC_DECLARATION;
	stat->right_neighbor = NULL;
	stat->hasReturn = 0;

	stat->returnType = type;
	stat->functionName = identifier;
	stat->functionParameters = func_parameters;
	stat->functionBody = func_body;
	stat->node.symbolTable = NULL;
	return stat;
}

struct mcc_ast_statement *mcc_ast_new_expression_statement(struct mcc_ast_expression *expression)
{
	struct mcc_ast_statement *stat = malloc(sizeof(*stat));
	if (!stat) {
		return NULL;
	}
	stat->type = MCC_AST_STATEMENT_EXPRESSION;
	stat->right_neighbor = NULL;
	stat->hasReturn = 0;

	stat->returnExpression = expression;
	stat->node.symbolTable = NULL;
	return stat;
}

struct mcc_ast_statement *
mcc_ast_new_decl_statement(enum mcc_ast_type_type type, char *identifier, struct mcc_ast_expression *initialValue)
{
	struct mcc_ast_statement *stat = malloc(sizeof(*stat));
	if (!stat) {
		return NULL;
	}
	stat->type = MCC_AST_STATEMENT_TYPE_DECLARATION;
	stat->right_neighbor = NULL;
	stat->hasReturn = 0;

	stat->initialValue = initialValue;
	stat->identifier = identifier;
	stat->declType = type;
	stat->size = NULL;
	stat->node.symbolTable = NULL;
	return stat;
}

struct mcc_ast_statement *
mcc_ast_new_array_statement(enum mcc_ast_type_type type, char *identifier, struct mcc_ast_literal *size)
{
	struct mcc_ast_statement *stat = mcc_ast_new_decl_statement(type, identifier, NULL);
	stat->type = MCC_AST_STATEMENT_ARRAY_DECLARATION;
	stat->right_neighbor = NULL;
	stat->hasReturn = 0;
	switch (type) {
	case MCC_AST_TYPE_INT:
		stat->declType = MCC_AST_TYPE_INT_ARRAY;
		break;
	case MCC_AST_TYPE_BOOL:
		stat->declType = MCC_AST_TYPE_BOOL_ARRAY;
		break;
	case MCC_AST_TYPE_FLOAT:
		stat->declType = MCC_AST_TYPE_FLOAT_ARRAY;
		break;
	default:
		break;
	}
	stat->size = size;
	return stat;
}

struct mcc_ast_statement *mcc_ast_new_assign_statement(struct mcc_ast_expression *ident_expr,
                                                       struct mcc_ast_expression *expr)
{
	struct mcc_ast_statement *stat = malloc(sizeof(*stat));
	if (!stat) {
		return NULL;
	}
	stat->type = MCC_AST_STATEMENT_ASSIGNMENT;
	stat->right_neighbor = NULL;
	stat->hasReturn = 0;

	stat->statementAssignLeftside = ident_expr;
	stat->statementAssignRightside = expr;
	stat->node.symbolTable = NULL;
	return stat;
}

struct mcc_ast_statement *mcc_ast_new_block_statement(struct mcc_ast_statement *first)
{
	struct mcc_ast_statement *stat = malloc(sizeof(*stat));
	if (!stat) {
		return NULL;
	}
	stat->type = MCC_AST_STATEMENT_BLOCK;
	stat->firstStatement = first;
	stat->blockName = malloc(30*sizeof(char));
	stat->right_neighbor = NULL;
	stat->hasReturn = 0;
	stat->node.symbolTable = NULL;

	return stat;
}

struct mcc_ast_statement *mcc_ast_get_last_statement(struct mcc_ast_statement *statement)
{
	while (statement->right_neighbor != NULL) {
		statement = statement->right_neighbor;
	}
	return statement;
}

struct mcc_ast_expression *mcc_ast_get_last_argument(struct mcc_ast_expression *expression)
{
	while (expression->right_neighbor != NULL) {
		expression = expression->right_neighbor;
	}
	return expression;
}

void mcc_ast_delete_statement(struct mcc_ast_statement *stat)
{
	if (!stat)
		return;
	// assert(stat);

	switch (stat->type) {
	case MCC_AST_STATEMENT_ARRAY_DECLARATION:
		free(stat->identifier);
		mcc_ast_delete_literal(stat->size);
		break;
		// array behaves like deklaration
	case MCC_AST_STATEMENT_TYPE_DECLARATION:
		free(stat->identifier);
		/* if (stat->initialValue != NULL)
		        mcc_ast_delete_expression(stat->initialValue); */
		break;
	case MCC_AST_STATEMENT_IF:
		mcc_ast_delete_expression(stat->condition);
		mcc_ast_delete_statement(stat->truepath);
		if (stat->falsepath != NULL)
			mcc_ast_delete_statement(stat->falsepath);
		break;
	case MCC_AST_STATEMENT_WHILE:
		mcc_ast_delete_expression(stat->whilecondition);
		mcc_ast_delete_statement(stat->whilebody);
		break;
	case MCC_AST_STATEMENT_FUNC_DECLARATION:
		free(stat->functionName);
		if (stat->functionParameters != NULL)
			mcc_ast_delete_statement(stat->functionParameters);
		mcc_ast_delete_statement(stat->functionBody);
		break;
	case MCC_AST_STATEMENT_RETURN:
		if (stat->returnExpression != NULL)
			mcc_ast_delete_expression(stat->returnExpression);
		break;
	case MCC_AST_STATEMENT_EXPRESSION:
		if (stat->statementExpression != NULL)
			mcc_ast_delete_expression(stat->statementExpression);
		break;
	case MCC_AST_STATEMENT_ASSIGNMENT:
		mcc_ast_delete_expression(stat->statementAssignLeftside);
		mcc_ast_delete_expression(stat->statementAssignRightside);
		break;
	case MCC_AST_STATEMENT_BLOCK:
		mcc_ast_delete_statement(stat->firstStatement);
		free(stat->blockName);
		break;
	}

	if (stat->right_neighbor != NULL) {
		mcc_ast_delete_statement(stat->right_neighbor);
	}

	free(stat);
}
