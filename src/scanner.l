%option prefix="mcc_parser_"

%option batch
%option bison-bridge
%option bison-locations
%option noinput
%option nounput
%option noyywrap
%option reentrant
%option yylineno


%{
#include "parser.tab.h"

#define YYSTYPE MCC_PARSER_STYPE
#define YYLTYPE MCC_PARSER_LTYPE

void mcc_parser_error();

#define YY_USER_ACTION \
	yylloc->first_column = yylloc->last_column; \
	yylloc->last_column += yyleng; \
  yylloc->last_line = yylloc->first_line;
%}

bool_literal   true|false
int_literal    [[:digit:]]+
float_literal  [[:digit:]]+\.[[:digit:]]+
string_literal \"([^\\\"]|\\.)*\"
id						 [[:alpha:]][[:alnum:]_]*
                        
%x comment  
%%

%{
	if (mcc_start_rule)
	{
		int t = mcc_start_rule;
		mcc_start_rule = 0;
		return t;
	}
%}

"/*"    BEGIN(comment);

<comment>[^*\n]*        /* eat anything that's not a '*' */  /* whole block taken from flex manual http://dinosaur.compilertools.net/flex/flex_11.html */
<comment>"*"+[^*/\n]*   /* eat up '*'s not followed by '/'s */
<comment>\n             yylloc->first_line++; yylloc->last_column = 0;
<comment>"*"+"/"        BEGIN(INITIAL);

{bool_literal}   { if (yytext[0] == 't') yylval->TK_INT_LITERAL = 1;
									 else yylval->TK_INT_LITERAL = 0;
									 return TK_BOOL_LITERAL; }

{int_literal}     { yylval->TK_INT_LITERAL = atol(yytext); return TK_INT_LITERAL; }

{float_literal}   { yylval->TK_FLOAT_LITERAL = atof(yytext); return TK_FLOAT_LITERAL; }

{string_literal}  { yylval->TK_STRING_LITERAL = strdup(yytext);int i = 0; while (yytext[i] != '\0') {if (yytext[i] == '\n') {yylloc->first_line++; yylloc->last_column = 0;} i++;} ; return TK_STRING_LITERAL; }

"+"               { return TK_PLUS; }
"-"               { return TK_MINUS; }
"*"               { return TK_ASTER; }
"/"               { return TK_SLASH; }
"="               { return TK_ASSIGN; }

","               { return TK_SEPERATOR; }
";"               { return TK_TERMINATOR; }

"||"							{ return TK_LOR; }
"&&"							{ return TK_LAND; }

"=="              { return TK_EQUAL; }
"!="              { return TK_NEQUAL; }
"<"               { return TK_LESS; }
">"               { return TK_GREATER; }
"<="              { return TK_LESSEQ; }
">="              { return TK_GREATEREQ; }

"bool"						{ return TK_TYPE_BOOL; }
"int"							{ return TK_TYPE_INT; }
"float"						{ return TK_TYPE_FLOAT; }
"string"					{ return TK_TYPE_STRING; }
"void"					  { return TK_TYPE_VOID; }
"while"						{ return TK_WHILE; }
"if"						  { return TK_IF; }
"else"					  { return TK_ELSE; }
"return"					{ return TK_RETURN; }

"!"               { return TK_NOT; }

"("               { return TK_LPARENTH; }
")"               { return TK_RPARENTH; }
"["								{ return TK_LPARRECT; }
"]"								{ return TK_RPARRECT; }
"{"               { return TK_LBRACKET; }
"}"               { return TK_RBRACKET; }

{id}							{ yylval->TK_IDENTIFIER = malloc(yyleng+1); strcpy(yylval->TK_IDENTIFIER, yytext); return TK_IDENTIFIER; }

[ \t\r]+          { /* ignore */ }

[\n]              { yylloc->first_line++; yylloc->last_column = 0; }

<<EOF>>           { return TK_END; }

.                 { char msg[32]; snprintf(msg, 32, "invalid character '%s'\n", yytext); mcc_parser_error(yylloc, NULL, NULL, NULL, msg, "filename", 4);}
