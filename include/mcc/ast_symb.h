// AST Synthesize Infrastructure
//
// This module provides basic synthing infrastructure for the AST data
// structure.

#ifndef MCC_AST_SYMB_H
#define MCC_AST_SYMB_H

#include <stdio.h>

#include "mcc/ast.h"
#include "mcc/list.h"

enum mcc_symbolTableOptions {
	MCC_SYMBT_OPTION_PRINT_SYMBOL_TABLE = 1,
};

enum mcc_symbolTableStatus {
	MCC_SYMBT_STATUS_OK = 0,
	MCC_SYMBT_STATUS_ERROR = 1,
};

const char *mcc_ast_symb_binary_op(enum mcc_ast_binary_op op);

const char *mcc_ast_symb_unary_op(enum mcc_ast_unary_op op);

const char *mcc_ast_symb_type(enum mcc_ast_type_type type);

// ---------------------------------------------------------------- DOT Printer

enum mcc_symbolTableStatus mcc_ast_symb_expression(FILE *out,
                                                   struct mcc_ast_expression *expression,
                                                   enum mcc_symbolTableOptions options,
                                                   list_t **allSymbolTableList);

enum mcc_symbolTableStatus mcc_ast_symb_literal(FILE *out,
                                                struct mcc_ast_literal *literal,
                                                enum mcc_symbolTableOptions options,
                                                list_t **allSymbolTableList);

enum mcc_symbolTableStatus mcc_ast_symb_statement(FILE *out,
                                                  struct mcc_ast_statement *statement,
                                                  enum mcc_symbolTableOptions options,
                                                  list_t **allSymbolTableList);

enum mcc_symbolTableStatus mcc_ast_symb_start(FILE *out,
                                              struct mcc_ast_statement *statement,
                                              enum mcc_symbolTableOptions options,
                                              char *filename,
                                              list_t **allSymbolTableList);

void mcc_ast_symb_clear_symTab(list_t *allSymbolTableList);

// clang-format off

#define mcc_ast_symb(out, x, opt, symTabl) _Generic((x),                 \
		struct mcc_ast_expression *: mcc_ast_symb_expression, \
		struct mcc_ast_literal *:    mcc_ast_symb_literal, \
		struct mcc_ast_statement *:  mcc_ast_symb_statement \
	)(out, x, opt, symTabl)

// clang-format on

#endif // MCC_AST_SYMB_H
