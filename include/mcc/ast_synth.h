// AST Synthesize Infrastructure
//
// This module provides basic synthing infrastructure for the AST data
// structure.

#ifndef MCC_AST_SYNTH_H
#define MCC_AST_SYNTH_H

#include "mcc/ast.h"
#include <stdio.h>
#include <stdlib.h>

enum mcc_semanticOptions { EMTPY };

enum mcc_sythesizeStatus {
	MCC_SYNTH_STATUS_OK = EXIT_SUCCESS,
	MCC_SYNTH_STATUS_ERROR = EXIT_FAILURE,
};

const char *mcc_ast_synth_unary_op(enum mcc_ast_unary_op op);

const char *mcc_ast_synth_type(enum mcc_ast_type_type type);

// ---------------------------------------------------------------- DOT Printer

enum mcc_sythesizeStatus
mcc_ast_synth_expression(FILE *out, struct mcc_ast_expression *expression, enum mcc_semanticOptions options);

enum mcc_sythesizeStatus
mcc_ast_synth_literal(FILE *out, struct mcc_ast_literal *literal, enum mcc_semanticOptions options);

enum mcc_sythesizeStatus
mcc_ast_synth_statement(FILE *out, struct mcc_ast_statement *statement, enum mcc_semanticOptions options);

enum mcc_sythesizeStatus
mcc_ast_synth_start(FILE *out, struct mcc_ast_statement *statement, enum mcc_semanticOptions options, char *filename);

// clang-format off

#define mcc_ast_synth(out, x, opt) _Generic((x),                 \
		struct mcc_ast_expression *: mcc_ast_synth_expression, \
		struct mcc_ast_literal *:    mcc_ast_synth_literal, \
		struct mcc_ast_statement *:  mcc_ast_synth_statement \
	)(out, x, opt)

// clang-format on

#endif // MCC_AST_SYNTH_H
