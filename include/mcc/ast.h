// Abstract Syntax Tree (AST)
//
// Here we define the AST data structure of the compiler. It mainly consists of
// *tagged unions* for node types and enums for operators.
//
// In addition to the node type specific members, each node features a common
// member `mmc_ast_node` which serves as a *base-class*. It holds data
// independent from the actual node type, like the source location.
//
// Also note that this makes excessive use of C11's *anonymous structs and
// unions* feature.

#ifndef MCC_AST_H
#define MCC_AST_H

// Forward Declarations
struct mcc_ast_expression;
struct mcc_ast_literal;
struct mcc_ast_statement;
struct mcc_ast_identifier;

// ------------------------------------------------------------------- AST Node

struct mcc_ast_source_location {
	int start_line;
	int start_col;
	int end_line;
	int end_col;
};

struct mcc_ast_node {
	struct mcc_ast_source_location sloc;
	struct mcc_symbolTable *symbolTable;
};

// ------------------------------------------------------------------ Types

enum mcc_ast_type_type {
	MCC_AST_TYPE_UNKNOWN,
	MCC_AST_TYPE_VOID,
	MCC_AST_TYPE_FLOAT,
	MCC_AST_TYPE_INT,
	MCC_AST_TYPE_STRING,
	MCC_AST_TYPE_BOOL,
	MCC_AST_TYPE_INT_ARRAY,
	MCC_AST_TYPE_FLOAT_ARRAY,
	MCC_AST_TYPE_BOOL_ARRAY,
};

// ------------------------------------------------------------------ Operators

enum mcc_ast_unary_op {
	MCC_AST_UNARY_OP_NOT,
	MCC_AST_UNARY_OP_NEG,
};

enum mcc_ast_binary_op {
	MCC_AST_BINARY_OP_ADD,
	MCC_AST_BINARY_OP_SUB,
	MCC_AST_BINARY_OP_MUL,
	MCC_AST_BINARY_OP_DIV,
	MCC_AST_BINARY_OP_EQU,
	MCC_AST_BINARY_OP_NEQ,
	MCC_AST_BINARY_OP_LES,
	MCC_AST_BINARY_OP_GRT,
	MCC_AST_BINARY_OP_LEQ,
	MCC_AST_BINARY_OP_GEQ,
	MCC_AST_BINARY_OP_AND,
	MCC_AST_BINARY_OP_OR,
	MCC_AST_BINARY_SEPERATE,
};

// ---------------------------------------------------------------- Expressions

enum mcc_ast_expression_type {
	MCC_AST_EXPRESSION_TYPE_LITERAL,
	MCC_AST_EXPRESSION_TYPE_BINARY_OP,
	MCC_AST_EXPRESSION_TYPE_PARENTH,
	MCC_AST_EXPRESSION_TYPE_UNARY_OP,
	MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS,
	MCC_AST_EXPREESION_TYPE_FUNCTION_CALL,
};

enum mcc_ast_statement_type {
	MCC_AST_STATEMENT_FUNC_DECLARATION,
	MCC_AST_STATEMENT_TYPE_DECLARATION,
	MCC_AST_STATEMENT_ARRAY_DECLARATION,
	MCC_AST_STATEMENT_IF,
	MCC_AST_STATEMENT_WHILE,
	MCC_AST_STATEMENT_RETURN,
	MCC_AST_STATEMENT_EXPRESSION,
	MCC_AST_STATEMENT_ASSIGNMENT,
	MCC_AST_STATEMENT_BLOCK,
};

struct mcc_ast_expression {
	struct mcc_ast_node node;
	struct mcc_ast_expression *right_neighbor;

	enum mcc_ast_type_type synthesizedType;

	enum mcc_ast_expression_type type;
	union {
		// MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS
		struct {
			char *identifier;
			struct mcc_ast_expression *index;
		};

		// MCC_AST_EXPRESSION_TYPE_LITERAL
		struct mcc_ast_literal *literal;

		// MCC_AST_EXPRESSION_TYPE_BINARY_OP
		struct {
			enum mcc_ast_binary_op op;
			struct mcc_ast_expression *lhs;
			struct mcc_ast_expression *rhs;
		};
		struct {
			enum mcc_ast_unary_op unary_op;
			struct mcc_ast_expression *unary_next;
		};

		// MCC_AST_EXPRESSION_TYPE_FUNC_CALL
		struct {
			char *callname;
			struct mcc_ast_expression *functionarguments;
		};
		// MCC_AST_EXPRESSION_TYPE_PARENTH
		struct mcc_ast_expression *expression;
	};
};

struct mcc_ast_statement {
	struct mcc_ast_node node;

	int hasReturn;
	enum mcc_ast_statement_type type;
	union {
		//IF
		struct {
			struct mcc_ast_expression *condition;
			struct mcc_ast_statement *truepath;
			struct mcc_ast_statement *falsepath;
		};
		//Declaration
		struct {
			enum mcc_ast_type_type declType;
			char *identifier;
			struct mcc_ast_expression *initialValue;
			struct mcc_ast_literal *size;
		};
		//WHILE
		struct {
			struct mcc_ast_expression *whilecondition;
			struct mcc_ast_statement *whilebody;
		};
		//RETURN
		struct {
			struct mcc_ast_expression *returnExpression;
		};
		//FUNCTION declaration
		struct {
			enum mcc_ast_type_type returnType;
			char *functionName;
			struct mcc_ast_statement *functionParameters;
			struct mcc_ast_statement *functionBody;
		};
		//BLOCK
		struct {
			char *blockName;
			struct mcc_ast_statement *firstStatement;
		};
		/* 		struct
		                {
		                        struct mcc_ast_identifier *callname;
		                        struct mcc_ast_expression *functionarguments;
		                }; */
		//Block
		struct {
			struct mcc_ast_expression *statementExpression;
		};
		//ASSIGN
		struct {
			struct mcc_ast_expression *statementAssignLeftside;
			struct mcc_ast_expression *statementAssignRightside;
		};
	};

	struct mcc_ast_statement *right_neighbor;
};

void mcc_clear_statements();

struct mcc_ast_expression *mcc_ast_new_expression_literal(struct mcc_ast_literal *literal);

struct mcc_ast_expression *mcc_ast_new_expression_binary_op(enum mcc_ast_binary_op op,
                                                            struct mcc_ast_expression *lhs,
                                                            struct mcc_ast_expression *rhs);

struct mcc_ast_expression *mcc_ast_new_expression_unary_op(enum mcc_ast_unary_op op, struct mcc_ast_expression *exp);

struct mcc_ast_expression *mcc_ast_new_expression_parenth(struct mcc_ast_expression *expression);
struct mcc_ast_expression *mcc_ast_new_expression_identifier(char *identifier, struct mcc_ast_expression *expression);
struct mcc_ast_expression *mcc_ast_func_call_expression(char *identifier, struct mcc_ast_expression *func_arguments);

struct mcc_ast_statement *mcc_ast_new_if_statement(struct mcc_ast_expression *condition,
                                                   struct mcc_ast_statement *truepath,
                                                   struct mcc_ast_statement *falsepath);
struct mcc_ast_statement *mcc_ast_new_while_statement(struct mcc_ast_expression *condition,
                                                      struct mcc_ast_statement *whilebody);
struct mcc_ast_statement *mcc_ast_new_return_statement(struct mcc_ast_expression *returnExpression);
struct mcc_ast_statement *
mcc_ast_new_decl_statement(enum mcc_ast_type_type, char *identifier, struct mcc_ast_expression *initialValue);
struct mcc_ast_statement *
mcc_ast_new_array_statement(enum mcc_ast_type_type, char *identifier, struct mcc_ast_literal *size);
struct mcc_ast_statement *mcc_ast_new_func_statement(enum mcc_ast_type_type,
                                                     char *identifier,
                                                     struct mcc_ast_statement *func_parameter,
                                                     struct mcc_ast_statement *func_body);
struct mcc_ast_statement *mcc_ast_func_call_statement(char *callname, struct mcc_ast_expression *func_arguments);
struct mcc_ast_statement *mcc_ast_new_expression_statement(struct mcc_ast_expression *func_arguments);
struct mcc_ast_statement *mcc_ast_new_assign_statement(struct mcc_ast_expression *ident_expr,
                                                       struct mcc_ast_expression *expr);
struct mcc_ast_statement *mcc_ast_new_block_statement(struct mcc_ast_statement *first);

struct mcc_ast_statement *mcc_ast_get_last_statement(struct mcc_ast_statement *statement);
struct mcc_ast_expression *mcc_ast_get_last_argument(struct mcc_ast_expression *expression);

struct mcc_ast_identifier *mcc_ast_new_identifier(char *name);

void mcc_ast_delete_expression(struct mcc_ast_expression *expression);
void mcc_ast_delete_statement(struct mcc_ast_statement *statement);

int mcc_ast_get_literalType(struct mcc_ast_expression *expression);
void mcc_ast_sythesize(struct mcc_ast_statement *statement);

// ------------------------------------------------------------------- Literals

enum mcc_ast_literal_type {
	MCC_AST_LITERAL_TYPE_INT,
	MCC_AST_LITERAL_TYPE_FLOAT,
	MCC_AST_LITERAL_TYPE_STRING,
	MCC_AST_LITERAL_TYPE_BOOL,
};

struct mcc_ast_literal {
	struct mcc_ast_node node;

	enum mcc_ast_literal_type type;
	union {
		// MCC_AST_LITERAL_TYPE_INT
		long i_value;

		// MCC_AST_LITERAL_TYPE_FLOAT
		double f_value;

		// MCC_AST_LITERAL_TYPE_STRING
		char *s_value;

		// MCC_AST_LITERAL_TYPE_BOOL
		int b_value;
	};
};

struct mcc_ast_identifier {
	struct mcc_ast_node node;
	char *name;
};

struct mcc_ast_literal *mcc_ast_new_literal_int(long value);

struct mcc_ast_literal *mcc_ast_new_literal_float(double value);

struct mcc_ast_literal *mcc_ast_new_literal_string(char *value);

struct mcc_ast_literal *mcc_ast_new_literal_bool(int value);

void mcc_ast_delete_literal(struct mcc_ast_literal *literal);

void mcc_ast_delete_identifier(struct mcc_ast_identifier *identifier);

// -------------------------------------------------------------------- Utility

// clang-format off

#define mcc_ast_delete(x) _Generic((x), \
		struct mcc_ast_expression *: mcc_ast_delete_expression, \
		struct mcc_ast_literal *:    mcc_ast_delete_literal, \
		struct mcc_ast_statement *:  mcc_ast_delete_statement, \
		struct mcc_ast_identifier *:  mcc_ast_delete_identifier \
	)(x)

// clang-format on

#endif // MCC_AST_H
