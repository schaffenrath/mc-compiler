
#include "ast.h"
#include "list.h"
#include <stdio.h>
#include <string.h>

#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

struct mcc_symbolTable;

struct mcc_symbolAttributes {
	enum mcc_ast_type_type type;
	list_t *parameter_type_list;
	unsigned int array_size;
	int negativeStackOffset;
	int isParameter;
};

enum mcc_symbolSearch { MCC_SYMBOL_FUNC_ONLY, MCC_SYMBOL_VARIABLE_ONLY, MCC_SYMBOL_ALL };

struct mcc_symbolTable *mcc_symbTcreate(struct mcc_symbolTable *parent, const char *name);
struct mcc_symbolTable *mcc_symbTcreateRoot();

int mcc_symbTtypeToByte(enum mcc_ast_type_type type);
int mcc_symbTbaseTypeToByte(enum mcc_ast_type_type type);

char *mcc_symbTaddSymbolFunction(struct mcc_symbolTable *tab, const char *symbol, struct mcc_symbolAttributes *attr);
char *mcc_symbTaddSymbolVariable(struct mcc_symbolTable *tab, const char *symbol, struct mcc_symbolAttributes *attr);
// void mcc_symbTaddSymbol(struct mcc_symbolTable *tab, const char *symbol, struct mcc_symbolAttributes *attr);
struct mcc_symbolAttributes *
mcc_symbTlookup(struct mcc_symbolTable *tab, const char *symbol, enum mcc_symbolSearch syms);
struct mcc_symbolAttributes *
mcc_symbTlookupRec(struct mcc_symbolTable *tab, const char *symbol, enum mcc_symbolSearch syms);
struct mcc_symbolAttributes *
mcc_symbTlookupRec2(struct mcc_symbolTable *tab, const char *symbol, enum mcc_symbolSearch syms, int *offset);
struct mcc_symbolTable *mcc_symbTgetParent(struct mcc_symbolTable *tab);

void mcc_symbTsetFunction(struct mcc_symbolTable *tab, struct mcc_symbolAttributes *attr);
struct mcc_symbolAttributes *mcc_symbTgetFunction(struct mcc_symbolTable *tab);

void mcc_symbTbuildMemory(void *tab, void *arg);

void mcc_symbTprint(struct mcc_symbolTable *table, FILE *out);
void mcc_symbTprintList(void *table, void *arg);

struct mcc_symbolTable *mcc_symbTfindScope(struct mcc_symbolTable *table, const char *name);
unsigned int mcc_symbTgetMemorySpace(struct mcc_symbolTable *tab);

void mcc_symbTdelete(struct mcc_symbolTable *table);

#endif
