// AST Intermediate representation
//
// This module

#ifndef MCC_IR
#define MCC_IR

#include "mcc/ast.h"
#include "mcc/symbol_table.h"
#include <stdio.h>

enum mcc_ir_op_type {
	MCC_IR_OP_TYPE_UN_NEGATIVE,
	MCC_IR_OP_TYPE_UN_NEGATION,
	MCC_IR_OP_TYPE_BI_ADDITION,
	MCC_IR_OP_TYPE_BI_SUBRACTION,
	MCC_IR_OP_TYPE_BI_MULTIPLICATION,
	MCC_IR_OP_TYPE_BI_DIVISION,
	MCC_IR_OP_TYPE_BI_LOG_AND,
	MCC_IR_OP_TYPE_BI_LOG_OR,
	MCC_IR_OP_TYPE_BI_EQ,
	MCC_IR_OP_TYPE_BI_LO,
	MCC_IR_OP_TYPE_COPY,
	MCC_IR_OP_TYPE_JUMP,
	MCC_IR_OP_TYPE_IF_NOT_JUMP,
	MCC_IR_OP_TYPE_LABEL,
	MCC_IR_OP_TYPE_PREP_PARAM,
	MCC_IR_OP_TYPE_CALL,
	MCC_IR_OP_TYPE_LOAD,
	MCC_IR_OP_TYPE_STORE,
	//MCC_IR_OP_TYPE_ADDRESS,
	//MCC_IR_OP_TYPE_POINTER,
	MCC_IR_OP_TYPE_RETURN,
	MCC_IR_OP_TYPE_OPEN_BLOCK,
	MCC_IR_OP_TYPE_CLOSE_BLOCK,
	MCC_IR_OP_TYPE_BUILD_ARRAY,
};

typedef struct mcc_ir mcc_ir_t;
typedef mcc_ir_t *mcc_ir_ptr_t;

mcc_ir_ptr_t mcc_ir_createIR();

typedef void (*mcc_ir_it_fun_t)(enum mcc_ir_op_type type,
                                char *arg1,
                                char *arg2,
                                unsigned int arg2n,
                                char *res,
                                struct mcc_symbolTable *tab,
                                void *arg);

char *mcc_ir_createTemporary(mcc_ir_ptr_t ir, struct mcc_symbolTable *table, enum mcc_ast_type_type type);
char *mcc_ir_createTemporaryTemplate(mcc_ir_ptr_t ir,
                                     struct mcc_symbolTable *table,
                                     enum mcc_ast_type_type type,
                                     char *template);

void mcc_ir_addOperationUNeg(mcc_ir_ptr_t ir, char *from, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationUNot(mcc_ir_ptr_t ir, char *from, char *to, struct mcc_symbolTable *tab);

void mcc_ir_addOperationBAdd(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationBSub(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationBMul(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationBDiv(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationBAnd(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationBOr(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationBEq(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationBL(mcc_ir_ptr_t ir, char *op1, char *op2, char *to, struct mcc_symbolTable *tab);

void mcc_ir_addOperationCopy(mcc_ir_ptr_t ir, char *from, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationJump(mcc_ir_ptr_t ir, char *label, struct mcc_symbolTable *tab);
void mcc_ir_addOperationIfNotJump(mcc_ir_ptr_t ir, char *label, char *false_cond, struct mcc_symbolTable *tab);
void mcc_ir_addOperationLabel(mcc_ir_ptr_t ir, char *label, struct mcc_symbolTable *tab);
void mcc_ir_addOperationPrepParam(mcc_ir_ptr_t ir, char *parameter, struct mcc_symbolTable *tab);
void mcc_ir_addOperationReturn(mcc_ir_ptr_t ir, char *returnvalue, struct mcc_symbolTable *tab);
void mcc_ir_addOperationCall(mcc_ir_ptr_t ir,
                             char *function,
                             unsigned int parameterNumbers,
                             struct mcc_symbolTable *tab); // does this work like this?
void mcc_ir_addOperationLoad(mcc_ir_ptr_t ir, char *from, char *index, char *to, struct mcc_symbolTable *tab);
void mcc_ir_addOperationStore(mcc_ir_ptr_t ir, char *from, char *index, char *where, struct mcc_symbolTable *tab);
//void mcc_ir_addOperationAddress(mcc_ir_ptr_t ir, char *from, char *to, struct mcc_symbolTable *tab);
//void mcc_ir_addOperationPointer(mcc_ir_ptr_t ir, char *from, char *to, struct mcc_symbolTable *tab);

void mcc_ir_addOperationOpenBlock(mcc_ir_ptr_t ir, char *blockName, struct mcc_symbolTable *tab);
void mcc_ir_addOperationCloseBlock(mcc_ir_ptr_t ir, char *blockName, struct mcc_symbolTable *tab);
void mcc_ir_addOperationBuildArray(mcc_ir_ptr_t ir, char *varName, struct mcc_symbolTable *tab);

char *mcc_ir_createTemporaryLabel(mcc_ir_ptr_t ir, char *name, int max, const char *template);

void mcc_ir_print(mcc_ir_ptr_t ir, FILE *out);

void mcc_ir_addLiteral(mcc_ir_ptr_t ir, char *literal);

void mcc_ir_build_bb(mcc_ir_ptr_t ir);
void mcc_ir_print_cfg(mcc_ir_ptr_t ir, FILE *out);

struct mcc_symbolTable* mcc_ir_find_base_symbolTable(mcc_ir_ptr_t ir);

void mcc_ir_iterate(mcc_ir_ptr_t ir, mcc_ir_it_fun_t function, void *arg);

void mcc_ir_delete(mcc_ir_ptr_t ir);

#endif // MCC_IR
