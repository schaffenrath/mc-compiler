// AST Intermediate representation
//
// This module transforms a given AST to our internal representation

#ifndef MCC_AST_IR
#define MCC_AST_IR

#include <stdio.h>

#include "mcc/ast.h"
#include "mcc/ir.h"

enum mcc_irStatus {
	MCC_IR_STATUS_OK = 0,
	MCC_IR_STATUS_ERROR = 1,
};

// ---------------------------------------------------------------- IR

void mcc_ir_visit_statement(struct mcc_ast_statement *statement, void *data);
void mcc_ir_visit_expression(struct mcc_ast_expression *expression, void *data);

enum mcc_irStatus mcc_ast_ir_expression(FILE *out, struct mcc_ast_expression *expression, mcc_ir_ptr_t *ir);

enum mcc_irStatus mcc_ast_ir_statement(FILE *out, struct mcc_ast_statement *statement, mcc_ir_ptr_t *ir);

// clang-format off

#define mcc_ast_ir(out, x) _Generic((x), \
		struct mcc_ast_expression *: mcc_ast_ir_expression, \
		struct mcc_ast_statement *:  mcc_ast_ir_statement, \
	)(out, x)

// clang-format on

#endif // MCC_AST_IR
