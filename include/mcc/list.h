
#ifndef __LIST_H__
#define __LIST_H__

enum list_options{
  LIST_OPTIONS_OWNER = 1
};

typedef struct test{
  void *data;
  struct test* next;
} list_element_t;

typedef struct{
  list_element_t* first;
  list_element_t* last;
  unsigned int size;
  enum list_options options;
} list_t;

typedef list_t* list_ptr_t;

list_ptr_t list_create(enum list_options);

void list_append(list_ptr_t, void* data);

void *list_get(list_ptr_t, unsigned int i);

void list_remove_idx(list_ptr_t, unsigned int i);

//void list_remove(list_ptr_t, void* data);

void list_push(list_ptr_t, void* data);

void* list_top(list_ptr_t);

void list_pop(list_ptr_t);

int list_find(list_ptr_t, void* data);

void list_iterate_idx(list_ptr_t, unsigned int from, unsigned int to, void(*fun)(void*, void*), void* arg);

void list_iterate(list_ptr_t, void(*fun)(void*, void*), void* arg);

void list_free(list_ptr_t);

#endif