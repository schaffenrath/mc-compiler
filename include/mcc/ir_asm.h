// AST Intermediate representation
//
// This module

#ifndef MCC_IR_ASM
#define MCC_IR_ASM

#include "mcc/ir.h"
#include "mcc/symbol_table.h"
#include <stdio.h>

typedef struct mcc_asm {
	list_t *code;
	list_t *data;
	int labelCounter;
	unsigned int nestingCounter;
} mcc_asm_t;

typedef struct mcc_asm mcc_asm_t;
typedef mcc_asm_t *mcc_asm_ptr_t;

mcc_asm_ptr_t mcc_ir_asm_create_assembler();

void mcc_ir_asm_translate(mcc_ir_ptr_t ir, mcc_asm_ptr_t assembler);
void mcc_ir_asm_to_output(FILE *out, mcc_asm_ptr_t assembler);
void mcc_ir_asm_to_output_f_flag(FILE *out, mcc_asm_ptr_t assembler);

void mcc_ir_asm_delete(mcc_asm_ptr_t assembler);

#endif // MCC_IR_ASM