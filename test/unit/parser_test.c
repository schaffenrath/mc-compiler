#include <CuTest.h>

#include "mcc/ast.h"
#include "mcc/parser.h"

// Threshold for floating point comparisions.
static const double EPS = 1e-3;

void BinaryOp_1(CuTest *tc)
{
	const char input[] = "192 + 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_ADD, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_2(CuTest *tc)
{
	const char input[] = "192 - 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_SUB, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_3(CuTest *tc)
{
	const char input[] = "192 / 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_DIV, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_4(CuTest *tc)
{
	const char input[] = "192 * 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_MUL, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_5(CuTest *tc)
{
	const char input[] = "192 == 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_EQU, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_6(CuTest *tc)
{
	const char input[] = "192 != 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_NEQ, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_7(CuTest *tc)
{
	const char input[] = "192 < 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_LES, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_8(CuTest *tc)
{
	const char input[] = "192 > 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_GRT, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_9(CuTest *tc)
{
	const char input[] = "192 <= 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_LEQ, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_10(CuTest *tc)
{
	const char input[] = "192 >= 3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_GEQ, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 3.14, expr->rhs->literal->f_value, EPS);

	mcc_ast_delete(expr);
}

void BinaryOp_11(CuTest *tc)
{
	const char input[] = "true && false";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_AND, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_BOOL, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 1, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_BOOL, expr->rhs->literal->type);
	CuAssertIntEquals(tc, 0, expr->rhs->literal->i_value);

	mcc_ast_delete(expr);
}

void BinaryOp_12(CuTest *tc)
{
	const char input[] = "true || false";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_OR, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_BOOL, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 1, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_BOOL, expr->rhs->literal->type);
	CuAssertIntEquals(tc, 0, expr->rhs->literal->i_value);

	mcc_ast_delete(expr);
}

void BinaryOp_13(CuTest *tc)
{
	const char input[] = "3.14, 420";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_SEPERATE, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 3.14, expr->lhs->literal->f_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);

	// root -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->rhs->literal->type);
	CuAssertDblEquals(tc, 420, expr->rhs->literal->i_value, EPS);

	mcc_ast_delete(expr);
}

void UnaryOp_1(CuTest *tc)
{
	const char input[] = "!true";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_UNARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_UNARY_OP_NOT, expr->unary_op);

	// root -> next
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->unary_next->type);

	// root -> next -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_BOOL, expr->unary_next->literal->type);
	CuAssertIntEquals(tc, 1, expr->unary_next->literal->i_value);

	mcc_ast_delete(expr);
}

void UnaryOp_2(CuTest *tc)
{
	const char input[] = "-3.14";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_UNARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_UNARY_OP_NEG, expr->unary_op);

	// root -> next
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->unary_next->type);

	// root -> next -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, expr->unary_next->literal->type);
	CuAssertIntEquals(tc, 3.14, expr->unary_next->literal->f_value);

	mcc_ast_delete(expr);
}

void NestedExpression_1(CuTest *tc)
{
	const char input[] = "42 * (192 + 3.14)";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	// root
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_MUL, expr->op);

	// root -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->type);

	// root -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->literal->type);
	CuAssertIntEquals(tc, 42, expr->lhs->literal->i_value);

	// root -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_PARENTH, expr->rhs->type);

	struct mcc_ast_expression *subexpr = expr->rhs->expression;

	// subexpr
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, subexpr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_ADD, subexpr->op);

	// subexpr -> lhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, subexpr->lhs->type);

	// subexpr -> lhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, subexpr->lhs->literal->type);
	CuAssertIntEquals(tc, 192, subexpr->lhs->literal->i_value);

	// subexpr -> rhs
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, subexpr->rhs->type);

	// subexpr -> rhs -> literal
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_FLOAT, subexpr->rhs->literal->type);
	CuAssertIntEquals(tc, 3.14, subexpr->rhs->literal->f_value);

	mcc_ast_delete(expr);
}

void MissingClosingParenthesis_1(CuTest *tc)
{
	const char input[] = "(42";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertTrue(tc, MCC_PARSER_STATUS_OK != result.status);
	CuAssertTrue(tc, NULL == result.expression);
}

void SourceLocation_SingleLineColumn(CuTest *tc)
{
	const char input[] = "(42 + 192)";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_PARENTH, expr->type);
	CuAssertIntEquals(tc, 1, expr->node.sloc.start_col);

	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->expression->type);
	CuAssertIntEquals(tc, 2, expr->expression->node.sloc.start_col);

	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->expression->lhs->literal->type);
	CuAssertIntEquals(tc, 2, expr->expression->lhs->literal->node.sloc.start_col);

	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->expression->rhs->literal->type);
	CuAssertIntEquals(tc, 7, expr->expression->rhs->literal->node.sloc.start_col);

	mcc_ast_delete(expr);
}

void SourceLocation_MultiLineColumn(CuTest *tc)
{
	const char input[] = "(42 + \n 192)";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_PARENTH, expr->type);
	CuAssertIntEquals(tc, 1, expr->node.sloc.start_col);
	CuAssertIntEquals(tc, 1, expr->node.sloc.start_line);

	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->expression->type);
	CuAssertIntEquals(tc, 2, expr->expression->node.sloc.start_col);
	CuAssertIntEquals(tc, 1, expr->expression->node.sloc.start_line);

	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->expression->lhs->literal->type);
	CuAssertIntEquals(tc, 2, expr->expression->lhs->literal->node.sloc.start_col);
	CuAssertIntEquals(tc, 1, expr->expression->lhs->literal->node.sloc.start_line);

	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->expression->rhs->literal->type);
	CuAssertIntEquals(tc, 1, expr->expression->rhs->literal->node.sloc.start_col);
	CuAssertIntEquals(tc, 2, expr->expression->rhs->literal->node.sloc.start_line);

	mcc_ast_delete(expr);
}

void ArrayAssignment2(CuTest *tc)
{
	const char input[] = "a = b;";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);
}

void ArrayAssignment(CuTest *tc)
{
	const char input[] = "a[7] = b;";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_statement *stmnt = result.statement;

	CuAssertIntEquals(tc, MCC_AST_STATEMENT_ASSIGNMENT, stmnt->type);

	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS, stmnt->statementAssignLeftside->type);
	CuAssertStrEquals(tc, "a", stmnt->statementAssignLeftside->identifier);
	CuAssertPtrNotNull(tc, stmnt->statementAssignLeftside->index);

	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_VARIABLE_ACCESS, stmnt->statementAssignRightside->type);
	CuAssertStrEquals(tc, "b", stmnt->statementAssignRightside->identifier);

	mcc_ast_delete(stmnt);
}

void FunctionCallStmt(CuTest *tc)
{
	const char input[] = "fun(foo, bar, 4);";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_statement *stmnt = result.statement;

	CuAssertIntEquals(tc, MCC_AST_STATEMENT_EXPRESSION, stmnt->type);
	CuAssertPtrNotNull(tc, stmnt->statementExpression);

	mcc_ast_delete(stmnt);
}

void FunctionCallexpr(CuTest *tc)
{
	const char input[] = "fun(foo, bar, 4)";
	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	CuAssertIntEquals(tc, MCC_AST_EXPREESION_TYPE_FUNCTION_CALL, expr->type);

	CuAssertStrEquals(tc, "fun", expr->identifier);
	CuAssertPtrNotNull(tc, expr->functionarguments);

	mcc_ast_delete(expr);
}

void DanglingElse(CuTest *tc) /* else should bind to nearest if statement (inner one)  */
{
	const char input[] = "if (true) if (false) return; else return 2;";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_statement *stat = result.statement;
	/*root */
	CuAssertIntEquals(tc, MCC_AST_STATEMENT_IF, stat->type);

	/* root -> falsepath is empty */
	CuAssertPtrEquals(tc, NULL, stat->falsepath);

	/* root -> truepath is if statement with else */
	CuAssertIntEquals(tc, MCC_AST_STATEMENT_IF, stat->truepath->type);

	/* root -> truepath -> truepath is return statement */
	CuAssertIntEquals(tc, MCC_AST_STATEMENT_RETURN, stat->truepath->truepath->type);
	CuAssertPtrEquals(tc, NULL, stat->truepath->truepath->returnExpression);

	/* root -> truepath -> falsepath is return statement with expression */
	CuAssertIntEquals(tc, MCC_AST_STATEMENT_RETURN, stat->truepath->falsepath->type);
	CuAssertIntEquals(tc, 2, stat->truepath->falsepath->returnExpression->literal->i_value);

	mcc_ast_delete(stat);
}

void PrecedenceMul(CuTest *tc)
{
	const char input[] = "3 + 4 * 5 - 6";

	/*
	  (-)
	 /   \
	(+)  (6)
       /   \
     (3)  (*)
	  /   \
	 (4)  (5)    */

	struct mcc_parser_result result = mcc_parse_string_expression(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	struct mcc_ast_expression *expr = result.expression;

	/*root */
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_SUB, expr->op);

	/* root -> lhs is addition */
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->lhs->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_ADD, expr->lhs->op);

	/* root -> rhs is literal 6 */
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->rhs->type);
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->rhs->literal->type);
	CuAssertIntEquals(tc, 6, expr->rhs->literal->i_value);

	/* root -> lhs -> lhs is literal 3 */
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->lhs->type);
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->lhs->literal->type);
	CuAssertIntEquals(tc, 3, expr->lhs->lhs->literal->i_value);

	/* root -> lhs -> rhs is multiplication expression */
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_BINARY_OP, expr->lhs->rhs->type);
	CuAssertIntEquals(tc, MCC_AST_BINARY_OP_MUL, expr->lhs->rhs->op);

	/* root -> lhs -> rhs -> lhs is literal 4 */
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->rhs->lhs->type);
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->rhs->lhs->literal->type);
	CuAssertIntEquals(tc, 4, expr->lhs->rhs->lhs->literal->i_value);

	/* root -> lhs -> rhs -> lhs is literal 5 */
	CuAssertIntEquals(tc, MCC_AST_EXPRESSION_TYPE_LITERAL, expr->lhs->rhs->rhs->type);
	CuAssertIntEquals(tc, MCC_AST_LITERAL_TYPE_INT, expr->lhs->rhs->rhs->literal->type);
	CuAssertIntEquals(tc, 5, expr->lhs->rhs->rhs->literal->i_value);

	mcc_ast_delete(expr);
}

//  TEST(BinaryOp_13)
#define TESTS \
	TEST(BinaryOp_1) \
	TEST(BinaryOp_2) \
	TEST(BinaryOp_3) \
	TEST(BinaryOp_4) \
	TEST(BinaryOp_5) \
	TEST(BinaryOp_6) \
	TEST(BinaryOp_7) \
	TEST(BinaryOp_8) \
	TEST(BinaryOp_9) \
	TEST(BinaryOp_10) \
	TEST(BinaryOp_11) \
	TEST(BinaryOp_12) \
	TEST(UnaryOp_1) \
	TEST(UnaryOp_2) \
	TEST(NestedExpression_1) \
	TEST(MissingClosingParenthesis_1) \
	TEST(SourceLocation_SingleLineColumn) \
	TEST(SourceLocation_MultiLineColumn) \
	TEST(FunctionCallexpr) \
	TEST(FunctionCallStmt) \
	TEST(DanglingElse) \
	TEST(PrecedenceMul)

#include "main_stub.inc"
#undef TESTS
