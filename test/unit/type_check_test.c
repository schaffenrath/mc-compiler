#include "mcc/ast.h"
#include "mcc/ast_symb.h"
#include "mcc/ast_synth.h"
#include "mcc/parser.h"
#include "mcc/symbol_table.h"
#include <CuTest.h>

void Synthesize_1(CuTest *tc)
{
	const char input[] = "int main(){return 1+3;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_INT, stmnt->functionBody->returnExpression->synthesizedType);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_2(CuTest *tc)
{
	const char input[] = "float main(){return 1.3+3.0;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_FLOAT, stmnt->functionBody->returnExpression->synthesizedType);

	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbolTables, 1), "main", MCC_SYMBOL_FUNC_ONLY);

	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, stmnt->functionBody->returnExpression->synthesizedType, attr->type);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_3(CuTest *tc)
{
	const char input[] = "bool main(){return true&&false;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_BOOL, stmnt->functionBody->returnExpression->synthesizedType);
	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbolTables, 1), "main", MCC_SYMBOL_FUNC_ONLY);

	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, stmnt->functionBody->returnExpression->synthesizedType, attr->type);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_4(CuTest *tc)
{
	const char input[] = "int main(){int[2] a; return a[1];}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_INT, stmnt->functionBody->right_neighbor->returnExpression->synthesizedType);
	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbolTables, 1), "main", MCC_SYMBOL_FUNC_ONLY);

	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, stmnt->functionBody->right_neighbor->returnExpression->synthesizedType, attr->type);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_5(CuTest *tc)
{
	const char input[] = "void test5(){return;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_VOID, stmnt->returnType);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_6(CuTest *tc)
{
	const char input[] = "bool test6(){float a;float b;return a>b;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_BOOL,
	                  stmnt->functionBody->right_neighbor->right_neighbor->returnExpression->synthesizedType);
	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbolTables, 1), "test6", MCC_SYMBOL_FUNC_ONLY);

	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, stmnt->functionBody->right_neighbor->right_neighbor->returnExpression->synthesizedType,
	                  attr->type);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_7(CuTest *tc)
{
	const char input[] = "string test7(){string a; a =\"string\";}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 0, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_STRING,
	                  stmnt->functionBody->right_neighbor->statementAssignRightside->synthesizedType);
	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbolTables, 1), "test7", MCC_SYMBOL_FUNC_ONLY);

	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, stmnt->functionBody->right_neighbor->statementAssignRightside->synthesizedType,
	                  attr->type);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_8(CuTest *tc)
{
	const char input[] = "bool test8(){bool[3] a; return a[2];}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_BOOL,
	                  stmnt->functionBody->right_neighbor->returnExpression->synthesizedType);
	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbolTables, 1), "test8", MCC_SYMBOL_FUNC_ONLY);

	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, stmnt->functionBody->right_neighbor->returnExpression->synthesizedType, attr->type);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_9(CuTest *tc)
{
	const char input[] = "string test9(){if (!(3 > -2)){return \"test\";} else {return \"test2\";}}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_BOOL, stmnt->functionBody->condition->synthesizedType);
	CuAssertIntEquals(tc, MCC_AST_TYPE_STRING,
	                  stmnt->functionBody->truepath->firstStatement->returnExpression->synthesizedType);
	CuAssertIntEquals(tc, MCC_AST_TYPE_STRING,
	                  stmnt->functionBody->falsepath->firstStatement->returnExpression->synthesizedType);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_10(CuTest *tc)
{
	const char input[] = "int test10(){if ((3 > -2.2)){return \"test\";} else {return \"test2\";}}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_UNKNOWN, stmnt->functionBody->condition->synthesizedType);
	CuAssertIntEquals(tc, MCC_AST_TYPE_STRING,
	                  stmnt->functionBody->truepath->firstStatement->returnExpression->synthesizedType);
	CuAssertIntEquals(tc, MCC_AST_TYPE_STRING,
	                  stmnt->functionBody->falsepath->firstStatement->returnExpression->synthesizedType);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_11(CuTest *tc)
{
	const char input[] = "int test11(){while (3 > 2){float[3] a; a[2] = a+a;}}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 0, stmnt->hasReturn);
	CuAssertIntEquals(tc, MCC_AST_TYPE_BOOL, stmnt->functionBody->whilecondition->synthesizedType);
	CuAssertIntEquals(
	    tc, MCC_AST_TYPE_FLOAT_ARRAY,
	    stmnt->functionBody->whilebody->firstStatement->right_neighbor->statementAssignRightside->synthesizedType);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void Synthesize_12(CuTest *tc)
{
	const char input[] = "int fac(int num, int num2){return 1;} int main(){3+3;return fac(3,5);}";
	struct mcc_parser_result result = mcc_parse_string(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	CuAssertIntEquals(tc, 1, stmnt->hasReturn);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

#define TESTS \
	TEST(Synthesize_1) \
	TEST(Synthesize_2) \
	TEST(Synthesize_3) \
	TEST(Synthesize_4) \
	TEST(Synthesize_5) \
	TEST(Synthesize_6) \
	TEST(Synthesize_7) \
	TEST(Synthesize_8) \
	TEST(Synthesize_9) \
	TEST(Synthesize_10) \
	TEST(Synthesize_11) \
	TEST(Synthesize_12)

#include "main_stub.inc"
#undef TESTS
