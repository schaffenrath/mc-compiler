#include "mcc/ast.h"
#include "mcc/ast_ir.h"
#include "mcc/ast_symb.h"
#include "mcc/ast_synth.h"
#include "mcc/parser.h"
#include "mcc/symbol_table.h"
#include <CuTest.h>

void Basics(CuTest *tc)
{
	const char input[] = "int main(){int a;int b; int c; a = b+c;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	mcc_ir_ptr_t ir;
	mcc_ast_ir_statement(stdout, stmnt, &ir);

	mcc_ir_print(ir, stdout);

	mcc_ir_delete(ir);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void IntOperations(CuTest *tc)
{
	const char input[] = "int main(){int a;int b; int c; a = b-c; b = b*c; b = b/5;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	mcc_ir_ptr_t ir;
	mcc_ast_ir_statement(stdout, stmnt, &ir);

	mcc_ir_print(ir, stdout);

	mcc_ir_delete(ir);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void BoolOperations(CuTest *tc)
{
	const char input[] = "int main(){bool a;bool b; bool c; a = b && c; b = c || a; c = c == true;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	mcc_ir_ptr_t ir;
	mcc_ast_ir_statement(stdout, stmnt, &ir);

	mcc_ir_print(ir, stdout);

	mcc_ir_delete(ir);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void CompareOperations(CuTest *tc)
{
	const char input[] = "int main(){int a;int b; bool c; c = a < b; c = a > b; c = a <= b; c = a >= b;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	mcc_ir_ptr_t ir;
	mcc_ast_ir_statement(stdout, stmnt, &ir);

	mcc_ir_print(ir, stdout);

	mcc_ir_delete(ir);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}
void UnaryOperations(CuTest *tc)
{
	const char input[] = "int main(){int a;bool b;int c; a = -5; c = -a; b = !true; }";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	mcc_ir_ptr_t ir;
	mcc_ast_ir_statement(stdout, stmnt, &ir);

	mcc_ir_print(ir, stdout);

	mcc_ir_delete(ir);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void IfStatement(CuTest *tc)
{
	const char input[] = "int main(){int a;int b;int c; if (a+b > 10) c = 30; }";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	mcc_ir_ptr_t ir;
	mcc_ast_ir_statement(stdout, stmnt, &ir);

	mcc_ir_print(ir, stdout);

	mcc_ir_delete(ir);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void IfElseStatement(CuTest *tc)
{
	const char input[] = "int main(){int a;int b;int c; if (a+b > 10) c = 30; else c = 40; }";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	mcc_ir_ptr_t ir;
	mcc_ast_ir_statement(stdout, stmnt, &ir);

	mcc_ir_print(ir, stdout);

	mcc_ir_delete(ir);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

void WhileStatement(CuTest *tc)
{
	const char input[] = "int main(){int a; a = 0;while (a < 10) a = a+1;}";
	struct mcc_parser_result result = mcc_parse_string_statement(input);

	CuAssertIntEquals(tc, MCC_PARSER_STATUS_OK, result.status);

	list_t *allSymbolTables;

	struct mcc_ast_statement *stmnt = result.statement;
	mcc_ast_symb_statement(stdout, stmnt, 0, &allSymbolTables);
	mcc_ast_synth_statement(stdout, stmnt, 0);

	mcc_ir_ptr_t ir;
	mcc_ast_ir_statement(stdout, stmnt, &ir);

	mcc_ir_print(ir, stdout);

	mcc_ir_delete(ir);

	mcc_ast_symb_clear_symTab(allSymbolTables);
	mcc_ast_delete(stmnt);
}

#define TESTS \
	TEST(Basics) \
	TEST(IntOperations) \
	TEST(BoolOperations) \
	TEST(CompareOperations) \
	TEST(UnaryOperations) \
	TEST(IfStatement) \
	TEST(IfElseStatement) \
	TEST(WhileStatement)

#include "main_stub.inc"
#undef TESTS
