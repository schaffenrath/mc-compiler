#include <CuTest.h>
#include <stdlib.h>

#include "mcc/ast.h"
#include "mcc/ast_symb.h"
#include "mcc/parser.h"
#include "mcc/symbol_table.h"

void Symbol_Pure(CuTest *tc)
{
	struct mcc_symbolTable *tab = mcc_symbTcreateRoot();
	CuAssertPtrNotNull(tc, tab);

	mcc_symbTdelete(tab);
}

void Symbol_Lookup_Simple(CuTest *tc)
{
	struct mcc_symbolTable *tab = mcc_symbTcreateRoot();

	struct mcc_symbolAttributes *attr = malloc(sizeof(struct mcc_symbolAttributes));
	attr->array_size = 0;
	attr->parameter_type_list = NULL;
	attr->type = MCC_AST_TYPE_VOID;
	mcc_symbTaddSymbolVariable(tab, "test1", attr);

	CuAssertPtrEquals(tc, attr, mcc_symbTlookup(tab, "test1", MCC_SYMBOL_VARIABLE_ONLY));
	CuAssertPtrEquals(tc, attr, mcc_symbTlookupRec(tab, "test1", MCC_SYMBOL_VARIABLE_ONLY));

	CuAssertPtrEquals(tc, NULL, mcc_symbTlookup(tab, "test2", MCC_SYMBOL_ALL));
	CuAssertPtrEquals(tc, NULL, mcc_symbTlookupRec(tab, "test2", MCC_SYMBOL_ALL));

	mcc_symbTdelete(tab);
}

void Symbol_Lookup_Complex(CuTest *tc)
{
	struct mcc_symbolTable *tab = mcc_symbTcreateRoot();
	struct mcc_symbolTable *tabSub = mcc_symbTcreate(tab, "tab1");

	struct mcc_symbolAttributes *attr = malloc(sizeof(struct mcc_symbolAttributes));
	struct mcc_symbolAttributes *attr2 = malloc(sizeof(struct mcc_symbolAttributes));
	attr2->isParameter = attr->isParameter = 0;
	attr2->array_size = attr->array_size = 0;
	attr2->parameter_type_list = attr->parameter_type_list = NULL;
	attr2->type = attr->type = MCC_AST_TYPE_VOID;
	mcc_symbTaddSymbolVariable(tab, "test1", attr);
	mcc_symbTaddSymbolVariable(tabSub, "test2", attr2);

	CuAssertPtrEquals(tc, attr, mcc_symbTlookup(tab, "test1", MCC_SYMBOL_ALL));
	CuAssertPtrEquals(tc, attr, mcc_symbTlookupRec(tab, "test1", MCC_SYMBOL_ALL));

	CuAssertPtrEquals(tc, NULL, mcc_symbTlookup(tab, "test2", MCC_SYMBOL_ALL));
	CuAssertPtrEquals(tc, NULL, mcc_symbTlookupRec(tab, "test2", MCC_SYMBOL_ALL));

	CuAssertPtrEquals(tc, NULL, mcc_symbTlookup(tabSub, "test1", MCC_SYMBOL_ALL));
	CuAssertPtrEquals(tc, attr, mcc_symbTlookupRec(tabSub, "test1", MCC_SYMBOL_ALL));

	CuAssertPtrEquals(tc, attr2, mcc_symbTlookup(tabSub, "test2", MCC_SYMBOL_ALL));
	CuAssertPtrEquals(tc, attr2, mcc_symbTlookupRec(tabSub, "test2", MCC_SYMBOL_ALL));

	mcc_symbTdelete(tab);
	mcc_symbTdelete(tabSub);
}

void Symbol_Code1(CuTest *tc)
{
	struct mcc_parser_result res = mcc_parse_string_statement("int i;");

	list_t *allSymbols;
	mcc_ast_symb(stderr, res.statement, 0, &allSymbols);

	CuAssertIntEquals(tc, 1, allSymbols->size);

	struct mcc_symbolAttributes *attr = mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 0), "i", MCC_SYMBOL_ALL);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_INT, attr->type);
	CuAssertPtrEquals(tc, NULL, attr->parameter_type_list);

	mcc_ast_symb_clear_symTab(allSymbols);
	mcc_ast_delete(res.statement);
}

void Symbol_Code2(CuTest *tc)
{
	struct mcc_parser_result res = mcc_parse_string_statement("void func(){ int i; };");

	list_t *allSymbols;
	mcc_ast_symb(stderr, res.statement, 0, &allSymbols);

	CuAssertIntEquals(tc, 2, allSymbols->size);

	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "i", MCC_SYMBOL_VARIABLE_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_INT, attr->type);
	CuAssertPtrEquals(tc, NULL, attr->parameter_type_list);

	attr = mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "func", MCC_SYMBOL_FUNC_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_VOID, attr->type);
	CuAssertPtrNotNull(tc, attr->parameter_type_list);
	CuAssertIntEquals(tc, 0, attr->parameter_type_list->size);

	mcc_ast_symb_clear_symTab(allSymbols);
	mcc_ast_delete(res.statement);
}

void Symbol_Code3(CuTest *tc)
{
	struct mcc_parser_result res =
	    mcc_parse_string_statement("void funcParam(int param1, float[100] param2){ bool i; }");

	list_t *allSymbols;
	mcc_ast_symb(stderr, res.statement, 0, &allSymbols);

	CuAssertIntEquals(tc, 2, allSymbols->size);

	struct mcc_symbolAttributes *attr = mcc_symbTlookup((struct mcc_symbolTable *)list_get(allSymbols, 1), "i", MCC_SYMBOL_ALL);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_BOOL, attr->type);
	CuAssertPtrEquals(tc, NULL, attr->parameter_type_list);
	attr = mcc_symbTlookup((struct mcc_symbolTable *)list_get(allSymbols, 1), "param1", MCC_SYMBOL_ALL);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_INT, attr->type);
	CuAssertPtrEquals(tc, NULL, attr->parameter_type_list);
	attr = mcc_symbTlookup((struct mcc_symbolTable *)list_get(allSymbols, 1), "param2", MCC_SYMBOL_ALL);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_FLOAT_ARRAY, attr->type);
	CuAssertPtrEquals(tc, NULL, attr->parameter_type_list);

	attr = mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "funcParam", MCC_SYMBOL_ALL);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_VOID, attr->type);
	CuAssertPtrNotNull(tc, attr->parameter_type_list);
	CuAssertIntEquals(tc, 2, attr->parameter_type_list->size);
	CuAssertIntEquals(tc, MCC_AST_TYPE_INT,
	                  ((struct mcc_symbolAttributes *)list_get(attr->parameter_type_list, 0))->type);
	CuAssertIntEquals(tc, MCC_AST_TYPE_FLOAT_ARRAY,
	                  ((struct mcc_symbolAttributes *)list_get(attr->parameter_type_list, 1))->type);
	CuAssertIntEquals(tc, 100, ((struct mcc_symbolAttributes *)list_get(attr->parameter_type_list, 1))->array_size);

	mcc_ast_symb_clear_symTab(allSymbols);
	mcc_ast_delete(res.statement);
}

void Symbol_Code4(CuTest *tc)
{
	struct mcc_parser_result res =
	    mcc_parse_string_statement("void funcParam(int param1, float param2){ param1 = 5; }");

	list_t *allSymbols;
	mcc_ast_symb(stderr, res.statement, 0, &allSymbols);

	CuAssertIntEquals(tc, 2, allSymbols->size);

	CuAssertPtrEquals(tc, list_get(allSymbols, 0), res.statement->node.symbolTable);

	CuAssertPtrEquals(tc, list_get(allSymbols, 1), res.statement->functionBody->node.symbolTable);

	mcc_ast_symb_clear_symTab(allSymbols);
	mcc_ast_delete(res.statement);
}

void Symbol_DoubleName(CuTest *tc)
{
	struct mcc_parser_result res = mcc_parse_string_statement("void this(string this){ return this; }");

	list_t *allSymbols;
	mcc_ast_symb(stderr, res.statement, 0, &allSymbols);

	CuAssertIntEquals(tc, 2, allSymbols->size);

	CuAssertPtrEquals(tc, list_get(allSymbols, 0), res.statement->node.symbolTable);
	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "this", MCC_SYMBOL_FUNC_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_VOID, attr->type);

	CuAssertPtrEquals(tc, list_get(allSymbols, 1), res.statement->functionBody->node.symbolTable);
	attr = mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "this", MCC_SYMBOL_VARIABLE_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_STRING, attr->type);

	mcc_ast_symb_clear_symTab(allSymbols);
	mcc_ast_delete(res.statement);
}

void Symbol_Base_Functions(CuTest *tc)
{
	struct mcc_parser_result res = mcc_parse_string_statement("void this(string this){ return this; }");

	list_t *allSymbols;
	mcc_ast_symb(stderr, res.statement, 0, &allSymbols);

	struct mcc_symbolAttributes *attr =
	    mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "print", MCC_SYMBOL_FUNC_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_VOID, attr->type);
	CuAssertIntEquals(tc, 1, attr->parameter_type_list->size);
	struct mcc_symbolAttributes *paramAttr = list_get(attr->parameter_type_list, 0);
	CuAssertPtrNotNull(tc, paramAttr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_STRING, paramAttr->type);

	attr = mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "print_nl", MCC_SYMBOL_FUNC_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_VOID, attr->type);
	CuAssertIntEquals(tc, 0, attr->parameter_type_list->size);

	attr = mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "print_int", MCC_SYMBOL_FUNC_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_VOID, attr->type);
	CuAssertIntEquals(tc, 1, attr->parameter_type_list->size);
	paramAttr = list_get(attr->parameter_type_list, 0);
	CuAssertPtrNotNull(tc, paramAttr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_INT, paramAttr->type);

	attr = mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "print_float", MCC_SYMBOL_FUNC_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_VOID, attr->type);
	CuAssertIntEquals(tc, 1, attr->parameter_type_list->size);
	paramAttr = list_get(attr->parameter_type_list, 0);
	CuAssertPtrNotNull(tc, paramAttr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_FLOAT, paramAttr->type);

	attr = mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "read_int", MCC_SYMBOL_FUNC_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_INT, attr->type);
	CuAssertIntEquals(tc, 0, attr->parameter_type_list->size);

	attr = mcc_symbTlookupRec((struct mcc_symbolTable *)list_get(allSymbols, 1), "read_float", MCC_SYMBOL_FUNC_ONLY);
	CuAssertPtrNotNull(tc, attr);
	CuAssertIntEquals(tc, MCC_AST_TYPE_FLOAT, attr->type);
	CuAssertIntEquals(tc, 0, attr->parameter_type_list->size);

	mcc_ast_symb_clear_symTab(allSymbols);
	mcc_ast_delete(res.statement);
}

#define TESTS \
	TEST(Symbol_Pure) \
	TEST(Symbol_Lookup_Simple) \
	TEST(Symbol_Lookup_Complex) \
	TEST(Symbol_Code1) \
	TEST(Symbol_Code2) \
	TEST(Symbol_Code3) \
	TEST(Symbol_Code4) \
	TEST(Symbol_DoubleName) \
	TEST(Symbol_Base_Functions)

#include "main_stub.inc"
#undef TESTS
