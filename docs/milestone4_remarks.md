# Milestone 4

- `test` contains files that shouldn't be present for the final submission.
- Build, unit tests, and integration tests look good.
- Consider splitting `translate_copy` into smaller functions.
- Please don't use `exec` directly, either fork and call `exec` in the child, or simply use `system`.
