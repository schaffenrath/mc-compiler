# Milestone 1

- `mv ex01/dijkstra test/integration`
- Add example inputs from course repository to `test/integration`
- Disable unfinished parser test *ArrayAssignment2*
- Parser leaks memory (mentioned in README)
- Integration test fails

		./mcc -o integration_tests/./fib /home/user/milestone1/team_02_milestone_1/scripts/../test/integration/./fib/./fib.mc
		fopen: No such file or directory

	`mcc` does not support the flags required by the specification

- `mc_ast_to_dot` does not support the flags required by the specification
- `mc_ast_to_dot` output cannot be piped directly to `dot` as it appends `Success`
- `start_rule` is an implementation detail!
- `start_rule` should be thread local to ensure thread-safety
- Function definition / declaration is not a statement
- Parser prints directly to `stderr`
- Code inconsistently indented!
- Prefer `snprintf` over `sprintf`
- Please follow the error message format illustrated in the specification
	- Filename is missing
- Prevent code duplication (e.g.: `mcc_parse_string`, `mcc_parse_string_expression`, `mcc_parse_string_statement`)

All test inputs should already be part of the integration test suite.
While the compiler does not yet yield executables, it should accept all of these inputs.
