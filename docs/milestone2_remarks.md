# Milestone 2

- Move `arg_handler_default.h` into `app`.
	`include` should contain the public interface of the library exclusively.
	It's fine to also have shared header files inside `app`.
- Update the integration test runner (see previous mail).
- Consider putting third-party code into the `vendor` directory.
	- Think about whether `hashmap.h` and `map.h` need to be part of the library's public interface.
	- Be careful when using third-party code regarding library exports.
- Use the `UNUSED` macro to supress warnings about unused variables and parameters.
- Look into the remaining warnings.
	- Pay special attention to format string related stuff as this can easily cause memory corruption.
	- Fallthrough warnings can be supress with a comment.
- No semantic error reported when passing a `bool[3]` to a function taking a `bool[4]`.
	The size of an array is part of the type.
	Other semantic checks look fine.
- `mc_type_check_trace` does not output anything (?)
- `mc_symbol_table` seems fine, consider adding source locations.
- `ast_symb.h` and `ast_synth.h` shared the same header comment, this may be an oversight.
- Consider reorganizing includes / header guards:
	- Header guard first
	- System includes before project includes
- Keep the following coding guidelines in mind:
	- Avoid using conditional and loop statements inside `case`.
- `utils/log.c` contains code that directly prints to `stderr`.
- `utils/map.c` skips error checking `malloc` and `realloc` calls.
