# Milestone 3

- Move `arg_handler_default.h` and `parse_handler_default.h` to `app`.
	They are not part of the library's public interface.
	Having header files inside `app` is fine.
- Warnings about implicit function declaration `mcc_parser_error`.
- Please update the integration test runner.
- The `-f` option should only be considered after semantic checks have been run.
	Consider the following:

		$ ./mc_cfg_to_dot -f main ../test/integration/fib/fib.mc
		08:57:00 ERROR ../test/integration/fib/fib.mc:19:10: Function `fib` not yet declared

		08:57:00 ERROR ../test/integration/fib/fib.mc:19:1: non compatible expression literals for assignment to 'result', (INT and UNKNOWN TYPE)

		digraph "AST" {
				nodesep=0.6
				"0x55980eaf5430" [shape=box, label="main:\n...\nRETURN _temp7\n"];
		}

	This should not trigger an error, just output the CFG of `main`.
- Consider printing all instructions in a basic block.
	Printing the CFG is also a debug utility which should not hide information.
	If you'd implement some optimisation pass on the IR, like constant folding + propagation, having these instructions printed would be helpful.
	But I won't subtract points for this, so if you got other work to do, leave it as it is.
- Pay attention when using other people's code (`utils/map.c:45`).
- Consider storing third-party code inside `vendor`.
