#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "arg_handler_default.h"
#include "mcc/ast.h"
#include "mcc/ast_ir.h"
#include "mcc/ast_symb.h"
#include "mcc/ast_synth.h"
#include "mcc/ir_asm.h"
#include "mcc/parser.h"
#include "parse_handler_default.h"

const char doc[] =
    "\nThe mC compiler. It takes mC input files and produces an executable.\n\nUse '-' as input file to read from "
    "stdin.\n\nEnvironment Variables:\nMCC_BACKEND \t override the back-end compiler (defaults to 'gcc' in PATH)";

int main(int argc, char *argv[])
{
	log_init();
	arguments_t arguments = init_args();

	const struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	log_set_quiet(arguments.quiet);

	char *mccbackend;
	if ((mccbackend = getenv("MCC_BACKEND")) == NULL) {
		mccbackend = "gcc";
	}

	struct mcc_ast_statement **stmnt = NULL;

	if (parse(&stmnt, &arguments) != 0)
		return EXIT_FAILURE;

	for (unsigned int i = 0; i < arguments.argc; i++) {
		list_t *allSymbolTables;

		if (mcc_ast_symb_start(stdout, stmnt[i], 0, arguments.args[i], &allSymbolTables) ==
		    MCC_SYMBT_STATUS_OK) {
			if (mcc_ast_synth_start(stdout, stmnt[i], 0, arguments.args[i]) == MCC_SYNTH_STATUS_OK) {

				if (arguments.function)
					if (filter_function(&stmnt[i], &arguments) != 0) {
						mcc_ast_symb_clear_symTab(allSymbolTables);
						continue;
					}

				mcc_ir_ptr_t ir;
				mcc_ast_ir_statement(stdout, stmnt[i], &ir);

				mcc_asm_ptr_t assembler = mcc_ir_asm_create_assembler();
				// build up memory for all the symbols
				list_iterate(allSymbolTables, mcc_symbTbuildMemory, NULL);

				mcc_ir_asm_translate(ir, assembler);

				FILE *tmp = fopen("/tmp/tempasm.s", "w");
				mcc_ir_asm_to_output(tmp, assembler);
				fclose(tmp);
				char command[200];
				if (arguments.output) {

					snprintf(command, 200,
					         "%s /tmp/tempasm.s ../resources/mc_builtins.c -m32 -o %s", mccbackend,
					         arguments.out_file);
				} else {
					snprintf(command, 200, "%s /tmp/tempasm.s ../resources/mc_builtins.c -m32",
					         mccbackend);
				}
				if (system(command) != 0) {
					fprintf(stderr, "Error on command \"%s\".\n", command);
				}
				mcc_ir_asm_delete(assembler);
				mcc_ir_delete(ir);

				if (system("rm /tmp/tempasm.s") != 0) {
					fprintf(stderr, "File \"/tmp/tempasm.s\" was not deleted properly.\n");
				}
			} else {
				mcc_ast_symb_clear_symTab(allSymbolTables);
				clean_parse(&arguments, stmnt);
				log_close();
				return EXIT_FAILURE;
			}
		} else {
			mcc_ast_symb_clear_symTab(allSymbolTables);
			clean_parse(&arguments, stmnt);
			log_close();
			return EXIT_FAILURE;
		}

		mcc_ast_symb_clear_symTab(allSymbolTables);
	}

	// cleanup
	clean_parse(&arguments, stmnt);
	log_close();
	return EXIT_SUCCESS;
}
