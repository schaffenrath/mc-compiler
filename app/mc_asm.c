#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arg_handler_default.h"
#include "mcc/ast.h"
#include "mcc/ast_ir.h"
#include "mcc/ast_symb.h"
#include "mcc/ast_synth.h"
#include "mcc/ir_asm.h"
#include "mcc/parser.h"
#include "parse_handler_default.h"

const char doc[] = "\nUtility for printing the generated assembly code. Errors are reported on invalid inputs.\n\n\
Use '-' as input file to read from stdin.";

int main(int argc, char *argv[])
{
	log_init();
	arguments_t arguments = init_args();

	const struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	struct mcc_ast_statement **stmnt = NULL;

	if (parse(&stmnt, &arguments) != 0)
		return EXIT_FAILURE;

	for (unsigned int i = 0; i < arguments.argc; i++) {
		list_t *allSymbolTables;

		if (mcc_ast_symb_start(arguments.out, stmnt[i], 0, arguments.args[i], &allSymbolTables) ==
		    MCC_SYMBT_STATUS_OK) {
			mcc_ast_synth_start(arguments.out, stmnt[i], 0, arguments.args[i]);

			if (arguments.function)
				if (filter_function(&stmnt[i], &arguments) != 0) {
					mcc_ast_symb_clear_symTab(allSymbolTables);
					continue;
				}

			mcc_ir_ptr_t ir;
			mcc_ast_ir_statement(arguments.out, stmnt[i], &ir);
			mcc_asm_ptr_t assembler = mcc_ir_asm_create_assembler();
			// build up memory for all the symbols
			list_iterate(allSymbolTables, mcc_symbTbuildMemory, NULL);
			list_iterate(allSymbolTables, mcc_symbTprintList, stdout);
			mcc_ir_asm_translate(ir, assembler);
			if (arguments.function) {
				filter_function_asm(assembler, &arguments);
				mcc_ir_asm_to_output_f_flag(arguments.out, assembler);
			} else {
				mcc_ir_asm_to_output(arguments.out, assembler);
			}
			mcc_ir_asm_delete(assembler);
			mcc_ir_delete(ir);
		}

		mcc_ast_symb_clear_symTab(allSymbolTables);
	}

	// cleanup
	clean_parse(&arguments, stmnt);
	log_close();
	return EXIT_SUCCESS;
}
