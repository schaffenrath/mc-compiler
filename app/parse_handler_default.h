#ifndef PARSE_HANDLER_DEF
#define PARSE_HANDLER_DEF

#include <argp.h>
#include <stdlib.h>
#include <string.h>

#include "arg_handler_default.h"
#include "mcc/ast.h"
#include "mcc/ir_asm.h"
#include "mcc/list.h"
#include "mcc/parser.h"

int handle_in_out(arguments_t *arguments)
{
	// handle output
	arguments->out = stdout;
	if (arguments->output) {
		arguments->out = fopen(arguments->out_file, "w+");
		if (arguments->out == NULL) {
			fprintf(stderr, "Error opening file %s", arguments->out_file);
			return 1;
		}
	}

	// handle input
	arguments->in = (FILE **)malloc(arguments->argc * sizeof(FILE *));
	for (unsigned int i = 0; i < arguments->argc; i++) {
		if (strcmp("-", arguments->args[i]) == 0) {
			arguments->in[i] = stdin;
		} else {
			arguments->in[i] = fopen(arguments->args[i], "r");
			if (!arguments->in[i]) {
				fclose(arguments->out);
				free(arguments->in);
				free(arguments->args);
				perror("fopen");
				return 1;
			}
		}
	}

	return 0;
}

void close_files(arguments_t *arguments)
{

	fclose(arguments->out);
	for (unsigned int i = 0; i < arguments->argc; i++) {
		mcc_ast_delete(arguments->stmnt_head[i]);
		fclose(arguments->in[i]);
	}
	free(arguments->in);
	free(arguments->stmnt_head);
}

void clean_parse(arguments_t *arguments, struct mcc_ast_statement **stmnt)
{
	close_files(arguments);
	clean_args(arguments);
	free(stmnt);
}

int parse(struct mcc_ast_statement ***stmnt, arguments_t *arguments)
{
	if (handle_in_out(arguments) != 0)
		return 1;

	arguments->stmnt_head = (struct mcc_ast_statement **)malloc(arguments->argc * sizeof(struct mcc_ast_statement));
	*stmnt = (struct mcc_ast_statement **)malloc(arguments->argc * sizeof(struct mcc_ast_statement));

	for (unsigned int i = 0; i < arguments->argc; i++) {
		struct mcc_parser_result result = mcc_parse_file_with_name(arguments->in[i], arguments->args[i]);
		if (result.status != MCC_PARSER_STATUS_OK) {
			perror("Parsing error");
			clean_parse(arguments, *stmnt);
			return 1;
		}
		arguments->stmnt_head[i] = result.statement;
		*stmnt[i] = arguments->stmnt_head[i];
	}
	return 0;
}

int filter_function(struct mcc_ast_statement **stmnt, arguments_t *arguments)
{
	if (arguments->function) {
		while (*stmnt != NULL && strcmp((*stmnt)->functionName, arguments->func_name) != 0) {
			*stmnt = (*stmnt)->right_neighbor;
		}

		if (*stmnt == NULL) {
			fprintf(stderr, "The function '%s' was not found in the provided .mc file!\n",
			        arguments->func_name);
			return 1;
		}
		// delete functions declared after specified one
		else if ((*stmnt)->right_neighbor != NULL) {
			mcc_ast_delete((*stmnt)->right_neighbor);
			(*stmnt)->right_neighbor = NULL;
		}
	}
	return 0;
}

int filter_function_asm(mcc_asm_ptr_t assembler, arguments_t *arguments)
{
	char *funcName = (char *)malloc(sizeof(char) * 255);
	snprintf(funcName, 255, "%s:", arguments->func_name);
	list_element_t *iterator = assembler->code->first;

	while (iterator != NULL && strcmp((char *)iterator->data, funcName) != 0) {
		list_element_t *next_element = iterator->next;
		free(iterator->data);
		free(iterator);
		iterator = next_element;
	}
	assembler->code->first = iterator;
	free(funcName);
	return 0;
}
#endif
