#include <argp.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arg_handler_default.h"
#include "mcc/ast.h"
#include "mcc/ast_print.h"
#include "mcc/parser.h"
#include "parse_handler_default.h"

const char doc[] = "\nUtility for printing an abstract syntax tree in the DOT format. The output can be visualised \
using graphviz. Errors are reported on invalid inputs.\n\nUse '-' as input file to read from stdin.";

int main(int argc, char **argv)
{
	log_init();

	arguments_t arguments = init_args();

	const struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	log_set_quiet(arguments.quiet);

	struct mcc_ast_statement **stmnt = NULL;

	if (parse(&stmnt, &arguments) != 0)
		return EXIT_FAILURE;

	for (unsigned int i = 0; i < arguments.argc; i++) {
		if (arguments.function)
			filter_function(&stmnt[i], &arguments);

		mcc_ast_print_dot(arguments.out, stmnt[i]);
	}

	// cleanup
	clean_parse(&arguments, stmnt);
	log_close();
	return EXIT_SUCCESS;
}
