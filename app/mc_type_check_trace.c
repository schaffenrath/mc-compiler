#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arg_handler_default.h"
#include "mcc/ast.h"
#include "mcc/ast_symb.h"
#include "mcc/ast_synth.h"
#include "mcc/log.h"
#include "mcc/parser.h"
#include "parse_handler_default.h"

const char doc[] = "\nUtility for tracing the type checking process. Errors are reported on invalid inputs.\n\nUse "
                   "'-' as input file to read from stdin.";

int main(int argc, char *argv[])
{
	log_init();
	log_set_level(2); // always report info logs
	arguments_t arguments = init_args();

	const struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	log_set_quiet(arguments.quiet);

	struct mcc_ast_statement **stmnt = NULL;

	if (parse(&stmnt, &arguments) != 0)
		return EXIT_FAILURE;

	for (unsigned int i = 0; i < arguments.argc; i++) {
		list_t *allSymbolTables;
		enum mcc_sythesizeStatus resultSynthesizing =
		    mcc_ast_symb_start(arguments.out, stmnt[i], 0, arguments.args[i], &allSymbolTables);

		if (resultSynthesizing == EXIT_FAILURE) {
			mcc_ast_symb_clear_symTab(allSymbolTables);
			clean_parse(&arguments, stmnt);
			log_close();
			return EXIT_FAILURE;
		}

		resultSynthesizing = mcc_ast_synth_start(arguments.out, stmnt[i], 0, arguments.args[i]);
		mcc_ast_symb_clear_symTab(allSymbolTables);
	}

	// cleanup
	clean_parse(&arguments, stmnt);
	log_close();

	return EXIT_SUCCESS;
}
