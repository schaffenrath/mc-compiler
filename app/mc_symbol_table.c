#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arg_handler_default.h"
#include "mcc/ast.h"
#include "mcc/ast_symb.h"
#include "mcc/parser.h"
#include "parse_handler_default.h"

const char doc[] = "\nUtility for displaying the generated symbol tables. Tables are printed when their scope is \
closed, meaning innermost symbol tables will be printed first. \
Errors are reported on invalid inputs.\
\n\nUse '-' as input file to read from stdin.";

int main(int argc, char *argv[])
{
	log_init();
	struct arguments arguments = init_args();

	const struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	log_set_quiet(arguments.quiet);

	struct mcc_ast_statement **stmnt = NULL;

	if (parse(&stmnt, &arguments) != 0)
		return EXIT_FAILURE;

	list_t *allSymbolTables;
	for (unsigned int i = 0; i < arguments.argc; i++) {
		mcc_ast_symb_start(arguments.out, stmnt[i], MCC_SYMBT_OPTION_PRINT_SYMBOL_TABLE, arguments.args[i],
		                   &allSymbolTables);

		mcc_ast_symb_clear_symTab(allSymbolTables);
	}
	// cleanup
	clean_parse(&arguments, stmnt);
	log_close();
	return EXIT_SUCCESS;
}
