#include <argp.h>
#include <stdlib.h>
#include <string.h>

#include "mcc/ast.h"

#ifndef ARG_HANDLER_DEF
#define ARG_HANDLER_DEF

extern const char doc[];

static char args_doc[] = "FILE";

struct argp_option options[] = {{"output", 'o', "FILE", 0, "Write the output to FILE (defaults to stdout)", 0},
                                {"function", 'f', "FUNC_NAME", 0, "Limit scope to the given function", 0},
                                {"quiet", 'q', 0, 0, "quiets error log messages", 0},
                                {0}};

typedef struct arguments {
	unsigned int argc;
	unsigned int quiet;
	char **args;
	int output, function;
	char *out_file, *func_name;
	FILE **in, *out;
	struct mcc_ast_statement **stmnt_head;
} arguments_t;

arguments_t init_args()
{
	return (arguments_t){.output = 0, .function = 0, .out_file = NULL, .func_name = NULL, .args = NULL, .quiet = 0};
}

void clean_args(arguments_t *args)
{
	if (args->args != NULL)
		free(args->args);
}

static int parse_opt(int key, char *arg, struct argp_state *state)
{
	arguments_t *arguments = state->input;

	switch (key) {
	case 'o':
		arguments->output = 1;
		arguments->out_file = arg;
		break;
	case 'f':
		arguments->function = 1;
		arguments->func_name = arg;
		break;
	case 'q':
		arguments->quiet = 1;
		break;
	case ARGP_KEY_NO_ARGS:
		argp_usage(state);
		break;
	case ARGP_KEY_ARG:
		// if (state->arg_num >= 1)
		// {
		// 	printf("Too many arguments\n");
		// 	argp_usage(state);
		// }
		// printf("State %d\n", state->arg_num);
		return ARGP_ERR_UNKNOWN;
		break;
	case ARGP_KEY_ARGS:
		arguments->argc = state->argc - state->next;
		arguments->args = (char **)malloc(sizeof(char *) * arguments->argc);
		for (unsigned int i = 0; i < arguments->argc; i++) {
			arguments->args[i] = state->argv[state->next + i];
		}
	default:
		break;
	}
	return 0;
}

#endif
